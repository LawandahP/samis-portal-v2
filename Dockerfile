FROM node:14.17.5 as build

WORKDIR /app

COPY . .
COPY package.json ./
#RUN apk add yarn
RUN yarn install --production
RUN yarn build

# Use official nginx image as the base image
FROM karua/angular-nginx

# Copy the build output to replace the default nginx contents.
COPY --from=build /app/build/ /usr/share/nginx/html
COPY --from=build .env.example /usr/share/nginx/html/.env

WORKDIR /usr/share/nginx/html

# Expose port 80
EXPOSE 80
CMD ["/bin/sh", "-c", "runtime-env-cra && nginx -g \"daemon off;\""]
