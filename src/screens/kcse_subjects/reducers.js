
import {
    KCSE_SUBJECT_GET_REQUEST,
    KCSE_SUBJECT_GET_SUCCESS,
    KCSE_SUBJECT_GET_FAIL,

    KCSE_SUBJECT_CREATE_REQUEST,
    KCSE_SUBJECT_CREATE_SUCCESS,
    KCSE_SUBJECT_CREATE_FAIL,
    KCSE_SUBJECT_CREATE_RESET,

    KCSE_SUBJECT_DETAILS_REQUEST,
    KCSE_SUBJECT_DETAILS_SUCCESS,
    KCSE_SUBJECT_DETAILS_FAIL,

    KCSE_SUBJECT_UPDATE_REQUEST,
    KCSE_SUBJECT_UPDATE_SUCCESS,
    KCSE_SUBJECT_UPDATE_FAIL,

    KCSE_SUBJECT_DELETE_REQUEST,
    KCSE_SUBJECT_DELETE_SUCCESS,
    KCSE_SUBJECT_DELETE_FAIL,



} from './constants';


export const kcseSubjectCreateReducer = (state = {}, action) => {
    switch (action.type) {
        case KCSE_SUBJECT_CREATE_REQUEST:
            return { loading: true }

        case KCSE_SUBJECT_CREATE_SUCCESS:
            return { loading: false, success: true, kcseSubject: action.payload.data.items }

        case KCSE_SUBJECT_CREATE_FAIL:
            return { loading: false, error: action.payload }

        case KCSE_SUBJECT_CREATE_RESET:
            return {}
        default:
            return state
    }
}

export const kcseSubjectReadReducer = (state = { kcseSubjects: [] }, action) => {
    switch (action.type) {
        case KCSE_SUBJECT_GET_REQUEST:
            return { loading: true, kcseSubjects: [] }

        case KCSE_SUBJECT_GET_SUCCESS:
            return {
                loading: false,
                kcseSubjects: action.payload.data.items,
                index: action.payload.index,
                totalPages: action.payload.data.page_count,
                size: action.payload.data.page_size
            }

        case KCSE_SUBJECT_GET_FAIL:
            return { loading: false, error: action.payload }

        default:
            return state
    }
}


export const kcseSubjectUpdateReducer = (state = { kcseSubject: {} }, action) => {
    switch (action.type) {
        case KCSE_SUBJECT_UPDATE_REQUEST:
            return { loading: true }

        case KCSE_SUBJECT_UPDATE_SUCCESS:
            return { loading: false, success: true, kcseSubject: action.payload }

        case KCSE_SUBJECT_UPDATE_FAIL:
            return { loading: false, error: action.payload }

        default:
            return state
    }
}



export const kcseSubjectDetailsReducer = (state = { kcseSubject: {} }, action) => {
    switch (action.type) {
        case KCSE_SUBJECT_DETAILS_REQUEST:
            return { loading: true, ...state }

        case KCSE_SUBJECT_DETAILS_SUCCESS:
            return { loading: false, kcseSubject: action.payload.data }

        case KCSE_SUBJECT_DETAILS_FAIL:
            return { loading: false, error: action.payload }

        default:
            return state
    }
}



export const kcseSubjectDeleteReducer = (state = {}, action) => {
    switch (action.type) {
        case KCSE_SUBJECT_DELETE_REQUEST:
            return { loading: true }

        case KCSE_SUBJECT_DELETE_SUCCESS:
            return { loading: false, success: true }

        case KCSE_SUBJECT_DELETE_FAIL:
            return { loading: false, error: action.payload }

        default:
            return state
    }
}

