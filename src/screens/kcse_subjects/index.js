import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';

import { FiDelete, FiEdit3 } from 'react-icons/fi'

import { TabTitle, toTitleCase } from '../../utils/globalFunc'

import Controls from '../../components/controls/Controls'
import ToastAlert from '../../components/display/ToastAlert'
import Loader from '../../components/display/Loader'
import TableComponent from '../../components/useTable/index'
import { Chip, TableTop } from '../../components/useTable/elements'
import { ActionButtonWrapper } from '../../components/controls/Button'
import ConfirmDialog from '../../components/display/dialog'
import Modal from '../../components/display/modal'
import { NgPageContainer, NgPaper } from '../../components/display/elements'
import KcseSubjectEditForm from './editform';
import KcseSubjectForm from './form';

import { createKcseSubject, readKcseSubjects, updateKcseSubject, deleteKcseSubject } from './actions'


const headCells = [
    { id: 'name', label: 'Name', minWidth: 120 },
    { id: 'alias', label: 'Alias', minWidth: 100 },
    { id: 'category', label: 'Category', minWidth: 100 },
    { id: 'code', label: 'Code' },
    { id: 'actions', label: 'Actions', disableSorting: true },
]



function KcseSubjectsReadScreen() {
    // TabTitle('Students | Samis Systems')
    TabTitle('Samis - Kcse Subjects')
    const dispatch = useDispatch()
    
    const kcseSubjectCreate = useSelector(state => state.kcseSubjectCreate)
    const { success: successCreate } = kcseSubjectCreate

    const kcseSubjectRead = useSelector(state => state.kcseSubjectRead)
    const { loading, error, kcseSubjects } = kcseSubjectRead

    const kcseSubjectUpdate = useSelector(state => state.kcseSubjectUpdate)
    const { success: successUpdate } = kcseSubjectUpdate

    const kcseSubjectDelete = useSelector(state => state.kcseSubjectDelete)
    const { success: successDelete } = kcseSubjectDelete


    const [message, setMessage] = useState(null)

    const [openModal, setOpenModal] = useState(false);
    const [openPopup, setOpenPopup] = useState(false);
    const [confirmDialog, setConfirmDialog] = useState({ isOpen: false, title: '', subTitle: '' })

    const [search, setSearch] = useState({ fn: items => { return items; } })
    const [recordForEdit, setRecordForEdit] = useState(null);

    const { TblContainer,
        TblHead,
        TblPagination,
        recordsAfterPaginatingAndSorting,
        page, rowsPerPage
    } = TableComponent(kcseSubjects, headCells, search)


    const handleSearch = (e) => {
        e.preventDefault()
        let target = e.target
        setSearch({
            fn: items => {
                if (target.value === "")
                    return items;
                else
                    return items.filter(x => x.name.includes(toTitleCase(e.target.value)))
            }
        })
    }

    const newEntry = (kcseSubject, handleResetForm) => {
        dispatch(createKcseSubject(kcseSubject))
        setOpenPopup(false);
        handleResetForm()
    }

    const editEntry = (kcseSubject, handleResetForm) => {
        dispatch(updateKcseSubject(kcseSubject))
        setOpenModal(false);
        handleResetForm()

    }

    const editHandler = (kcseSubject) => {
        setRecordForEdit(kcseSubject)
        setOpenPopup(true)
    }

    const deleteHandler = (id) => {
        setConfirmDialog({
            ...confirmDialog,
            isOpen: false
        })
        dispatch(deleteKcseSubject(id))
        dispatch(readKcseSubjects())
    }

    useEffect(() => {
        dispatch(readKcseSubjects(page, rowsPerPage))
        if (successCreate) {
            setMessage('Subject Created Successfully')
        } else {
            if (successUpdate) {
                setMessage('Subject Updated Successfully')
            }
        }
        if (successDelete) {
            setMessage('Subject Deleted Successfully')
        }
    }, [dispatch, successCreate, successUpdate, successDelete, page, rowsPerPage])



    return (
        <NgPageContainer>
            <NgPaper>
                {successCreate && <ToastAlert severity="success">{message}</ToastAlert>}
                {successUpdate && <ToastAlert severity="success">{message}</ToastAlert>}
                {successDelete && <ToastAlert severity="success">{message}</ToastAlert>}
                <TableTop>
                    <Controls.SearchInputField
                        label="Search Subjects"
                        onChange={handleSearch}
                    />
                    <Controls.AddButton
                        onClick={() => setOpenModal(true)}
                    >
                    </Controls.AddButton>
                </TableTop>


                {loading
                    ? <Loader />
                    : error
                        ? <ToastAlert severity="error">{error}</ToastAlert>
                        : (

                            <>
                                <TblContainer>
                                    <TblHead />

                                    <TableBody>
                                        {
                                            recordsAfterPaginatingAndSorting()?.map(kcseSubject =>
                                            (
                                                <TableRow key={kcseSubject.id}>
                                                    <TableCell>{kcseSubject.name}</TableCell>
                                                    <TableCell>{kcseSubject.alias}</TableCell>
                                                    <TableCell>{kcseSubject.category}</TableCell>
                                                    <TableCell>{kcseSubject.code}</TableCell>
                                                    <TableCell>
                                                        <ActionButtonWrapper>
                                                            <Controls.ActionButton
                                                                title="edit"
                                                                onClick={() => editHandler(kcseSubject)}
                                                                edit>
                                                                <FiEdit3 />
                                                            </Controls.ActionButton>
                                                            <Controls.ActionButton
                                                                title="deactivate"
                                                                onClick={() => {
                                                                    setConfirmDialog({
                                                                        isOpen: true,
                                                                        title: "Are you sure you want to delete this Subject?",
                                                                        subTitle: "You can't undo this operation",
                                                                        onConfirm: () => { deleteHandler(kcseSubject.id) }
                                                                    })
                                                                }}>
                                                                <FiDelete />
                                                            </Controls.ActionButton>
                                                        </ActionButtonWrapper>
                                                    </TableCell>
                                                </TableRow>
                                            )
                                            )}
                                    </TableBody>
                                </TblContainer>

                                <TblPagination />
                            </>

                        )}
            </NgPaper>

            <Modal
                openModal={openModal}
                setOpenModal={setOpenModal}
                title="Add Subject"
            >
                <KcseSubjectForm
                    newEntry={newEntry}
                />
            </Modal>

            <Modal
                openPopup={openPopup}
                setOpenPopup={setOpenPopup}
                title="Edit Subjects"
            >
                <KcseSubjectEditForm
                    recordForEdit={recordForEdit}
                    editEntry={editEntry}
                />
            </Modal>

            <ConfirmDialog
                confirmDialog={confirmDialog}
                setConfirmDialog={setConfirmDialog}
            />
        </NgPageContainer>

    )
}


export default KcseSubjectsReadScreen



