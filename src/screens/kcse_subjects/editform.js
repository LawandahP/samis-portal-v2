import React, {useEffect, useState} from 'react'
import Controls from '../../components/controls/Controls'
import ToastAlert from '../../components/display/ToastAlert'
import Loading from '../../components/display/Loader'

import { MainForm, useForm } from '../../components/useForm'
import { toTitleCase } from '../../utils/globalFunc'
import { FormButton, 
    } 
from '../../components/useForm/formElements';

const KcseSubjectEditForm = (props) => {
    const { editEntry, recordForEdit, kcseSubjectId } = props;

    const [ success, setSuccess ] = useState()
    const [ error, setError ]     = useState()
    const [ loading, setLoading ] = useState(false)

    const validate = (fieldValues = values) => {
        let temp = {...errors}
        if('name' in fieldValues)
            temp.name = fieldValues.name ? "" : "Subject Name is Required"
        if('alias' in fieldValues)
            temp.alias = fieldValues.alias ? "" : "Alias is Required"
        if('category' in fieldValues)
            temp.category = fieldValues.category ? "" : "Category is Required"
        if('code' in fieldValues)
            temp.code = fieldValues.code ? "" : "Subject code is Required"
        // temp.phone_no = (\\+?\\d{9,13}).test(values.phone_no) ? "" : "Student Name is Required"
        setErrors({ ...temp })

        // tests whether post array elements passes text implemented by validate() function
        if (fieldValues === values)
            return Object.values(temp).every(x => x === "")
        
    }

    
    const initialValues = {
        id: 0,
        name: '',
        alias: '',
        category: '',
        code: '',
    }

    const { values,  
            setValues,
            errors, 
            setErrors, 
            handleResetForm, 
            handleInputChange } = useForm(initialValues, true, validate);


    const submitHandler = (e) => {
        e.preventDefault()
        if (validate()) {
            editEntry(values, handleResetForm);
        } 
    }
    

    useEffect(() => {
        if(recordForEdit != null)
            setValues({
                ...recordForEdit
            })
    
    }, [recordForEdit, success, loading, error, setValues, kcseSubjectId ])
  
    return (
        <>
            { error && <ToastAlert severity="error">{error}</ToastAlert>}
                <MainForm onSubmit={submitHandler}>
                    <Controls.InputField 
                        error={errors.name}
                        label="Name"
                        value={values.name}
                        name='name'
                        onChange={handleInputChange}
                        onInput={(e) => e.target.value = toTitleCase(e.target.value)}>        
                    </Controls.InputField>
                    
                    <Controls.InputField
                        error={errors.alias}
                        label="Alias"
                        name='alias'
                        value={values.alias}
                        onChange={handleInputChange}
                        onInput={(e) => e.target.value = toTitleCase(e.target.value)}>
                    </Controls.InputField>

                    <Controls.InputField
                        error={errors.category}
                        label="Category"
                        name='category'
                        value={values.category}
                        onChange={handleInputChange}
                        onInput={(e) => e.target.value = toTitleCase(e.target.value)}>
                    </Controls.InputField>

                    <Controls.InputField
                        type="number"
                        error={errors.code}
                        label="Code"
                        name='code'
                        value={values.code}
                        onChange={handleInputChange}
                    />
                            
                    { loading ? <Loading /> 
                        :<FormButton type='submit'>
                            Submit 
                        </FormButton>
                    } 
                </MainForm>
                 
            {/* </Container> */}
        </>
    )
}

export default KcseSubjectEditForm