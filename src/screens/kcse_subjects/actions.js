import axios from 'axios';
import { backend } from '../../proxy';

import {
    KCSE_SUBJECT_GET_REQUEST,
    KCSE_SUBJECT_GET_SUCCESS,
    KCSE_SUBJECT_GET_FAIL,

    KCSE_SUBJECT_CREATE_REQUEST,
    KCSE_SUBJECT_CREATE_SUCCESS,
    KCSE_SUBJECT_CREATE_FAIL,
    KCSE_SUBJECT_CREATE_RESET,

    KCSE_SUBJECT_DETAILS_REQUEST,
    KCSE_SUBJECT_DETAILS_SUCCESS,
    KCSE_SUBJECT_DETAILS_FAIL,

    KCSE_SUBJECT_UPDATE_REQUEST,
    KCSE_SUBJECT_UPDATE_SUCCESS,
    KCSE_SUBJECT_UPDATE_FAIL,

    KCSE_SUBJECT_DELETE_REQUEST,
    KCSE_SUBJECT_DELETE_SUCCESS,
    KCSE_SUBJECT_DELETE_FAIL,



} from './constants';

export const readKcseSubjects = (page, size) => async (dispatch) => {
    try {
        dispatch({ type: KCSE_SUBJECT_GET_REQUEST })
        const { data } = await axios.get(`${backend}/school/kcse_subjects/v1?page=${page}&size=${size}`)

        dispatch({
            type: KCSE_SUBJECT_GET_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type: KCSE_SUBJECT_GET_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })
    }
}



export const createKcseSubject = (kcseSubject) => async (dispatch, getState) => {
    try {
        dispatch({
            type: KCSE_SUBJECT_CREATE_REQUEST
        })

        const { data } = await axios.post(
            `${backend}/school/kcse_subjects/v1`,
            kcseSubject,
        )

        dispatch({
            type: KCSE_SUBJECT_CREATE_SUCCESS,
            success: true,
            payload: data
        })

    } catch (error) {
        dispatch({
            type: KCSE_SUBJECT_CREATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


export const kcseSubjectDetails = (id) => async (dispatch) => {
    try {
        dispatch({ type: KCSE_SUBJECT_DETAILS_REQUEST })
        const { data } = await axios.get(`${backend}/school/kcse_subjects/${id}`) //backend in package.json "http://127.0.0.1:8000/"

        dispatch({
            type: KCSE_SUBJECT_DETAILS_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type: KCSE_SUBJECT_DETAILS_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })
    }
}


export const updateKcseSubject = (kcseSubject) => async (dispatch) => {
    try {
        dispatch({
            type: KCSE_SUBJECT_UPDATE_REQUEST
        })

        const { data } = await axios.put(
            `${backend}/school/kcse_subjects/v1/${kcseSubject.id}`,
            kcseSubject,
        )

        dispatch({
            type: KCSE_SUBJECT_UPDATE_SUCCESS,
            payload: data
        })

        //update details
        dispatch({
            type: KCSE_SUBJECT_DETAILS_SUCCESS,
            payload: data
        })

    } catch (error) {
        dispatch({
            type: KCSE_SUBJECT_UPDATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


export const deleteKcseSubject = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: KCSE_SUBJECT_DELETE_REQUEST
        })

        const { data } = await axios.delete(
            `${backend}/school/kcse_subjects/v1/${id}`,
            //config
        )

        dispatch({
            type: KCSE_SUBJECT_DELETE_SUCCESS,
        })

    } catch (error) {
        dispatch({
            type: KCSE_SUBJECT_DELETE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}
