import React, {useEffect, useState} from 'react'
import { useDispatch, useSelector } from 'react-redux'

import Controls from '../../components/controls/Controls'
import ToastAlert from '../../components/display/ToastAlert'
import Loader from '../../components/display/Loader'

import { MainForm, useForm } from '../../components/useForm'
import { toTitleCase } from '../../utils/globalFunc'
import { FormButton } from '../../components/useForm/formElements';

import { styled, makeStyles } from '@mui/styles'
import { Avatar, Grid } from '@mui/material'
import { Button } from '../../components/controls/Button'
import { FiCamera } from 'react-icons/fi'



const Input = styled('input')({
    display: 'none',
  });

const useStyles = makeStyles(theme => ({
    image: {
        width: 170,
        height: 170,
        margin: "auto"
    }

}));

const defaultImageSrc = "/img/placeholder.jpg"


function StaffEditForm(props) {
    const classes = useStyles();
    const { handleEditStaff, recordForEdit, staffId } = props;
    const dispatch = useDispatch();

    const staffUpdate = useSelector(state => state.staffUpdate)
    const { loading, error, success } = staffUpdate

    
    const validate = (fieldValues = values) => {
        let temp = {...errors}
        if('role' in fieldValues)
            temp.role = fieldValues.role ? "" : "Role is Required"
        if('full_name' in fieldValues)
            temp.full_name = fieldValues.full_name ? "" : "Full Name is Required"
        if('department' in fieldValues)
            temp.department = fieldValues.department ? "" : "Department is Requied"
        if('description' in fieldValues)
            temp.description = fieldValues.description ? "" : "Description is Required"
       
        setErrors({ ...temp })

        // tests whether post array elements passes text implemented by validate() function
        if (fieldValues === values)
            return Object.values(temp).every(x => x === "")
        
    }

    const initialFValues = {
        full_name: '',
        role: '',
        department: '',
        description: '',
        image_src: defaultImageSrc,
        staff_image: ''
    }

    const { values,  
            setValues,
            errors, 
            setErrors, 
            handleResetForm, 
            handleInputChange } = useForm(initialFValues, true, validate);

    
    
    useEffect(() => {
        if(recordForEdit != null)
            setValues({
                ...recordForEdit
            })
    }, [dispatch,  recordForEdit, success, loading, error, setValues, staffId ])


     const submitHandler = (e) => {
        e.preventDefault()
        if (validate()) {
            const formData = new FormData()
            // formData.append('full_name', values.full_name)
            // formData.append('role', values.role)
            // formData.append('department', values.department)
            // formData.append('description', values.description)
            // formData.append('staff_image', values.staff_image)
            handleEditStaff(formData);
        }
    }

   
    return (
        
        <>       
            <MainForm onSubmit={submitHandler}>
                {loading ? <Loader /> 
                    :  (
                    <>
                        <Grid container>
                            <Grid item md={6} xs={12}>
                                
                                <Avatar className={classes.image} src={values.image_src}/>
                                    <label htmlFor="contained-button-file">
                                        <Input accept="image/*" 
                                        // error={errors.image_src}
                                        id="contained-button-file" 
                                        type="file"
                                        name="staff_image"
                                        onChange={handleInputChange}/>
                                        <Button className={classes.input} variant="contained" component="span">
                                            <FiCamera />  Upload Image
                                        </Button>
                                    </label>
                            </Grid>

                            <Grid item md={6}>
                                <Controls.InputField
                                    error={errors.full_name}
                                    label="Full Name"
                                    value={values.full_name}
                                    name='full_name'
                                    onChange={handleInputChange}>
                                </Controls.InputField> 

                                <Controls.InputField
                                    error={errors.role}
                                    label="Role"
                                    value={values.role}
                                    name='role'
                                    onChange={handleInputChange}>
                                </Controls.InputField>

                                <Controls.InputField
                                    error={errors.department}
                                    label="Department"
                                    value={values.department}
                                    name='department'
                                    onChange={handleInputChange}>
                                </Controls.InputField>

                                <Controls.InputField
                                    multiline
                                    rows={2}
                                    error={errors.description}
                                    label="Description"
                                    value={values.description}
                                    name='description'
                                    onChange={handleInputChange}>
                                </Controls.InputField>

                                    <FormButton type='submit'>
                                        Submit 
                                    </FormButton>
                            </Grid>
                        </Grid>
                    </>
                        
                )}  
            </MainForm>           
        </>
            
            
                       
    )
}

export default StaffEditForm
