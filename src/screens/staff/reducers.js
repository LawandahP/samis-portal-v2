import { 
    STAFF_CREATE_REQUEST,
    STAFF_CREATE_SUCCESS,
    STAFF_CREATE_FAIL,

    STAFF_READ_REQUEST,
    STAFF_READ_SUCCESS,
    STAFF_READ_FAIL,

    STAFF_DETAILS_REQUEST,
    STAFF_DETAILS_SUCCESS,
    STAFF_DETAILS_FAIL,
    STAFF_DETAILS_RESET,

    STAFF_UPDATE_REQUEST,
    STAFF_UPDATE_SUCCESS,
    STAFF_UPDATE_FAIL,
    STAFF_UPDATE_RESET,

    STAFF_DELETE_REQUEST,
    STAFF_DELETE_SUCCESS,
    STAFF_DELETE_FAIL,
    STAFF_DELETE_RESET

} from './constants';




export const staffCreateReducer = (state = { }, action) =>{
    switch(action.type) {
        case STAFF_CREATE_REQUEST:
            return {loading: true}
        
        case STAFF_CREATE_SUCCESS:
            return {loading: false, success: true, staff: action.payload.data}
        
        case STAFF_CREATE_FAIL:
            return {loading: false, error: action.payload}
        
        // case DEFAULT_USER_LOGOUT:
        //     return {}   
             
        default:
            return state
    }
}

export const staffReadReducer = (state = { staffs: []}, action) => {
    switch(action.type) {
        case STAFF_READ_REQUEST:
            return {
                loading: true,
                staffs: []
            }
        
        case STAFF_READ_SUCCESS:
            return {
                loading: false,
                staffs: action.payload.data.items,
                index:action.payload.index,
                totalPages: action.payload.data.page_count,
                size: action.payload.data.page_size
            }

        case STAFF_READ_FAIL:
            return {
                loading: false,
                error: action.payload,
            }
        
        default:
            return state
    }
}


export const staffDetailsReducer = (state = { staff: { } }, action) =>{
    switch(action.type) {
        case STAFF_DETAILS_REQUEST:
            return {loading: true, ...state}
        
        case STAFF_DETAILS_SUCCESS:
            return {loading: false, success:true, staff: action.payload.data}
        
        case STAFF_DETAILS_FAIL:
            return {loading: false, error: action.payload}
        
        case STAFF_DETAILS_RESET:
            return {}
    
        default:
            return state
    }
}


export const staffUpdateReducer = (state = { staff: {} }, action) =>{
    switch(action.type) {
        case STAFF_UPDATE_REQUEST:
            return {loading: true}
        
        case STAFF_UPDATE_SUCCESS:
            return {loading: false, success: true, staff: action.payload}
        
        case STAFF_UPDATE_FAIL:
            return {loading: false, error: action.payload}
        
        case STAFF_UPDATE_RESET:
            return {}
        default:
            return state
    }
}

export const staffDeleteReducer = (state = {}, action) =>{
    switch(action.type) {
        case STAFF_DELETE_REQUEST:
            return {loading: true}
        
        case STAFF_DELETE_SUCCESS:
            return {loading: false, success: true}
        
        case STAFF_DELETE_FAIL:
            return {loading: false, error: action.payload}

        case STAFF_DELETE_RESET:
            return {}
        default:
            return state
    }
}

