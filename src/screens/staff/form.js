import React, { useEffect, useState, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useLocation } from 'react-router-dom'

import Controls from '../../components/controls/Controls'
import ToastAlert from '../../components/display/ToastAlert'
import Loader from '../../components/display/Loader'

import { MainForm, useForm } from '../../components/useForm'
import { toTitleCase } from '../../utils/globalFunc'
import { FormButton, FormButtonWrapper, Text } from '../../components/useForm/formElements';

import { styled, makeStyles } from '@mui/styles'
import { Avatar, Grid, Button } from '@mui/material'

import { FiCamera } from 'react-icons/fi'

import { createStatus, readStatus } from '../stepper/status/actions';
import { processPathName } from '../../utils/globalFunc';





const defaultImageSrc = "/images/placeholder.png"

const Input = styled('input')({
    display: 'none',
});

const useStyles = makeStyles(theme => ({
    image: {
        width: 170,
        height: 170,
        margin: "auto"
    }

}));

const StaffForm = (props) => {
    const { handleStaffCreate, handleNext } = props;

    const dispatch = useDispatch()
    const location = useLocation();

    const classes = useStyles();

    const staffCreate = useSelector(state => state.staffCreate)
    const { error, loading, success: successCreateStaff } = staffCreate

    const [message, setMessage] = useState('')

    const validate = (fieldValues = values) => {
        let temp = { ...errors }
        if ('role' in fieldValues)
            temp.role = fieldValues.role ? "" : "Role is Required"
        if ('full_name' in fieldValues)
            temp.full_name = fieldValues.full_name ? "" : "Full Name is Required"
        if ('department' in fieldValues)
            temp.department = fieldValues.department ? "" : "Department is Requied"
        if ('description' in fieldValues)
            temp.description = fieldValues.description ? "" : "Description is Required"

        setErrors({ ...temp })

        // tests whether post array elements passes text implemented by validate() function
        if (fieldValues === values)
            return Object.values(temp).every(x => x === "")

    }

    const initialFValues = {
        full_name: '',
        role: '',
        department: '',
        description: '',
        image_src: defaultImageSrc,
        staff_image: ''
    }

    const {
        values,
        // setValues,
        errors,
        setErrors,
        handleResetForm,
        handleInputChange
    } = useForm(initialFValues, true, validate);

    const submitHandler = (e) => {
        e.preventDefault()
        if (validate()) {
            const formData = new FormData()
            formData.append('full_name', values.full_name)
            formData.append('role', values.role)
            formData.append('department', values.department)
            formData.append('description', values.description)
            formData.append('staff_image', values.staff_image)
            handleStaffCreate(formData, handleResetForm);
            handleResetForm()
        }
    }


    // Handle Next Step
    const [ count, setCount ] = useState(0);
    const incrementCount = () => {
        setCount(prevCount => prevCount + 1);
    };

    const statusRead = useSelector(state => state.statusRead)
    const {getStep} = statusRead
    let currentStep = getStep.step

    const detectStepChange = useMemo(() => {
        return stepChange(currentStep)
    }, [stepChange,])


    function stepChange(currentStep) {
        if(location.pathname === processPathName  && currentStep > 5)
            handleNext()
    }

    useEffect(() => {
        dispatch(readStatus())
        if (successCreateStaff) {
            setMessage('Staff Created Successfully')
            incrementCount();
        }
        if (count >= 3) {
            handleNext()
            setCount(0);
            dispatch(createStatus(6))
        }
    }, [successCreateStaff])


    return (
        <>
            {error && <ToastAlert severity='error'>{error}</ToastAlert>}
            {successCreateStaff && <ToastAlert>{message}</ToastAlert>}

            {location.pathname === processPathName ? 
                <Text>Submit At least 4 Staff Members: { count ? count : "0"}</Text>
                : ""
            }   


            <MainForm onSubmit={submitHandler}>
                <Grid container>
                    <Grid item md={6} xs={12}>
                        {/* <Image src={values.image_src} style={{width: "200px", height: "200px", textAlign: "center", borderRadius: "50%"}} alt="upload" fluid /> */}
                        <Avatar className={classes.image} src={values.image_src} />
                        <label htmlFor="contained-button-file">
                            <Input accept="image/*"
                                // error={errors.image_src}
                                id="contained-button-file"
                                type="file"
                                name="staff_image"
                                onChange={handleInputChange} />
                            <Button className={classes.input} variant="contained" component="span">
                                <FiCamera />  Upload Image
                            </Button>
                        </label>
                    </Grid>

                    <Grid item md={6}>
                        <Controls.InputField
                            error={errors.full_name}
                            label="Full Name"
                            value={values.full_name}
                            name='full_name'
                            onChange={handleInputChange}
                            onInput={(e) => e.target.value = toTitleCase(e.target.value)}>
                        </Controls.InputField>

                        <Controls.InputField
                            error={errors.role}
                            label="Role"
                            value={values.role}
                            name='role'
                            onChange={handleInputChange}>
                        </Controls.InputField>

                        <Controls.InputField
                            error={errors.department}
                            label="Department"
                            value={values.department}
                            name='department'
                            onChange={handleInputChange}>
                        </Controls.InputField>

                        <Controls.InputField
                            multiline
                            rows={3}
                            error={errors.description}
                            label="Description"
                            value={values.description}
                            name='description'
                            onChange={handleInputChange}>
                        </Controls.InputField>

                        { loading ? <Loader />
                            :   <FormButtonWrapper>
                                    <FormButton type='submit'>
                                        Submit
                                    </FormButton>
                                </FormButtonWrapper>
                        }
                    </Grid>
                </Grid>        
            </MainForm>
        </>
    );
};

export default StaffForm;
