import axios from 'axios';
import { backend, bff } from '../../proxy';

import { 
    STAFF_CREATE_REQUEST,
    STAFF_CREATE_SUCCESS,
    STAFF_CREATE_FAIL,

    STAFF_READ_REQUEST,
    STAFF_READ_SUCCESS,
    STAFF_READ_FAIL,

    STAFF_DETAILS_REQUEST,
    STAFF_DETAILS_SUCCESS,
    STAFF_DETAILS_FAIL,

    STAFF_UPDATE_REQUEST,
    STAFF_UPDATE_SUCCESS,
    STAFF_UPDATE_FAIL,

    STAFF_DELETE_REQUEST,
    STAFF_DELETE_SUCCESS,
    STAFF_DELETE_FAIL,

} from './constants';



export const createStaff = (staff) => async (dispatch, getState) => {
    try {
        dispatch({
            type: STAFF_CREATE_REQUEST
        })

        const config = {
            headers: {
                'Content-type':'multipart/form-data'
            }
        }

        const { data } = await axios.post(
            `${bff}/visitor/staff`,
            staff,
            config
        )

        dispatch({
            type: STAFF_CREATE_SUCCESS,
            success: true,
            payload: data
        })


    } catch(error) {
        dispatch({
            type: STAFF_CREATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


export const readStaff = (page, size) => async (dispatch) => {
    try {
        dispatch({
            type: STAFF_READ_REQUEST
        })

        const { data } = await axios.get(`${bff}/visitor/staff?page=${page}&size=${size}`)

        dispatch({
            type: STAFF_READ_SUCCESS,
            payload: data
        })

    } catch(error) {
        dispatch({
            type: STAFF_READ_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.message
        })

    }
}

export const staffDetails = (id) => async (dispatch) => {
    try {
        dispatch({ type: STAFF_DETAILS_REQUEST })
        const { data } = await axios.get(`${backend}/placeholder-staff/${id}`)

        dispatch({
            type:STAFF_DETAILS_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type:STAFF_DETAILS_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.message
        })
    }
}


export const updateStaff = (staff) => async (dispatch) => {
    try {
        dispatch({
            type: STAFF_UPDATE_REQUEST
        })
        
        const { data } = await axios.put(
            `${backend}/placeholder-staff/${staff.id}`,
            staff,
        )

        dispatch({
            type: STAFF_UPDATE_SUCCESS,
            payload: data
        })

        //update details
        dispatch({
            type: STAFF_DETAILS_SUCCESS,
            payload: data
        })

    } catch(error) {
        dispatch({
            type: STAFF_UPDATE_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.message
        })

    }
}


export const deleteStaff = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: STAFF_DELETE_REQUEST
        })
        
        const { data } = await axios.delete(
            `${backend}/placeholder-staff/${id}`,
            //config
        )

        dispatch({
            type: STAFF_DELETE_SUCCESS,
        })

    } catch(error) {
        dispatch({
            type: STAFF_DELETE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}
