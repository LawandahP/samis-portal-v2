import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { makeStyles } from '@mui/styles'
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import { FiDelete, FiEdit3 } from 'react-icons/fi'

import StaffForm from './form'
import StaffEditForm from './editform'
import { createStaff, deleteStaff, readStaff, updateStaff } from './actions'
import { TabTitle, toTitleCase } from '../../utils/globalFunc'

import Controls from '../../components/controls/Controls'
import ToastAlert from '../../components/display/ToastAlert'
import TableComponent from '../../components/useTable/index'
import { Chip, TableTop } from '../../components/useTable/elements'
import { ActionButtonWrapper } from '../../components/controls/Button'
import ConfirmDialog from '../../components/display/dialog'
import Modal from '../../components/display/modal'
import { NgPageContainer, NgPaper } from '../../components/display/elements'
import Loader from '../../components/display/Loader';
import { Avatar } from '@mui/material';



const headCells = [
    { id: 'image_url', label: '', disableSorting: true},
    { id: 'full_name', label: 'Staff Name', minWidth: 170 },
    { id: 'role', label: 'Role', minWidth: 200 },
    { id: 'department', label: 'Department', minWidth: 250, disableSorting: false },
    { id: 'description', label: 'Description', minWidth: 270 , disableSorting: true},
    { id: 'actions', label: 'Actions', minWidth: 150 , disableSorting: true },

]


function StaffReadScreen(props) {
    TabTitle('Staff - Samis Systems')

    // const { handleNext } = props;
    
    const dispatch = useDispatch();

    const [ search, setSearch ] = useState({fn:items => {return items;}})
    const [ recordForEdit, setRecordForEdit ] = useState(null);

    const [ openPopup, setOpenPopup ] = useState(false)
    const [ openModal, setOpenModal ] = useState(false)

    const staffRead = useSelector(state => state.staffRead)
    const { loading, error, staffs } = staffRead

    const staffCreate = useSelector(state => state.staffCreate)
    const { success: successCreate } = staffCreate

    const staffUpdate = useSelector(state => state.staffUpdate)
    const { success: successUpdate } = staffUpdate

    const staffDelete = useSelector(state => state.staffDelete)
    const { success: successDelete } = staffDelete

    const [ message, setMessage ] = useState('')

    const [ confirmDialog, setConfirmDialog ] = useState({isOpen: false, title: '', subTitle: ''})

    const { TblContainer, 
            TblHead, 
            TblPagination, 
            recordsAfterPaginatingAndSorting,
            page, rowsPerPage
        } = TableComponent (staffs, headCells, search)
    
    useEffect(() => {
        dispatch(readStaff(page, rowsPerPage))
        if(successCreate) {
            setMessage('Staff Created Successfully')
        } else if (successUpdate) {
            setMessage('Staff record updated Successfully')
        }
        if (successDelete)
            setMessage('Staff record Deleted Successfully')
    }, [dispatch, successCreate, successUpdate, successDelete, page, rowsPerPage])

    const handleSearch = (e) => {
        e.preventDefault()
        let target = e.target
        setSearch({
            fn:items => {
                if(target.value === "")
                    return items;
                else
                    return items.filter(x => x.full_name.includes(toTitleCase(e.target.value)))
            }
        })
    }


    const handleStaffCreate = (staff, handleResetForm) => {
        dispatch(createStaff(staff))
        if(successCreate)
            handleResetForm()
            setOpenModal(false);
    }

    const handleEditStaff = (staff, handleResetForm) => {
        dispatch(updateStaff(staff))
        if(successUpdate)
            handleResetForm()
            setOpenPopup(false);  
    }

    const editHandler = (staff) => { 
        setRecordForEdit(staff)
        setOpenPopup(true)
    }

    const deleteHandler = (id) => {
        setConfirmDialog({
            ...confirmDialog,
            isOpen: false
        })
        dispatch(deleteStaff(id))  
    }

    return (
        
           
                    
            <NgPageContainer>   
                { message && <ToastAlert severity="success">{message}</ToastAlert>}
                <NgPaper>
                    <TableTop>
                        <Controls.SearchInputField
                            label="Search Staff"
                            onChange={handleSearch}
                        />
                        <Controls.AddButton
                            onClick={() => setOpenModal(true)}
                        >
                        </Controls.AddButton>
                    </TableTop>    
                    
                               
                    { loading
                    ? <Loader />
                    : error
                    ? <ToastAlert severity="error">{error}</ToastAlert>
                    : (
                        <>                        
                        
                            <TblContainer>
                                <TblHead />

                                <TableBody>
                                {
                                    recordsAfterPaginatingAndSorting()?.map(staff => 
                                        (<TableRow key={staff.full_name}>
                                            <TableCell><Avatar src={staff.image_url} /></TableCell>
                                            <TableCell>{staff.full_name}</TableCell>
                                            <TableCell>{staff.role}</TableCell>
                                            <TableCell>{staff.department}</TableCell>
                                            <TableCell>{staff.description}</TableCell>
                                            <TableCell>
                                                <ActionButtonWrapper>
                                                    <Controls.ActionButton
                                                        title="edit"
                                                        onClick={() => editHandler(staff)}
                                                        edit>
                                                        <FiEdit3 />
                                                    </Controls.ActionButton>
                                                    <Controls.ActionButton
                                                        title="deactivate"
                                                        onClick={() => {
                                                            setConfirmDialog({
                                                                isOpen: true,
                                                                title: "Are you sure you want to delete this Staff Member?",
                                                                subTitle: "You can't undo this operation",
                                                                onConfirm: () => { deleteHandler(staff.id) }
                                                            })
                                                        }}>
                                                        <FiDelete />
                                                    </Controls.ActionButton>
                                                </ActionButtonWrapper>
                                            </TableCell>
                                                                        
                                        </TableRow>)
                                )}
                                </TableBody>
                            </TblContainer>
                            <TblPagination />
                        </>
                        
                    )}
                </NgPaper> 
                                
                <Modal
                    openModal = {openModal}
                    setOpenModal = {setOpenModal}
                    title="Create Staff"
                >
                    <StaffForm 
                        handleStaffCreate = {handleStaffCreate}/>  
                </Modal>  

                <Modal
                    openPopup = {openPopup}
                    setOpenPopup = {setOpenPopup}
                    title="Edit Staff"
                >
                    <StaffEditForm
                        recordForEdit = {recordForEdit}
                        handleEditStaff = {handleEditStaff}
                    />
                </Modal> 
                
                <ConfirmDialog 
                    confirmDialog={confirmDialog}
                    setConfirmDialog={setConfirmDialog} /> 
                
            </NgPageContainer>          
        
    )
}

export default StaffReadScreen
