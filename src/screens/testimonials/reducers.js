
import { 
    TESTIMONIAL_READ_REQUEST,
    TESTIMONIAL_READ_SUCCESS,
    TESTIMONIAL_READ_FAIL,

    TESTIMONIAL_CREATE_REQUEST,
    TESTIMONIAL_CREATE_SUCCESS,
    TESTIMONIAL_CREATE_FAIL,
    TESTIMONIAL_CREATE_RESET,

    TESTIMONIAL_DETAILS_REQUEST,
    TESTIMONIAL_DETAILS_SUCCESS,
    TESTIMONIAL_DETAILS_FAIL,
    TESTIMONIAL_DETAILS_RESET,

    TESTIMONIAL_UPDATE_REQUEST,
    TESTIMONIAL_UPDATE_SUCCESS,
    TESTIMONIAL_UPDATE_FAIL,
    TESTIMONIAL_UPDATE_RESET,

    TESTIMONIAL_DELETE_REQUEST,
    TESTIMONIAL_DELETE_SUCCESS,
    TESTIMONIAL_DELETE_FAIL,
    TESTIMONIAL_DELETE_RESET
} from './constants';



export const testimonialReadReducer = (state = { testimonials:[] }, action) =>{
    switch(action.type) {
        case TESTIMONIAL_READ_REQUEST:
            return {loading: true, testimonials:[]}
        
        case TESTIMONIAL_READ_SUCCESS:
            return {
                        loading: false,
                        testimonials: action.payload.data.items,
                        index:action.payload.index,
                        totalPages: action.payload.data.page_count,
                        size: action.payload.data.page_size
                    }
        
        case TESTIMONIAL_READ_FAIL:
            return {loading: false, error: action.payload}
        
        default:
            return state
    }
}


export const testimonialCreateReducer = (state = {}, action) =>{
    switch(action.type) {
        case TESTIMONIAL_CREATE_REQUEST:
            return {loading: true}
        
        case TESTIMONIAL_CREATE_SUCCESS:
            return {loading: false, success: true, testimonial: action.payload.data.items}
        
        case TESTIMONIAL_CREATE_FAIL:
            return {loading: false, error: action.payload}
        
        case TESTIMONIAL_CREATE_RESET:
            return {}
        default:
            return state
    }
}


export const testimonialDetailsReducer = (state = { testimonial: { } }, action) =>{
    switch(action.type) {
        case TESTIMONIAL_DETAILS_REQUEST:
            return {loading: true, ...state}
        
        case TESTIMONIAL_DETAILS_SUCCESS:
            return {loading: false, success:true, testimonial: action.payload.data}
        
        case TESTIMONIAL_DETAILS_FAIL:
            return {loading: false, error: action.payload}
        
        case TESTIMONIAL_DETAILS_RESET:
            return {}
    
        default:
            return state
    }
}


export const testimonialUpdateReducer = (state = { testimonial: {} }, action) =>{
    switch(action.type) {
        case TESTIMONIAL_UPDATE_REQUEST:
            return {loading: true}
        
        case TESTIMONIAL_UPDATE_SUCCESS:
            return {loading: false, success: true, testimonial: action.payload}
        
        case TESTIMONIAL_UPDATE_FAIL:
            return {loading: false, error: action.payload}
        
        case TESTIMONIAL_UPDATE_RESET:
            return {}
        default:
            return state
    }
}

export const testimonialDeleteReducer = (state = {}, action) =>{
    switch(action.type) {
        case TESTIMONIAL_DELETE_REQUEST:
            return {loading: true}
        
        case TESTIMONIAL_DELETE_SUCCESS:
            return {loading: false, success: true}
        
        case TESTIMONIAL_DELETE_FAIL:
            return {loading: false, error: action.payload}

        case TESTIMONIAL_DELETE_RESET:
            return {}
        default:
            return state
    }
}

