import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { createTestimonial, deleteTestimonial, readTestimonials, updateTestimonial } from './actions'
import { makeStyles } from '@mui/styles'

import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import TestimonialForm from './form'
import TestimonialEditForm from './editform'

import { Avatar, Container, Grid } from '@mui/material'
// import { TESTIMONIAL_CREATE_RESET } from './testimonialConstants'

import { NgPageContainer, NgPaper } from '../../components/display/elements';
import { FiDelete, FiEdit3 } from 'react-icons/fi';
import Loader from '../../components/display/Loader';
import { TabTitle, toTitleCase } from '../../utils/globalFunc'
import Controls from '../../components/controls/Controls';
import Modal from '../../components/display/modal';
import TableComponent from '../../components/useTable';
import { TableTop } from '../../components/useTable/elements';
import ToastAlert from '../../components/display/ToastAlert';
import { ActionButtonWrapper } from '../../components/controls/Button';
import ConfirmDialog from '../../components/display/dialog';

const useStyles = makeStyles(theme => ({
    searchInput: {
        width : "85%",
    },
    newButton: {
        position: "absolute",
        right: "10px",
        margin: "1rem"
    },
    paper: {
        width: "100%",
        overflow: "hidden",
    },
    
}))
const headCells = [
    { id: 'image_url', label: '', disableSorting: true},
    { id: 'name', label: 'Name', minWidth: 170 },
    { id: 'role', label: 'Role', minWidth: 170 },
    { id: 'info', label: 'info', minWidth: 400, disableSorting: true },
    { id: 'actions', label: 'Actions', minWidth: 200 , disableSorting: true },

]


function TestimonialReadScreen() {
    TabTitle('Testimonials - Samis Systems')
    const dispatch = useDispatch();
    const classes = useStyles();

    const [ search, setSearch ] = useState({fn:items => {return items;}})
    const [ recordForEdit, setRecordForEdit ] = useState(null);

    const [ openPopup, setOpenPopup ] = useState(false)
    const [ openModal, setOpenModal ] = useState(false)

    const testimonialRead = useSelector(state => state.testimonialRead)
    const { loading, error, testimonials } = testimonialRead

    const testimonialCreate = useSelector(state => state.testimonialCreate)
    const { success: successCreate } = testimonialCreate

    const testimonialUpdate = useSelector(state => state.testimonialUpdate)
    const { success: successUpdate } = testimonialUpdate

    const testimonialDelete = useSelector(state => state.testimonialDelete)
    const { success: successDelete } = testimonialDelete

    

    const [ message, setMessage ] = useState('')

    const [ confirmDialog, setConfirmDialog ] = useState({isOpen: false, title: '', subTitle: ''})

    const { TblContainer, 
            TblHead, 
            TblPagination, 
            recordsAfterPaginatingAndSorting,
            page, rowsPerPage
        } = TableComponent (testimonials, headCells, search)
    
    useEffect(() => {
        dispatch(readTestimonials(page, rowsPerPage))
        if(successCreate) {
            setMessage('Testimonial Created Successfully')
        } else {
            if(successUpdate) {
                setMessage('Testimonial Updated Successfully')
            }
        }
        if(successDelete) {
            setMessage('Testimonial Deleted Successfully')
        }

    }, [dispatch, successCreate, successUpdate, successDelete, page, rowsPerPage])

    const handleSearch = (e) => {
        e.preventDefault()
        let target = e.target
        setSearch({
            fn:items => {
                if(target.value === "")
                    return items;
                else
                    return items.filter(x => x.name.includes(toTitleCase(e.target.value)))
            }
        })
    }


    const handleTestimonialCreate = (testimonial, handleResetForm) => {
        dispatch(createTestimonial(testimonial))
    //     if(successCreate)
    //         handleResetForm()
    //         setOpenModal(false);
    }

    const editEntry = (testimonial, handleResetForm) => {
        dispatch(updateTestimonial(testimonial))
        setOpenPopup(false);
        handleResetForm()   
    }

    const editHandler = (testimonial) => { 
        setRecordForEdit(testimonial)
        setOpenPopup(true)
    }

    const deleteHandler = (id) => {
        setConfirmDialog({
            ...confirmDialog,
            isOpen: false
        })
        dispatch(deleteTestimonial(id))  
    }

    return (
        <>
            <NgPageContainer>  
                <NgPaper>
                    { message && <ToastAlert severity="success">{message}</ToastAlert>} 
                    <TableTop>
                        <Controls.SearchInputField
                            label="Search Testimonials"
                            onChange={handleSearch}
                        />
                        <Controls.AddButton
                            onClick={() => setOpenModal(true)}
                        >
                        </Controls.AddButton>
                    </TableTop>

                    { loading
                    ? <Loader />
                    : error
                    ? <ToastAlert severity="error">{error}</ToastAlert>
                    : (
                        <>            
                            <TblContainer>
                                <TblHead />

                                <TableBody>
                                {
                                    recordsAfterPaginatingAndSorting()?.map(testimonial => 
                                        (<TableRow key={testimonial.id}>
                                            <TableCell><Avatar src={testimonial.image_url} /></TableCell>
                                            <TableCell>{testimonial.name}</TableCell>
                                            <TableCell>{testimonial.role}</TableCell>
                                            <TableCell>{testimonial.info}</TableCell>
                                            <TableCell>
                                                <ActionButtonWrapper>
                                                    <Controls.ActionButton
                                                        title="edit"
                                                        onClick={() => editHandler(testimonial)}
                                                        edit>
                                                        <FiEdit3 />
                                                    </Controls.ActionButton>
                                                    <Controls.ActionButton
                                                        title="deactivate"
                                                        onClick={() => {
                                                            setConfirmDialog({
                                                                isOpen: true,
                                                                title: "Are you sure you want to delete this Testimonial?",
                                                                subTitle: "You can't undo this operation",
                                                                onConfirm: () => { deleteHandler(testimonial.id) }
                                                            })
                                                        }}>
                                                        <FiDelete />
                                                    </Controls.ActionButton>
                                                </ActionButtonWrapper>
                                            </TableCell>
                                                                        
                                        </TableRow>)
                                )}
                                </TableBody>
                            </TblContainer>
                            <TblPagination />
                        </>
                    )}
                </NgPaper> 
                               
                
                <Modal
                    openModal = {openModal}
                    setOpenModal = {setOpenModal}
                    title="Create Testimonial"
                >
                    <TestimonialForm 
                        recordForEdit = {recordForEdit}
                        handleTestimonialCreate = {handleTestimonialCreate}/>  
                </Modal>  

                <Modal
                    openPopup = {openPopup}
                    setOpenPopup = {setOpenPopup}
                    title="Edit Testimonial"
                >
                    <TestimonialEditForm
                        recordForEdit = {recordForEdit}
                        editEntry = {editEntry}
                    />
                </Modal> 
                
                <ConfirmDialog 
                    confirmDialog={confirmDialog}
                    setConfirmDialog={setConfirmDialog} />
                
            </NgPageContainer>
        </>
        
    )
}

export default TestimonialReadScreen
