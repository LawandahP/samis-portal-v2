import React, {useEffect} from 'react'

import { useDispatch, useSelector } from 'react-redux';


import { MainForm, useForm } from '../../components/useForm'

import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import { styled } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';
import { Avatar } from '@mui/material';
import Loader from '../../components/display/Loader';
import ToastAlert from '../../components/display/ToastAlert';
import Controls from '../../components/controls/Controls';
import { FormButton } from '../../components/useForm/formElements';



const Input = styled('input')({
    display: 'none',
  });

const useStyles = makeStyles(theme => ({
    root: {
        // display: "flex",
        justifyContent: 'center',
        alignItems: 'center',
        // height: "100vh",
        textAlign: "center",
        // paddingTop: theme.spacing(2)
    },
    input: {
        marginTop: "10px",
        textAlign: "center"
    },

    image: {
        width: 170,
        height: 170,
        margin: "auto"
    }
}))

const defaultImageSrc = "/img/placeholder.jpg"


function TestimonialEditForm(props) {
    const classes = useStyles();
    const { editEntry, recordForEdit, testimonialId } = props;
    const dispatch = useDispatch();

    const testimonialUpdate = useSelector(state => state.testimonialUpdate)
    const { loading, error, success } = testimonialUpdate

    // const [successMessage, setSuccessMessage] = useState('')
    
    const validate = (fieldValues = values) => {
        let temp = {...errors}
        if('name' in fieldValues)
            temp.name = fieldValues.name ? "" : "Testimonial Giver Name is Required"
        if('role' in fieldValues)
            temp.role = fieldValues.role ? "" : "Role is Required"
        if('info' in fieldValues)
            temp.info = fieldValues.info ? "" : "Info is Required"
        // if('image_src' in fieldValues)
        //     temp.image_src = fieldValues.image_src === defaultImageSrc ? "" : "No image chosen"
        // temp.phone_no = (\\+?\\d{9,13}).test(values.phone_no) ? "" : "Student Name is Required"
        setErrors({ ...temp })

        // tests whether post array elements passes text implemented by validate() function
        if (fieldValues === values)
            return Object.values(temp).every(x => x === "")
        
    }

    const initialFValues = {
        id: 0,
        name: '',
        role: '',
        info: '',
        image_name: '',
        image_src: defaultImageSrc,
        image_file: '',
    }

    const { values,  
            setValues,
            errors, 
            setErrors, 
            handleResetForm, 
            handleInputChange } = useForm(initialFValues, true, validate);

    
    
    useEffect(() => {
        if(recordForEdit != null)
            setValues({
                ...recordForEdit
            })
    }, [dispatch,  recordForEdit, success, loading, error, setValues, testimonialId ])


    const submitHandler = (e) => {
        e.preventDefault()
        if (validate()) {
            // const formData = new FormData()
            // formData.append('name', values.name)
            // formData.append('role', values.role)
            // formData.append('info', values.info)
            // formData.append('image_name', values.image_name)
            // formData.append('image_file', values.image_file)
            editEntry(values, handleResetForm);
            handleResetForm()
        }
    }

   
    return (
        
        <div className={classes.root}>
            { loading && <Loader />}
            { error && <ToastAlert severity="error">{error}</ToastAlert>}
                <MainForm onSubmit={submitHandler}>
                    <Grid container>
                        <Grid item md={6} xs={12}>
                            <Avatar className={classes.image} src={values.image_src}/>

                            <label htmlFor="contained-button-file">
                                <Input accept="image/*" 
                                    id="contained-button-file" 
                                    type="file"
                                    onChange={handleInputChange}/>
                                    <Button className={classes.input} variant="contained" component="span">
                                        Upload image
                                    </Button>
                            </label>
                        </Grid>

                        <Grid item md={6} xs={12}>
                            <Controls.InputField
                                error={errors.name}
                                label="Name"
                                value={values.name}
                                name='name'
                                onChange={handleInputChange}
                            />
                            <Controls.InputField
                                error={errors.role}
                                label="Role"
                                value={values.role}
                                name='role'
                                onChange={handleInputChange}
                            />
                            <Controls.InputField 
                                multiline="true"
                                error={errors.info}
                                label="Info"
                                name='info'
                                value={values.info}
                                onChange={handleInputChange}
                            />
                            <Controls.InputField
                                // error={errors.image}
                                label="Image Name"
                                name='image_name'
                                value={values.image_name}
                                onChange={handleInputChange}
                            />
                            { loading ? <Loader /> 
                                :<FormButton type='submit'>
                                    Submit 
                                </FormButton>
                            }
                        </Grid>
                    </Grid>
                </MainForm>
        </div>
        
            
            
                       
    )
}

export default TestimonialEditForm
