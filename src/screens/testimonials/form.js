import React, {useEffect, useState, useMemo} from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom'

import Grid from '@mui/material/Grid';
import { styled } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';
import { Avatar } from '@mui/material';
import Loader from '../../components/display/Loader';
import ToastAlert from '../../components/display/ToastAlert';
import Controls from '../../components/controls/Controls';
import { FormButton, FormButtonWrapper, Text } from '../../components/useForm/formElements';
import { MainForm, useForm } from '../../components/useForm'

import { createStatus, readStatus } from '../stepper/status/actions';
import { processPathName } from '../../utils/globalFunc';


const Input = styled('input')({
    display: 'none',
  });

const useStyles = makeStyles(theme => ({
    input: {
        marginTop: "10px",
        textAlign: "center"
    },

    image: {
        width: 170,
        height: 170,
        margin: "auto"
    }
}))

const defaultImageSrc = "/img/placeholder.jpg"


function TestimonialForm(props) {
    const classes = useStyles();
    const { handleTestimonialCreate, handleNext } = props;

    const dispatch = useDispatch();
    const location = useLocation();

    const testimonialCreate = useSelector(state => state.testimonialCreate)
    const { loading, error, success: successCreateTestimonial } = testimonialCreate

    const [message, setMessage] = useState('')
    
    const validate = (fieldValues = values) => {
        let temp = {...errors}
        if('name' in fieldValues)
            temp.name = fieldValues.name ? "" : "Testimonial Giver Name is Required"
        if('role' in fieldValues)
            temp.role = fieldValues.role ? "" : "Role is Required"
        if('info' in fieldValues)
            temp.info = fieldValues.info ? "" : "Info is Required"
        // if('image_src' in fieldValues)
        //     temp.image_src = fieldValues.image_src === defaultImageSrc ? "" : "No image chosen"
        // temp.phone_no = (\\+?\\d{9,13}).test(values.phone_no) ? "" : "Student Name is Required"
        setErrors({ ...temp })

        // tests whether post array elements passes text implemented by validate() function
        if (fieldValues === values)
            return Object.values(temp).every(x => x === "")
        
    }

    const initialValues = {
        name: '',
        role: '',
        info: '',
        image_src: defaultImageSrc,
        image_file: '',
    }

    const { values,  
            setValues,
            errors, 
            setErrors, 
            handleResetForm, 
            handleInputChange } = useForm(initialValues, true, validate);

    const submitHandler = (e) => {
        e.preventDefault()
        if (validate()) {
            const formData = new FormData()
            formData.append('name', values.name)
            formData.append('role', values.role)
            formData.append('info', values.info)
            formData.append('image_file', values.image_file)
            handleTestimonialCreate(formData, handleResetForm);
        }
    }

    // Handle Next Step
    const [ count, setCount ] = useState(0);
    const incrementCount = () => {
        setCount(prevCount => prevCount + 1);
    };
    
    const statusRead = useSelector(state => state.statusRead)
    const {getStep} = statusRead
    let currentStep = getStep.step
    
    const detectStepChange = useMemo(() => {
        return stepChange(currentStep)
    }, [stepChange])

    function stepChange(currentStep) {
        if(location.pathname === processPathName && currentStep > 7)
            handleNext()
    }

    useEffect(() => {
        dispatch(readStatus())
        if(successCreateTestimonial) {
            setMessage('Testimonial Created Successfully')
            incrementCount();
        }
        if (count >= 3) {
            dispatch(createStatus(8))
            setCount(0);
            // history.push('/login')
        }      
    }, [dispatch, successCreateTestimonial])

   
    return (
        <>
            { error && <ToastAlert severity="error">{error}</ToastAlert>}
                <MainForm onSubmit={submitHandler}>
                    {location.pathname === processPathName ? 
                        <ToastAlert severity="info">Submit At least 4 Testimonials to proceed: { count ? count : "0"}</ToastAlert>
                        : ""
                    } 
                    <Grid container>
                        <Grid item md={6} xs={12}>
                            <Avatar className={classes.image} src={values.image_src}/>

                            <label htmlFor="contained-button-file">
                                <Input accept="image/*" 
                                    id="contained-button-file" 
                                    type="file"
                                    name="image_file"
                                    onChange={handleInputChange}
                                />
                                    <FormButton variant="contained" component="span">
                                        Upload image
                                    </FormButton>
                            </label>
                        </Grid>

                        <Grid item md={6} xs={12}>
                            <Controls.InputField
                                error={errors.name}
                                label="Name"
                                value={values.name}
                                name='name'
                                onChange={handleInputChange}
                            />
                            <Controls.InputField
                                error={errors.role}
                                label="Role"
                                value={values.role}
                                name='role'
                                onChange={handleInputChange}
                            />
                            <Controls.InputField 
                                multiline="true"
                                error={errors.info}
                                label="Info"
                                name='info'
                                value={values.info}
                                onChange={handleInputChange}
                            />

                        { loading ? <Loader />
                            :   <FormButtonWrapper>
                                    <FormButton type='submit'>
                                        Submit
                                    </FormButton>
                                </FormButtonWrapper>
                        }
                        </Grid>
                    </Grid>
                </MainForm>
        </>
        
            
            
                       
    )
}

export default TestimonialForm
