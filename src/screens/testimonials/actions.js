import axios from 'axios';
import { backend, bff } from '../../proxy';

import { 
    TESTIMONIAL_READ_REQUEST,
    TESTIMONIAL_READ_SUCCESS,
    TESTIMONIAL_READ_FAIL,

    TESTIMONIAL_CREATE_REQUEST,
    TESTIMONIAL_CREATE_SUCCESS,
    TESTIMONIAL_CREATE_FAIL,
    // TESTIMONIAL_CREATE_RESET,

    TESTIMONIAL_DETAILS_REQUEST,
    TESTIMONIAL_DETAILS_SUCCESS,
    TESTIMONIAL_DETAILS_FAIL,

    TESTIMONIAL_UPDATE_REQUEST,
    TESTIMONIAL_UPDATE_SUCCESS,
    TESTIMONIAL_UPDATE_FAIL,

    TESTIMONIAL_DELETE_REQUEST,
    TESTIMONIAL_DELETE_SUCCESS,
    TESTIMONIAL_DELETE_FAIL,

} from './constants';



export const createTestimonial = (testimony) => async (dispatch, getState) => {
    try {
        dispatch({
            type: TESTIMONIAL_CREATE_REQUEST
        })

        const config = {
            headers: {
                'Content-type':'multipart/form-data',
            }
        }
        
        const { data } = await axios.post(
            `${bff}/testimonials`,
            testimony,
            config
        )
        // const { data } = await axios.get(`${testimonial}`)
        dispatch({
            type: TESTIMONIAL_CREATE_SUCCESS,
            success: true,
            payload: data
        })

    } catch(error) {
        dispatch({
            type: TESTIMONIAL_CREATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}

export const readTestimonials = (page, size) => async (dispatch) => {
    try {
        dispatch({ type: TESTIMONIAL_READ_REQUEST })
        const { data } = await axios.get(`${bff}/testimonials?page=${page}&size=${size}`)

        dispatch({
            type:TESTIMONIAL_READ_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type:TESTIMONIAL_READ_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })
    }
}

export const testimonialDetails = (id) => async (dispatch) => {
    try {
        dispatch({ type: TESTIMONIAL_DETAILS_REQUEST })
        const { data } = await axios.get(`${backend}/testimonials/v1/${id}`)

        dispatch({
            type:TESTIMONIAL_DETAILS_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type:TESTIMONIAL_DETAILS_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })
    }
}


export const updateTestimonial = (testimony) => async (dispatch) => {
    try {
        dispatch({
            type: TESTIMONIAL_UPDATE_REQUEST
        })
        
        const { data } = await axios.put(
            `${backend}/testimonials/v1/${testimony.id}`,
            testimony,
        )

        dispatch({
            type: TESTIMONIAL_UPDATE_SUCCESS,
            payload: data
        })

        //update details
        dispatch({
            type: TESTIMONIAL_DETAILS_SUCCESS,
            payload: data
        })

    } catch(error) {
        dispatch({
            type: TESTIMONIAL_UPDATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


export const deleteTestimonial = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: TESTIMONIAL_DELETE_REQUEST
        })
        
        const { data } = await axios.delete(
            `${backend}/testimonials/v1/${id}`,
            //config
        )

        dispatch({
            type: TESTIMONIAL_DELETE_SUCCESS,
        })

    } catch(error) {
        dispatch({
            type: TESTIMONIAL_DELETE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


// export const uploadTestimonialImage = () => async (dispatch) => {
//     try {
//         dispatch({ type: TESTIMONIAL_READ_REQUEST })
//         const { data } = await axios.get(`${backend}/images/upload`)

//         dispatch({
//             type:TESTIMONIAL_READ_SUCCESS,
//             payload: data
//         })
//     } catch (error) {
//         dispatch({
//             type:TESTIMONIAL_READ_FAIL,
//             payload: error.response && error.response.data.detail
//                 ? error.response.data.detail
//                 : error.message
//         })
//     }
// }