
import { 
    STREAM_READ_REQUEST,
    STREAM_READ_SUCCESS,
    STREAM_READ_FAIL,

    STREAM_CREATE_REQUEST,
    STREAM_CREATE_SUCCESS,
    STREAM_CREATE_FAIL,
    STREAM_CREATE_RESET,

    STREAM_DETAILS_REQUEST,
    STREAM_DETAILS_SUCCESS,
    STREAM_DETAILS_FAIL,

    STREAM_UPDATE_REQUEST,
    STREAM_UPDATE_SUCCESS,
    STREAM_UPDATE_FAIL,

    STREAM_DELETE_REQUEST,
    STREAM_DELETE_SUCCESS,
    STREAM_DELETE_FAIL,

} from './constants';


export const streamCreateReducer = (state = {}, action) =>{
    switch(action.type) {
        case STREAM_CREATE_REQUEST:
            return {loading: true}
        
        case STREAM_CREATE_SUCCESS:
            return {loading: false, success: true, stream: action.payload.data.items}
        
        case STREAM_CREATE_FAIL:
            return {loading: false, error: action.payload}
        
        case STREAM_CREATE_RESET:
            return {}
        default:
            return state
    }
}

export const streamReadReducer = (state = { streams:[] }, action) =>{
    switch(action.type) {
        case STREAM_READ_REQUEST:
            return {loading: true, streams:[]}
        
        case STREAM_READ_SUCCESS:
            return {
                        loading: false,
                        streams: action.payload.data.items,
                        index:action.payload.index,
                        totalPages: action.payload.data.page_count,
                        size: action.payload.data.page_size
                    }
        
        case STREAM_READ_FAIL:
            return {loading: false, error: action.payload}
        
        default:
            return state
    }
}




export const streamUpdateReducer = (state = { stream: {} }, action) =>{
    switch(action.type) {
        case STREAM_UPDATE_REQUEST:
            return {loading: true}
        
        case STREAM_UPDATE_SUCCESS:
            return {loading: false, success: true, stream: action.payload}
        
        case STREAM_UPDATE_FAIL:
            return {loading: false, error: action.payload}
        
        default:
            return state
    }
}

export const streamDetailsReducer = (state = { stream: { } }, action) =>{
    switch(action.type) {
        case STREAM_DETAILS_REQUEST:
            return {loading: true, ...state}
        
        case STREAM_DETAILS_SUCCESS:
            return {loading: false, success: true, stream: action.payload.data}
        
        case STREAM_DETAILS_FAIL:
            return {loading: false, error: action.payload}
        
        default:
            return state
    }
}


export const streamDeleteReducer = (state = {}, action) =>{
    switch(action.type) {
        case STREAM_DELETE_REQUEST:
            return {loading: true}
        
        case STREAM_DELETE_SUCCESS:
            return {loading: false, success: true}
        
        case STREAM_DELETE_FAIL:
            return {loading: false, error: action.payload}
        
        default:
            return state
    }
}

