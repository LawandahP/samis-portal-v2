import React, {useEffect} from 'react'
import { Grid } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';

import Controls from '../../components/controls/Controls'
import ToastAlert from '../../components/display/ToastAlert'
import Loading from '../../components/display/Loader'

import { MainForm, useForm } from '../../components/useForm'
import { toTitleCase } from '../../utils/globalFunc'
import { FormButton } from '../../components/useForm/formElements';
import Loader from '../../components/display/Loader';

function StreamForm(props) {
    const { newEntry } = props;
    const dispatch = useDispatch();

    const streamCreate = useSelector(state => state.streamCreate)
    const { loading, error, success } = streamCreate

    // const [successMessage, setSuccessMessage] = useState('')
    
    const validate = (fieldValues = values) => {
        let temp = {...errors}
        if('name' in fieldValues)
            temp.name = fieldValues.name ? "" : "Stream Name is Required"
        // if('next_stream' in fieldValues)
        //     temp.next_stream = fieldValues.next_stream ? "" : "Next Stream is Required"
        // temp.phone_no = (\\+?\\d{9,13}).test(values.phone_no) ? "" : "Student Name is Required"
        setErrors({ ...temp })

        // tests whether post array elements passes text implemented by validate() function
        if (fieldValues === values)
            return Object.values(temp).every(x => x === "")
        
    }

    const initialFValues = {
        name: '',
        next_stream: '',
    }

    const { values,  
            setValues,
            errors, 
            setErrors, 
            handleResetForm, 
            handleInputChange } = useForm(initialFValues, true, validate);

    
    
    // useEffect(() => {
    //     if(recordForEdit != null)
    //         setValues({
    //             ...recordForEdit
    //         })
    
    // }, [dispatch,  recordForEdit, success, loading, error, setValues ])


    const submitHandler = (e) => {
        e.preventDefault()
        if (validate()) {
            newEntry(values, handleResetForm);
            handleResetForm()
        }
    }

   
    return (
        
        <>
            { loading && <Loader />}
            { error && <ToastAlert severity="error">{error}</ToastAlert>}
                <MainForm onSubmit={submitHandler}>
                    <Grid container>
                        <Grid item md={6} xs={12}>
                            <Controls.InputField
                                error={errors.name}
                                label="Stream Name"
                                value={values.name}
                                name='name'
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <Grid item md={6} xs={12}>
                            <Controls.InputField 
                                error={errors.next_stream}
                                label="Next Stream"
                                name='next_stream'
                                value={values.next_stream}
                                onChange={handleInputChange}
                            />
                        </Grid>

                        { loading ? <Loading /> 
                            :   <FormButton type='submit'>
                                    Submit 
                                </FormButton>
                            } 
                        

                    </Grid>
                </MainForm>
        </>
        
            
            
                       
    )
}

export default StreamForm
