export const STREAM_CREATE_REQUEST = 'STREAM_CREATE_REQUEST'
export const STREAM_CREATE_SUCCESS = 'STREAM_CREATE_SUCCESS'
export const STREAM_CREATE_FAIL    = 'STREAM_CREATE_FAIL'
export const STREAM_CREATE_RESET   = 'STREAM_CREATE_RESET'

export const STREAM_READ_REQUEST = 'STREAM_READ_REQUEST'
export const STREAM_READ_SUCCESS = 'STREAM_READ_SUCCESS'
export const STREAM_READ_FAIL    = 'STREAM_READ_FAIL'
export const STREAM_READ_RESET   = 'STREAM_READ_RESET'

export const STREAM_UPDATE_REQUEST = 'STREAM_UPDATE_REQUEST'
export const STREAM_UPDATE_SUCCESS = 'STREAM_UPDATE_SUCCESS'
export const STREAM_UPDATE_FAIL = 'STREAM_UPDATE_FAIL'
export const STREAM_UPDATE_RESET = 'STREAM_UPDATE_RESET'

export const STREAM_DETAILS_REQUEST = 'STREAM_DETAILS_REQUEST'
export const STREAM_DETAILS_SUCCESS = 'STREAM_DETAILS_SUCCESS'
export const STREAM_DETAILS_FAIL = 'STREAM_DETAILS_FAIL'
export const STREAM_DETAILS_RESET = 'STREAM_DETAILS_RESET'

export const STREAM_DELETE_REQUEST = 'STREAM_DELETE_REQUEST'
export const STREAM_DELETE_SUCCESS = 'STREAM_DELETE_SUCCESS'
export const STREAM_DELETE_FAIL = 'STREAM_DELETE_FAIL'
export const STREAM_DELETE_RESET = 'STREAM_DELETE_RESET'
