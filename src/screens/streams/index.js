import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';

import { FiDelete, FiEdit3 } from 'react-icons/fi'

import { TabTitle, toTitleCase } from '../../utils/globalFunc'

import Controls from '../../components/controls/Controls'
import ToastAlert from '../../components/display/ToastAlert'
import Loader from '../../components/display/Loader'
import TableComponent from '../../components/useTable/index'
import { Chip, TableTop } from '../../components/useTable/elements'
import { ActionButtonWrapper } from '../../components/controls/Button'
import ConfirmDialog from '../../components/display/dialog'
import Modal from '../../components/display/modal'
import { NgPageContainer, NgPaper } from '../../components/display/elements'
import StreamEditForm from './editform';
import StreamForm from './form';

import { createStream, readStreams, updateStream, deleteStream } from './actions'



// const useStyles = makeStyles(theme => ({
//     searchInput: {
//         width : "85%",
//     },
//     newButton: {
//         position: "absolute",
//         right: "10px",
//         margin: "1rem"
//     },
//     paper: {
//         width: "100%",
//         overflow: "hidden",
//     },
//     container: {
//         paddingTop: theme.spacing(10)
//     },
    
// }))


const headCells = [
    { id: 'name', label: 'Stream Name', minWidth: 170 },
    { id: 'next_stream', label: 'Next Stream', minWidth: 170 },
    { id: 'actions', label: 'Actions', disableSorting: true },
]


function StreamReadScreen() {
    TabTitle('Streams - Samis Systems')

    const dispatch = useDispatch();
    // const classes = useStyles();

    const [ search, setSearch ] = useState({fn:items => {return items;}})
    const [ recordForEdit, setRecordForEdit ] = useState(null);

    const [ openPopup, setOpenPopup ] = useState(false);
    const [ openModal, setOpenModal ] = useState(false);

    const streamCreate = useSelector(state => state.streamCreate)
    const { success: successCreate } = streamCreate

    const streamRead = useSelector(state => state.streamRead)
    const { loading, error, streams } = streamRead

    const streamUpdate = useSelector(state => state.streamUpdate)
    const { success: successUpdate } = streamUpdate

    const streamDelete = useSelector(state => state.streamDelete)
    const { success: successDelete } = streamDelete

    
    const [ message, setMessage ] = useState('')
    const [ confirmDialog, setConfirmDialog ] = useState({isOpen: false, title: '', subTitle: ''})

    const { TblContainer, 
            TblHead, 
            TblPagination, 
            recordsAfterPaginatingAndSorting,
            page, rowsPerPage
        } = TableComponent (streams, headCells, search)
    
    useEffect(() => {
        // dispatch({ type: STREAM_CREATE_RESET})
        dispatch(readStreams(page, rowsPerPage))
        if(successCreate) {
            setMessage('Stream Created Successfully')
        } else {
            if(successUpdate) {
                setMessage('Stream Updated Successfully')
            }
        }
        if(successDelete) {
            setMessage('Stream Deleted Successfully')
        }

        
    }, [dispatch, successCreate, successUpdate, successDelete, page, rowsPerPage])

    const handleSearch = (e) => {
        e.preventDefault()
        let target = e.target
        setSearch({
            fn:items => {
                if(target.value === "")
                    return items;
                else
                    return items.filter(x => x.name.includes(toTitleCase(e.target.value)))
            }
        })
    }    
    

    const newEntry = (stream, handleResetForm) => {
        dispatch(createStream(stream))
        handleResetForm()
        setOpenModal(false);
    }

    const editEntry = (stream, handleResetForm) => {
        dispatch(updateStream(stream))
        setOpenPopup(false);
        handleResetForm()   
    }

    const editHandler = (stream) => { 
        setRecordForEdit(stream)
        setOpenPopup(true)
    }

    const deleteHandler = (id) => {
        setConfirmDialog({
            ...confirmDialog,
            isOpen: false
        })
        dispatch(deleteStream(id)) 
    }

    return (
        <>
            
            <NgPageContainer>  
                <NgPaper>
                    { message && <ToastAlert severity="success">{message}</ToastAlert>}
                    <TableTop>
                        <Controls.SearchInputField
                            label="Search Subjects"
                            onChange={handleSearch}
                        />
                        <Controls.AddButton
                            onClick={() => setOpenModal(true)}
                        >
                        </Controls.AddButton>
                    </TableTop>
                    
                    { loading
                    ? <Loader />
                    : error
                    ? <ToastAlert severity="error">{error}</ToastAlert>
                    : (
                        <>
                            <TblContainer>
                                <TblHead />

                                    <TableBody>
                                    {
                                        recordsAfterPaginatingAndSorting()?.map(stream => 
                                            (<TableRow key={stream.name}>
                                                <TableCell>{stream.name}</TableCell>
                                                <TableCell>{stream.next_stream}</TableCell>
                                                <TableCell>
                                                    <ActionButtonWrapper>
                                                        <Controls.ActionButton
                                                            title="edit"
                                                            onClick={() => editHandler(stream)}
                                                            edit>
                                                            <FiEdit3 />
                                                        </Controls.ActionButton>
                                                        <Controls.ActionButton
                                                            title="deactivate"
                                                            onClick={() => {
                                                                setConfirmDialog({
                                                                    isOpen: true,
                                                                    title: "Are you sure you want to delete this Stream?",
                                                                    subTitle: "You can't undo this operation",
                                                                    onConfirm: () => { deleteHandler(stream.name) }
                                                                })
                                                            }}>
                                                            <FiDelete />
                                                        </Controls.ActionButton>
                                                    </ActionButtonWrapper>
                                                </TableCell>
                                                                            
                                            </TableRow>)
                                    )}
                                    </TableBody>
                            </TblContainer>
                            <TblPagination />
                        </>
                        
                    )}
                </NgPaper>
                                 
                
                <Modal
                    openModal = {openModal}
                    setOpenModal = {setOpenModal}
                    title="Create Stream"
                >
                    <StreamForm 
                        recordForEdit = {recordForEdit}
                        newEntry = {newEntry}/>  
                </Modal>  

                <Modal
                    openPopup = {openPopup}
                    setOpenPopup = {setOpenPopup}
                    title="Edit Stream"
                >
                    <StreamEditForm
                        recordForEdit = {recordForEdit}
                        editEntry = {editEntry}
                    />
                </Modal> 

                <ConfirmDialog 
                    confirmDialog={confirmDialog}
                    setConfirmDialog={setConfirmDialog} />
                
            </NgPageContainer>     
        </>
    )
}

export default StreamReadScreen
