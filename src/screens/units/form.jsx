import React, {useEffect, useState, useContext} from 'react'
import axios from 'axios';

import { useDispatch, useSelector } from 'react-redux'
import { UnitPropertiesContext } from '../../context';

import Controls from '../../components/controls/Controls'
import ToastAlert from '../../components/display/ToastAlert'
import Loading from '../../components/display/Loader'

import { MainForm, useForm } from '../../components/useForm'
import { FormButton } from '../../components/useForm/formElements';
import { List } from '../../components/display/elements';

import { toTitleCase } from '../../utils/globalFunc';


const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const amenitiez = [
  'Oliver Hansen',
  'Van Henry',
  'April Tucker',
  'Ralph Hubbard',
  'Omar Alexander',
  'Carlos Abbott',
  'Miriam Wagner',
  'Bradley Wilkerson',
  'Virginia Andrews',
  'Kelly Snyder',
];

function getStyles(name, val, theme) {
  return {
    fontWeight:
      val.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}




const UnitForm = (props) => {
    const { newEntry } = props;

    const { buildings, setBuildings, tenants, setTenants,
             loading, setLoading, error, setError} = useContext(UnitPropertiesContext)

    const createUnit = useSelector(state => state.createUnit)
    const { loading: loadingCreate, error: errorCreate, success } = createUnit


    const validate = (fieldValues = values) => {
        let temp = {...errors}
        if('unit_no' in fieldValues)
            temp.unit_no = fieldValues.unit_no ? "" : "Name is Required"        
        setErrors({ ...temp })

        // tests whether post array elements passes text implemented by validate() function
        if (fieldValues === values)
            return Object.values(temp).every(x => x === "")
    }
    
    const initialValues = {
        unit_no    : '',  
        space      : '',   
        bedrooms   : '',
        bathrooms  : '',
        building   : '',
        tenant     : ''
    }

    const { 
        values,  
        errors, 
        setValues,
        setErrors,
        handleResetForm,
        handleInputChange
    } = useForm(initialValues, true, validate);

    const submitHandler = (e) => {
        e.preventDefault()
        if (validate()) {
            newEntry(values, handleResetForm);
        } 
    } 

    return (
        <>
            { loading && <Loading />}
            { errorCreate && <ToastAlert severity="error">{errorCreate}</ToastAlert>}

                <MainForm onSubmit={submitHandler}>
                { loadingCreate && <Loading />}
                    <Controls.InputField 
                        value={values.unit_no}
                        name='unit_no'
                        onChange={handleInputChange}
                        label='Name' 
                        error={errors.unit_no}
                        onInput={(e) => e.target.value = toTitleCase(e.target.value)}>        
                    </Controls.InputField>
                    
                    <Controls.InputField
                        value={values.space}
                        name='space'
                        onChange={handleInputChange}
                        label='Space' 
                        error={errors.space}>
                    </Controls.InputField>

                    <Controls.InputField
                        value={values.bedrooms}
                        name='bedrooms'
                        onChange={handleInputChange}
                        label='Bedrooms' 
                        error={errors.bathrooms}>
                    </Controls.InputField>

                    <Controls.InputField
                        value={values.bathrooms}
                        name='bathrooms'
                        onChange={handleInputChange}
                        label='Bathrooms' 
                        error={errors.bathrooms}>
                    </Controls.InputField>

                    <Controls.SelectField
                        label="Building"
                        value={values.building}
                        name="building"
                        onChange={handleInputChange}
                        list_value="name"
                        options={buildings}
                        error={error}
                    />

                    <Controls.SelectField
                        label="Tenant"
                        value={values.tenant}
                        name="tenant"
                        onChange={handleInputChange}
                        list_value="unit_no"
                        options={tenants}
                        error={error}
                    />
                            
                    <FormButton type='submit'>
                        Submit { loadingCreate ? <Loading /> : ""}
                    </FormButton> 
                </MainForm>
                 
            {/* </Container> */}
        </>
    )
}

export default UnitForm