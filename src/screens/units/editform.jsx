import React, { useEffect, useContext } from 'react'
import axios from 'axios';

import { useSelector } from 'react-redux'
import { UnitPropertiesContext } from '../../context';

import Controls from '../../components/controls/Controls'
import ToastAlert from '../../components/display/ToastAlert'
import Loading from '../../components/display/Loader'

import { MainForm, useForm } from '../../components/useForm'
import { toTitleCase } from '../../utils/globalFunc'
import { FormButton, 
    } 
from '../../components/useForm/formElements';

const UnitEditForm = (props) => {
    const { editEntry, recordForEdit, unitId } = props;

    const { buildings, tenants, loading, error} = useContext(UnitPropertiesContext)

    const updateUnit = useSelector(state => state.updateUnit)
    const { loading: loadingUpdate, error: errorUpdate, success } = updateUnit

    const validate = (fieldValues = values) => {
        let temp = {...errors}
        if('unit_no' in fieldValues)
            temp.unit_no = fieldValues.unit_no ? "" : "Unit name is Required"
        // if('space' in fieldValues)
        //     temp.space = (/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/).test(fieldValues.space) ? "" : "Enter a valid Address"
        // if('bedrooms' in fieldValues)
        //     temp.bedrooms = (/^0([0-9](?:(?:[129][0-9])|(?:0[0-8])|(4[0-1]))[0-9]{6})$/).test(fieldValues.bedrooms) ? "" : "Enter a Valid Description"
        
        setErrors({ ...temp })

        // tests whether post array elements passes text implemented by validate() function
        if (fieldValues === values)
            return Object.values(temp).every(x => x === "")
    }
    
    const initialValues = {
        unit_no    : '',  
        space      : '',   
        bedrooms   : '',
        bathrooms  : '',
        building   : '',
        tenant     : ''
    }

    const { 
        values,  
        setValues,
        errors, 
        setErrors,
        handleResetForm,
        handleInputChange
    } = useForm(initialValues, true, validate);

    const submitHandler = (e) => {
        e.preventDefault()
        if (validate()) {
            editEntry(values, handleResetForm);
        } 
    }

    useEffect(() => {
        if(recordForEdit != null)
            setValues({
                ...recordForEdit
            })
    
    }, [recordForEdit, success, loading, error, setValues, unitId ])
  
    return (
        <>
            { errorUpdate && <ToastAlert severity="error">{errorUpdate}</ToastAlert>}
                <MainForm onSubmit={submitHandler}>
                    <Controls.InputField 
                        value={values.unit_no}
                        name='unit_no'
                        onChange={handleInputChange}
                        label='Name' 
                        error={errors.unit_no}
                        onInput={(e) => e.target.value = toTitleCase(e.target.value)}>        
                    </Controls.InputField>
                    
                    <Controls.InputField
                        value={values.space}
                        name='space'
                        onChange={handleInputChange}
                        label='Space' 
                        error={errors.space}>
                    </Controls.InputField>

                    <Controls.InputField
                        value={values.bedrooms}
                        name='bedrooms'
                        onChange={handleInputChange}
                        label='Bedrooms' 
                        error={errors.bedrooms}>
                    </Controls.InputField>

                    <Controls.InputField
                        value={values.bathrooms}
                        name='bathrooms'
                        onChange={handleInputChange}
                        label='Bathrooms' 
                        error={errors.bathrooms}>
                    </Controls.InputField>

                    <Controls.SelectField
                        label="Building"
                        value={values.building}
                        name="building"
                        onChange={handleInputChange}
                        options={buildings}
                    />

                    <Controls.SelectField
                        label="Tenant"
                        value={values.tenant}
                        name="tenant"
                        onChange={handleInputChange}
                        options={tenants}
                    />
                            
                    <FormButton type='submit'>
                        Submit { loadingUpdate ? <Loading /> : ""}
                    </FormButton>
                </MainForm>
                 
            {/* </Container> */}
        </>
    )
}

export default UnitEditForm