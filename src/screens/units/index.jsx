import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';

import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';

import { FiDelete, FiEdit3 } from 'react-icons/fi'

import { TabTitle, toTitleCase } from '../../utils/globalFunc'
import { createUnitAction, deleteUnitAction, readUnitsAction, updateUnitAction } from './actions'


import UnitForm from './form'
import UnitEditForm from './editform'

import Controls from '../../components/controls/Controls'
import ToastAlert from '../../components/display/ToastAlert'
import Loading from '../../components/display/Loader'
import TableComponent from '../../components/useTable/index'
import { Chip, TableTop } from '../../components/useTable/elements'
import { ActionButtonWrapper } from '../../components/controls/Button'
import ConfirmDialog from '../../components/display/dialog'
import Modal from '../../components/display/modal'
import { NgLink, NgPaper, List, NgPageContainer } from '../../components/display/elements'


import { UnitPropertiesContext } from '../../context';


const headCells = [
    { id: 'unit_no', label: 'Name', minWidth: 70 },
    { id: 'space', label: 'Sq Ft', minWidth: 170 },
    { id: 'bedrooms', label: 'Bedrooms', minWidth: 170 },
    { id: 'bathrooms', label: 'Bathrooms', minWidth: 100 },
    { id: 'building', label: 'Building', minWidth: 50 },
    { id: 'tenant', label: 'Tenant', minWidth: 50 },
    { id: 'actions', label: 'Actions', minWidth: 30, disableSorting: true },


    // { id: 'birth_cert_no', label: 'Birth Cert No', minWidth: 170 },
    // { id: 'date_of_bith', label: 'D.O.B', minWidth: 170 },
    // { id: 'term_admitted', label: 'Term of Admission', minWidth: 170 },

];



function UnitReadScreen() {

    TabTitle('Units')

    const dispatch = useDispatch();

    const [message, setMessage] = useState(null)

    const [openModal, setOpenModal] = useState(false);
    const [openPopup, setOpenPopup] = useState(false);
    const [confirmDialog, setConfirmDialog] = useState({ isOpen: false, title: '', subTitle: '' })

    const [search, setSearch] = useState({ fn: items => { return items; } })
    const [recordForEdit, setRecordForEdit] = useState(null);

    const createUnit = useSelector(state => state.createUnit)
    const { success: successCreate } = createUnit

    const readUnits = useSelector(state => state.readUnits)
    const { loading: loadingRead, error: errorRead, units } = readUnits

    const updateUnit = useSelector(state => state.updateUnit)
    const { success: successUpdate } = updateUnit

    const deleteUnit = useSelector(state => state.deleteUnit)
    const { success: successDelete } = deleteUnit

    const [tenants, setTenants] = useState();
    const [buildings, setBuildings] = useState();

    const [loading, setLoading] = useState(true)
    const [error, setError] = useState()


    const signInUser = useSelector(state => state.signInUser)
    const { userInfo } = signInUser

    const { TblContainer,
        TblHead,
        TblPagination,
        recordsAfterPaginatingAndSorting,
    } = TableComponent(units, headCells, search)


    const handleSearch = (e) => {
        e.preventDefault()
        let target = e.target
        setSearch({
            fn: items => {
                if (target.value === "")
                    return items;
                else
                    return items.filter(x => x.unit_no.includes(toTitleCase(e.target.value)))
            }
        })
    }

    const newEntry = (unit, handleResetForm) => {
        dispatch(createUnitAction(unit))
        if (successCreate) {
            handleResetForm()
            setOpenModal(false);
        }
    }

    const editEntry = (unit, handleResetForm) => {
        dispatch(updateUnitAction(unit))
        // setOpenPopup(false);
        // handleResetForm()   
    }

    const editHandler = (unit) => {
        setRecordForEdit(unit)
        setOpenPopup(true)
    }

    const deleteHandler = (id) => {
        setConfirmDialog({
            ...confirmDialog,
            isOpen: false
        })
        dispatch(deleteUnitAction(id))
    }

    const unitProperties = async () => {
        await axios.get(`unit_properties/`).then(res => {
            setLoading(false)
            setTenants(res.data.data.payload.tenants)
            setBuildings(res.data.data.payload.buildings)
        }).catch(err => {
            setError(err.response && err.response.data.detail ?
                <>
                    {Object.keys(err.response.data.detail).map(function (s) {
                        return (
                            <List>{err.response.data.detail[s]}</List>
                        )
                    })}
                </>
                : err.message)
            setLoading(false)
        })
    }

    useEffect(() => {
        unitProperties();
        dispatch(readUnitsAction())
        if (successCreate) {
            setMessage("Unit Added Successfully")
        }
    }, [successCreate, successUpdate, successDelete])

    return (
        <NgPageContainer>
            <NgPaper>
                {successCreate && <ToastAlert severity="success">{message}</ToastAlert>}
                <TableTop>
                    <Controls.SearchInputField
                        label="Search Units"
                        onChange={handleSearch}
                    />
                    <Controls.AddButton
                        onClick={() => setOpenModal(true)}
                    >
                    </Controls.AddButton>
                </TableTop>


                {loadingRead
                    ? <Loading />
                    : errorRead
                        ? <ToastAlert severity="error">{errorRead}</ToastAlert>
                        : (

                            <div>
                                <TblContainer>
                                    <TblHead />

                                    <TableBody>
                                        {
                                            recordsAfterPaginatingAndSorting() && recordsAfterPaginatingAndSorting().map(unit =>
                                            (
                                                <TableRow key={unit.id}>
                                                    <TableCell>
                                                        <NgLink to={`/unit/${unit.slug}`}>
                                                            {unit.unit_no}
                                                        </NgLink>
                                                    </TableCell>
                                                    <TableCell>{unit.space}</TableCell>
                                                    <TableCell>{unit.bedrooms}</TableCell>
                                                    <TableCell>{unit.bathrooms}</TableCell>
                                                    <TableCell>{unit.building}</TableCell>
                                                    <TableCell>
                                                        {
                                                            unit.tenant.full_name ? unit.tenant.full_name
                                                                : unit.tenant.full_name === "" || unit.tenant.full_name == null
                                                                    ? <Chip>vacant</Chip> : <Chip>vacant</Chip>
                                                        }
                                                    </TableCell>
                                                    <TableCell>
                                                        <ActionButtonWrapper>
                                                            <Controls.ActionButton
                                                                title="edit"
                                                                onClick={() => editHandler(unit)}
                                                                edit>
                                                                <FiEdit3 />
                                                            </Controls.ActionButton>
                                                            <Controls.ActionButton
                                                                title="deactivate"
                                                                onClick={() => {
                                                                    setConfirmDialog({
                                                                        isOpen: true,
                                                                        title: "Are you sure you want to delete this Unit?",
                                                                        subTitle: "You can't undo this operation",
                                                                        onConfirm: () => { deleteHandler(unit.slug) }
                                                                    })
                                                                }}>
                                                                <FiDelete />
                                                            </Controls.ActionButton>
                                                        </ActionButtonWrapper>
                                                    </TableCell>
                                                </TableRow>
                                            )
                                            )}
                                    </TableBody>
                                </TblContainer>

                                <TblPagination />
                            </div>

                        )}
            </NgPaper>

            <UnitPropertiesContext.Provider value={{
                tenants, setTenants, buildings,
                setBuildings, loading, setLoading, error, setError
            }}>
                <Modal
                    openModal={openModal}
                    setOpenModal={setOpenModal}
                    title="Create Unit"
                >
                    <UnitForm
                        newEntry={newEntry}
                    />
                </Modal>

                <Modal
                    openPopup={openPopup}
                    setOpenPopup={setOpenPopup}
                    title="Edit Unit"
                >
                    <UnitEditForm
                        recordForEdit={recordForEdit}
                        editEntry={editEntry}
                    />
                </Modal>
            </UnitPropertiesContext.Provider>


            <ConfirmDialog
                confirmDialog={confirmDialog}
                setConfirmDialog={setConfirmDialog}
            />
        </NgPageContainer>

    )
}


export default UnitReadScreen



