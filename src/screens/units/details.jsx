import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom'
import Loading from '../../components/display/Loader';
import ToastAlert from '../../components/display/ToastAlert';

import { unitDetailsAction } from './actions';



const UnitDetailsScreen = () => {
    const match = { params: useParams() }
    const dispatch = useDispatch()

    const unitDetails = useSelector(state => state.unitDetails)
    const { loading, error, unit } = unitDetails

    useEffect(() => {
        dispatch(unitDetailsAction(match.params.id))
    }, [])

    return (
        <>
            {error && <ToastAlert severity="error">{error}</ToastAlert>}
            {
                loading ? <Loading />
                : 
                <>
                    {unit.unit_no}
                </>
            }

        </>
    )
}

export default UnitDetailsScreen;
