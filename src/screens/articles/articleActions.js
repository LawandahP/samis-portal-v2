import axios from 'axios';
import {
    ARTICLE_GET_REQUEST,
    ARTICLE_GET_SUCCESS,
    ARTICLE_GET_FAIL,

    ARTICLE_CREATE_REQUEST,
    ARTICLE_CREATE_SUCCESS,
    ARTICLE_CREATE_FAIL,
    ARTICLE_CREATE_RESET,

    ARTICLE_DETAILS_REQUEST,
    ARTICLE_DETAILS_SUCCESS,
    ARTICLE_DETAILS_FAIL,

    ARTICLE_UPDATE_REQUEST,
    ARTICLE_UPDATE_SUCCESS,
    ARTICLE_UPDATE_FAIL,

    ARTICLE_DELETE_REQUEST,
    ARTICLE_DELETE_SUCCESS,
    ARTICLE_DELETE_FAIL,
} from './articleConstants';

import { proxy, bff } from '../proxy';


export const listArticles = (page, size) => async (dispatch) => {
    try {
        dispatch({
            type: ARTICLE_GET_REQUEST
        })
        const { data } = await axios.get(`${proxy}/articles/v1?page=${page}&size=${size}`)
        dispatch({
            type: ARTICLE_GET_SUCCESS,
            payload: data
        })

    } catch(error) {
        dispatch({
            type: ARTICLE_GET_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.message
        })

    }
}



export const createArticle = (testimonial) => async (dispatch, getState) => {
    try {
        dispatch({
            type: ARTICLE_CREATE_REQUEST
        })

        const { data } = await axios.post(
            `${proxy}/articles/v1`,
            testimonial,
        )

        dispatch({
            type: ARTICLE_CREATE_SUCCESS,
            success: true,
            payload: data
        })

    } catch(error) {
        dispatch({
            type: ARTICLE_CREATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


export const articleDetails = (article) => async (dispatch) => {
    try {
        dispatch({ type: ARTICLE_DETAILS_REQUEST })
        const { data } = await axios.get(`${proxy}/articles/v1/${article.article_id}`) //proxy in package.json "http://127.0.0.1:8000/"

        dispatch({
            type:ARTICLE_DETAILS_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type:ARTICLE_DETAILS_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })
    }
}


export const updateArticle = (article) => async (dispatch) => {
    try {
        dispatch({
            type: ARTICLE_UPDATE_REQUEST
        })

        const { data } = await axios.put(
            `${proxy}/articles/v1/${article.article_id}`,
            article,
        )

        dispatch({
            type: ARTICLE_UPDATE_SUCCESS,
            payload: data
        })

        //update details
        dispatch({
            type: ARTICLE_DETAILS_SUCCESS,
            payload: data
        })

    } catch(error) {
        dispatch({
            type: ARTICLE_UPDATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


export const deleteArticle = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: ARTICLE_DELETE_REQUEST
        })

        const { data } = await axios.delete(
            `${proxy}/articles/v1/${id}`,
        )

        dispatch({
            type: ARTICLE_DELETE_SUCCESS,
        })

    } catch(error) {
        dispatch({
            type: ARTICLE_DELETE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


// export const uploadArticleImage = () => async (dispatch) => {
//     try {
//         dispatch({ type: ARTICLE_GET_REQUEST })
//         const { data } = await axios.get(`${proxy}/articles/v1/images/upload`)

//         dispatch({
//             type:ARTICLE_GET_SUCCESS,
//             payload: data
//         })
//     } catch (error) {
//         dispatch({
//             type:ARTICLE_GET_FAIL,
//             payload: error.response && error.response.data.detail
//                 ? error.response.data.detail
//                 : error.message
//         })
//     }
// }
