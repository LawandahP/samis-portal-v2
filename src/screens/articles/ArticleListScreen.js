import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import Loader from '../components/Loader'
import ToastAlert from '../components/ToastAlert'
import TableComponent from '../components/TableComponent'
import Controls from '../components/controls/Controls'
import Popup from '../components/Popup'
import ConfirmDialog from '../components/ConfirmDialog'

import ArticleEditForm from './ArticleEditForm'
import ArticleForm from './ArticleForm'
import { createArticle, deleteArticle, listArticles, updateArticle } from './articleActions'

import { makeStyles } from '@mui/styles'
import EditOutlinedIcon from '@mui/icons-material/EditOutlined';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import Paper from '@mui/material/Paper';
import TableRow from '@mui/material/TableRow';

import { TabTitle, toTitleCase } from '../utils/globalFunc'
import { Container } from '@mui/material'
import { useHistory } from 'react-router-dom'



const useStyles = makeStyles(theme => ({
    searchInput: {
        width : "85%",
    },
    newButton: {
        position: "absolute",
        right: "10px",
        margin: "1rem"
    },
    paper: {
        width: "100%",
        overflow: "hidden",
    },
    container: {
        paddingTop: theme.spacing(10)
    }
}))
const headCells = [
    { id: 'text', label: 'Text', minWidth: 220 },
    { id: 'excerpt', label: 'Excerpt', minWidth: 300 },
    { id: 'user_id', label: 'Written By', minWidth: 170 },
    // { id: 'pen_name', label: 'Alias' },
    { id: 'created_on', label: 'Date created' },
    { id: 'actions', label: 'Actions' },

]

function ArticleListScreen() {
    TabTitle('Articles | Samis Systems')
    const dispatch = useDispatch();
    const classes = useStyles();
    let history = useHistory();

    const [ search, setSearch ] = useState({fn:items => {return items;}})
    const [ recordForEdit, setRecordForEdit ] = useState(null);

    const [ openPopup, setOpenPopup ] = useState(false);
    const [ openModal, setOpenModal ] = useState(false)

    const articleList = useSelector(state => state.articleList)
    const { loading, error, articles } = articleList

    const articleCreate = useSelector(state => state.articleCreate)
    const { success: successCreate } = articleCreate

    const userLogin = useSelector(state => state.userLogin)
    const { userInfo } = userLogin

    const [ confirmDialog, setConfirmDialog ] = useState({isOpen: false, title: '', subTitle: ''})

    const { TblContainer, 
            TblHead, 
            TblPagination, 
            recordsAfterPagingAndSorting,
            page, rowsPerPage
        } = TableComponent (articles, headCells, search)
    
    useEffect(() => {
        dispatch(listArticles(page, rowsPerPage))
        if(!userInfo) {
            history.push("/login") 
        }
    }, [dispatch, page, rowsPerPage])

    const handleSearch = (e) => {
        e.preventDefault()
        let target = e.target
        setSearch({
            fn:items => {
                if(target.value === "")
                    return items;
                else
                    return items.filter(x => x.text.includes(toTitleCase(e.target.value)))
            }
        })
    }

    const newEntry = (article, handleResetForm) => {
        dispatch(createArticle(article))
        setOpenPopup(false); 
        handleResetForm()
             
    }

    const editEntry = (article, handleResetForm) => {
        dispatch(updateArticle(article))
        handleResetForm()
        setOpenModal(false);
           
    }

    const editHandler = (article) => { 
        setRecordForEdit(article)
        setOpenModal(true)
    }

    const deleteHandler = (id) => {
        setConfirmDialog({
            ...confirmDialog,
            isOpen: false
        })
        dispatch(deleteArticle(id))  
        dispatch(listArticles())
    }

    return (
        <div>
            <Container maxWidth="lg" className={classes.container}>                   
            { loading
            ? <Loader />
            : error
            ? <ToastAlert severity="error">{error}</ToastAlert>
            : (
                <Paper className={classes.paper}>
                
                        <Controls.SearchInput
                            label="Search Article"
                            onChange = { handleSearch }
                        />
 
                    <TblContainer>
                        <TblHead />

                        <TableBody>
                        {
                            recordsAfterPagingAndSorting() && recordsAfterPagingAndSorting().map(article => 
                                (
                                
                                <TableRow key={article.article_id}>
                                    <TableCell>{article.text}</TableCell>
                                    <TableCell>{article.excerpt}</TableCell>
                                    <TableCell>{article.written_by}</TableCell>
                                    <TableCell>{article.created_on}</TableCell>
                                    <TableCell>
                                        <Controls.ActionButton
                                            title="Edit Article"
                                            color="primary">
                                            <EditOutlinedIcon 
                                                onClick={() => editHandler(article)}
                                                fontSize="small" />
                                        </Controls.ActionButton>
                                        <Controls.ActionButton 
                                            title="Delete Article"
                                            color="secondary"
                                            onClick={() => {
                                                setConfirmDialog({
                                                    isOpen: true,
                                                    title: "Are you sure you want to delete this article?",
                                                    subTitle: "You can't undo this operation",
                                                    onConfirm: () => { deleteHandler(article.id)  }
                                                })
                                                
                                            }}>
                                            <DeleteOutlineIcon 
                                                fontSize="small" />
                                            </Controls.ActionButton>
                                        </TableCell>
                                                                
                                </TableRow>)
                        )}
                        </TableBody>
                    </TblContainer>
                    
                    <TblPagination />
                </Paper>
                
            )}

                <Controls.AddButton
                    title="Add Stream"
                    onClick = {() => setOpenPopup(true)}
                    >
                </Controls.AddButton>

            <Popup
                openPopup = {openPopup}
                setOpenPopup = {setOpenPopup}
                title="Create New Article"
            >
                <ArticleForm 
                    recordForEdit = {recordForEdit}
                    newEntry = {newEntry}/>                
            </Popup> 

            <Popup
                openModal = {openModal}
                setOpenModal = {setOpenModal}
                title="Edit Article"
            >
                <ArticleEditForm 
                    recordForEdit = {recordForEdit}
                    editEntry = {editEntry}/>                
            </Popup>

            <ConfirmDialog 
                confirmDialog={confirmDialog}
                setConfirmDialog={setConfirmDialog} />
            
            </Container>

        </div>
    )
}

export default ArticleListScreen
