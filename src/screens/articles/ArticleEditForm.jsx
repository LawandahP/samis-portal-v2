import React, {useEffect} from 'react'
import { useDispatch, useSelector } from 'react-redux';

import Grid from '@mui/material/Grid';

import Loader from '../components/Loader';
import ToastAlert from '../components/ToastAlert';
import {useForm, Form } from '../components/useForm';
import Controls from '../components/controls/Controls';


function ArticleEditForm(props) {
    const { editEntry, recordForEdit, articleId } = props;
    const dispatch = useDispatch();

    const articleCreate = useSelector(state => state.articleCreate)
    const { loading: loadingCreate, error: errorCreate, success: successCreate } = articleCreate

    // const [successMessage, setSuccessMessage] = useState('')
    
    const validate = (fieldValues = values) => {
        let temp = {...errors}
        if('user_id' in fieldValues)
            temp.user_id = fieldValues.user_id ? "" : "Author is Required"
        if('pen_name' in fieldValues)
            temp.pen_name = fieldValues.pen_name ? "" : "Pen Name is Required"
        if('excerpt' in fieldValues)
            temp.excerpt = fieldValues.excerpt ? "" : "Excerpt is Required"
        if('text' in fieldValues)
            temp.text = fieldValues.text ? "" : "Heading is Required"
        if('created_on' in fieldValues)
            temp.created_on = fieldValues.created_on ? "" : "Date is Required"
        
        // temp.phone_no = (\\+?\\d{9,13}).test(values.phone_no) ? "" : "Student Name is Required"
        setErrors({ ...temp })

        // tests whether post array elements passes text implemented by validate() function
        if (fieldValues === values)
            return Object.values(temp).every(x => x === "")
        
    }

    const initialFValues = {
        text: '',
        excerpt: '',
        created_on: '',
        user_id: '',
        pen_name: '',
    }
        
    const { values,  
            setValues,
            errors, 
            setErrors, 
            handleResetForm, 
            handleInputChange } = useForm(initialFValues, true, validate);

    
    
    useEffect(() => {
        if(recordForEdit != null)
            setValues({
                ...recordForEdit
            })
    
    }, [dispatch,  recordForEdit, successCreate, loadingCreate, errorCreate, setValues, articleId ])


    const submitHandler = (e) => {
        e.preventDefault()
        // if (validate()) {
            editEntry(values, handleResetForm);
            handleResetForm()
        // }
    }

   
    return (
        
        <div>
            { loadingCreate && <Loader />}
            { errorCreate && <ToastAlert severity="error">{errorCreate}</ToastAlert>}
                <Form onSubmit={submitHandler}>
                    <Grid container>
                        <Grid item md={6} xs={12}>
                            <Controls.Input
                                error={errors.user_id}
                                label="Author Name"
                                value={values.user_id}
                                name='user_id'
                                onChange={handleInputChange}
                            />

                            <Controls.Input
                                error={errors.pen_name}
                                label="Author Pen Name"
                                value={values.pen_name}
                                name='pen_name'
                                onChange={handleInputChange}
                            />
                            <Controls.Input 
                                multiline
                                rows={2}
                                error={errors.text}
                                label="Heading"
                                name='text'
                                value={values.text}
                                onChange={handleInputChange}
                            />
                            <Controls.DatePicker 
                                label="Date"
                                name="created_on"
                                value={values.created_on}
                                onChange={handleInputChange}/>
                            
                        </Grid>

                        <Grid item md={6} xs={12}>
                            <Controls.Input 
                                multiline
                                rows={12}
                                error={errors.excerpt}
                                label="Excerpt"
                                name='excerpt'
                                value={values.excerpt}
                                onChange={handleInputChange}
                            />
                        </Grid>
                        
                        <div>
                            <Controls.Button
                                type="submit" 
                                text="Submit"
                            />
                            <Controls.Button 
                                onClick={handleResetForm}
                                text="Reset"
                                color="secondary"
                            />
                        
                        </div>
                        

                    </Grid>
                </Form>
        </div>
        
    )
}
export default ArticleEditForm
