import React, { useEffect, useState } from 'react'

import { Box, Container, Grid, ListItem, Paper, Typography } from '@mui/material'
import { makeStyles } from '@mui/styles'
import jsPDF from 'jspdf'
import { useDispatch, useSelector } from 'react-redux'


import { Root } from './tableStyles'
import { NgLink, NgPageContainer, NgPaper } from '../../components/display/elements';
import Controls from '../../components/controls/Controls';
import Loader from '../../components/display/Loader';
import ToastAlert from '../../components/display/ToastAlert';

import { readStudentReports, readAcademicReports, readAnalysisByTermAndYear } from './actions'
import { readContactInfo } from '../contact_info/actions'

import { MainForm, useForm } from '../../components/useForm'
import { readSchoolInfo } from '../school_info/actions'



const useStyles = makeStyles((theme) => ({
	container: {
        // paddingTop: theme.spacing(10),  
		textAlign: "center" ,
		justifyContent: "center",
		alignContent: "center",
		alignItems: "center",
		// display: "flex"     
    },

	paper: {
		width: "595px",
		height: "842px",
		textAlign: "center",
		// padding: theme.spacing(2),
	},

	image: {
        height: "auto",
        width: 100,
    },

	schoolName: {
        fontWeight: "600",
		fontSize: "1rem",
    },

	li: {
		listStyle: "none",
		margin: "none"
	},

	analysisName: {
		fontSize: ".8rem",
		fontWeight: "500",
		// marginTop: theme.spacing(2)
	},

	total: {
		fontSize: ".5rem",
		
	},

	gridTotal: {
		// marginTop: theme.spacing(2),
		textAlign: "start"
	},

	hr : {
		// color: theme.palette.primary.main,
	}

}))

const StudentsReportFormScreen = () => {
	const classes = useStyles()
    const dispatch = useDispatch()
	
	const studentReportRead = useSelector(state => state.studentReportRead)
	const { loading, error, studentReport, score, subjects} = studentReportRead

	const schoolInfoRead = useSelector(state => state.schoolInfoRead)
    const { loading: schoolInfoLoading, error: schoolInfoError, schoolInfo } = schoolInfoRead

	const contactInfoRead = useSelector(state => state.contactInfoRead)
    const { contactInfo } = contactInfoRead

	const analysisRead = useSelector(state => state.analysisRead);
	const { loading: loadingAnalysis, error: errorAnalysis, analysis } = analysisRead

    const [year, setYear] = useState()
	const [semister, setSemister] = useState()		
    
	const initialValues = {
        term: '2019/1',
        analysisName: "",
		admNo: "1300"
    }

    const { 
        values,  
        handleInputChange
    } = useForm(initialValues);

    let analysisName = values.analysisName
    let term = values.term
	let admNo = values.admNo

	const handleGetStudentReport = (e) => {
		e.preventDefault()
		dispatch(readStudentReports(admNo, analysisName, term))
	}

	const generateReportForm = () => {
		var doc = new jsPDF('p', 'pt', 'a4', true)
			// doc.setFont('Lato-Regular', 'normal');
			doc.html(document.querySelector("#report-form"), {
			callback: function(pdf) {
				var pageCount = doc.internal.getNumberOfPages();
				pdf.deletePage(pageCount)
				pdf.save(`${admNo}.pdf`)
			}
		});
	}

	useEffect(() => {
		setYear(() => {
            let str_year = term;
            if (str_year) {
                str_year = str_year.split("/")[0];
                return str_year
            }
        })
        setSemister(() => {
            let sem = term;
            if (sem) {
                sem = sem.split("/")[1];
                return sem
            }
        })
		// dispatch(readSchoolInfo())
		// dispatch(readContactInfo())
		dispatch(readAnalysisByTermAndYear(year, semister))
    }, [dispatch, term, semister, year])
	
    return (
        <NgPageContainer>
			<Grid container sx={{textAlign: "center"}}>
				<Grid item md={4} xs={12}>
					<MainForm onSubmit={handleGetStudentReport}>
						<Grid container>
							<Grid item md={12} xs={12}>
								<Controls.InputField
									type="number"
									name="admNo"
									label="Enter Adm No"
									variant="outlined"
									value={admNo}
									onChange={handleInputChange}
								/>
							</Grid>
							<Grid item md={12} xs={12}>
								<Controls.InputField
									name="term"
									label="Enter Term"
									value={term}
									onChange={handleInputChange}
								/>
							</Grid>
							<Grid item md={12} xs={12}>
								<Controls.SelectAnalysisField
									name="analysisName"
									label="Select analysis"
									variant="outlined"
									options={analysis}
									value={analysisName}
									onChange={handleInputChange}
									error = {	
												loadingAnalysis ? "loading"
												: analysis?.length === 0 ? `Analysis for ${term} not found` 
												: errorAnalysis ? errorAnalysis  
												: ""
											}
								/>
							</Grid>
							<Grid item md={12} xs={12}>
								<button
									onClick={generateReportForm}>
										print
								</button>

								<button type="submit">Submit</button>
									
								
							</Grid>
						</Grid>
					</MainForm>
				</Grid>

				<Grid item md={8} xs={12}>
					<div id="report-form">
						<NgPaper>
							<Grid container sx={{width: "100%"}}>
								<Grid item xs={2}>
									<Box
										className={classes.image}
										component="img"
										alt={ schoolInfo?.school_name }
										src={ loading ? console.log("loading") 
											: error ? console.log({error}) 
											: schoolInfo?.logo_url ? schoolInfo.logo_url 
											: <Box className={classes.image}
												component="img"
												src="images/logo">
											</Box>}
									/> 
								</Grid>

								<Grid item xs={8}>
									<ul className={classes.li}>
										<li>
											<Typography className={classes.schoolName}>
												{ schoolInfoLoading ? ""  : schoolInfoError ? "" : schoolInfo.school_name ? schoolInfo.school_name : "SCHOOL NAME"}
											</Typography>
										</li>
										<li>
											<Typography className={classes.analysis}>
												{contactInfo?.postal_address}
												TEL: {contactInfo.primary_phone_no}
											</Typography>
										</li>
										<li>
										{loading ? "" : error ? "" : 
											<Typography className={classes.analysis}>
												{studentReport?.analysis}
											</Typography>
										}
										</li>
									</ul>									
								</Grid>

								<Grid item xs={2}>
									<Box
										className={classes.image}
										
										component="img"
										alt={ schoolInfo.school_name }
										src={ loading ? console.log("loading") 
											: error ? console.log({error}) 
											: schoolInfo.logo_url ? schoolInfo.logo_url 
											: <Box className={classes.image}
												component="img"
												src="images/logo">
											</Box>}
									/> 
								</Grid>
							</Grid>	

							<hr className={classes.hr}/>	

							{
								loading ? <Loader />
								: error ? <ToastAlert severity="info">{error}</ToastAlert>
								: 

								<>
									<Root sx={{ width: "100%", maxWidth: '100%' }}>
										<table aria-label="custom pagination table">
											<thead>
												<tr>
													<th>Subjects</th>
													<th>Score</th>
													<th>Out Of</th>
													<th>Grade</th>
													<th>Points</th>
													<th>Position</th>
													<th>Remarks</th>
													<th>Teacher</th>
													<th>T.I</th>
												</tr>
											</thead>
											
												<tbody>
													{subjects?.map((subject) => (
														<tr key={subject.subject_code}>
															<td>{subject.subject_code} {subject.subject_name}</td>
															<td>{subject.score}</td>
															<td>{subject.out_of}</td>
															<td>{subject.grade}</td>
															<td>{subject.points}</td>
															<td>{subject.position}</td>
															<td>{subject.comment}</td>
															<td>{subject.teacher}</td>
															<td>{subject.teacher_initials}</td>
														</tr>
											
													))}
												</tbody>
										</table>
									</Root>

									<Grid container className={classes.gridTotal}>
										<Grid item md={3}>
											<Typography className={classes.total}>
												<b>Total Marks: </b>{score?.total_marks}(out of {score?.total_out_of} )
											</Typography>
										</Grid>
										<Grid item md={3}>
											<Typography className={classes.total}>
												<b>Points: </b>{score?.total_points}
											</Typography>
										</Grid>
										<Grid item md={3}>
											<Typography className={classes.total}>
												<b>Mean Points: </b>{score?.mean_points}
											</Typography>
										</Grid>
										<Grid item md={3}>
											<Typography className={classes.total}>
												<b>Mean Marks: </b>{score?.mean_marks}
											</Typography>
										</Grid>
									</Grid>
									<Grid container sx={{textAlign: "start", width: "100%"}}>
										<Grid item md={3}>
											<Typography className={classes.total}>
												<b>Mean Grade: </b>{score?.mean_grade}
											</Typography>
										</Grid>
										<Grid item md={3}>
											<Typography className={classes.total}>
												<b>Form Position: </b>{studentReport?.form_position}
											</Typography>
										</Grid>
										<Grid item md={3}>
											<Typography className={classes.total}>
												<b>Stream Position: </b>{studentReport?.stream_position}
											</Typography>
										</Grid>
									</Grid>
								</>
							}
						</NgPaper>			
					</div>	
				</Grid>
			</Grid>
            

							
        </NgPageContainer>
    )
}
export default StudentsReportFormScreen


