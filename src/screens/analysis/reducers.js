import { 
    ANALYSIS_READ_REQUEST,
    ANALYSIS_READ_SUCCESS,
    ANALYSIS_READ_FAIL,

    REPORTS_READ_REQUEST,
    REPORTS_READ_SUCCESS,
    REPORTS_READ_FAIL,
    
    REPORT_DETAILS_READ_REQUEST,
    REPORT_DETAILS_READ_SUCCESS,
    REPORT_DETAILS_READ_FAIL,

    STUDENT_REPORT_READ_REQUEST,
    STUDENT_REPORT_READ_SUCCESS,
    STUDENT_REPORT_READ_FAIL,

} from './constants'

export const readAnalysisByTermAndYearReducer = ( state = { analysis: [] }, action) => {
    switch(action.type) {
        case ANALYSIS_READ_REQUEST:
            return { loading: true, analysis: [] }
        
        case ANALYSIS_READ_SUCCESS:
            return { loading: false, success: true, analysis: action.payload.data}
        
        case ANALYSIS_READ_FAIL:
            return { loding: false, success: false, error: action.payload}
        
        default:
            return state
    }
}


export const readAcademicReportsReducer = ( state = { academicReports: [] }, action) => {
    switch(action.type) {
        case REPORTS_READ_REQUEST:
            return { loading: true, academicReports: [] }
        
        case REPORTS_READ_SUCCESS:
            return { 
                loading: false, success: true, 
                academicReports: action.payload.data.items,
                index:action.payload.data.index,
                totalPages: action.payload.data.page_count,
                size: action.payload.data.page_size
            }
        
        case REPORTS_READ_FAIL:
            return { loding: false, success: false, error: action.payload}
        
        default:
            return state
    }
}

export const academicReportDetailsReducer = (state = { academicReport: { } }, action) =>{
    switch(action.type) {
        case REPORT_DETAILS_READ_REQUEST:
            return {loading: true, ...state}
        
        case REPORT_DETAILS_READ_SUCCESS:
            return {loading: false, academicReport: action.payload.data}
        
        case REPORT_DETAILS_READ_FAIL:
            return {loading: false, error: action.payload}
        
        default:
            return state
    }
}


export const studentReportReducer = (state = { studentReport: { } }, action) =>{
    switch(action.type) {
        case STUDENT_REPORT_READ_REQUEST:
            return {loading: true, ...state}
        
        case STUDENT_REPORT_READ_SUCCESS:
            return {    loading: false, 
                        studentReport: action.payload.data,
                        exams: action.payload.data.exams,
                        score: action.payload.data.score,
                        subjects: action.payload.data.score.subjects
                    }
        
        case STUDENT_REPORT_READ_FAIL:
            return {loading: false, error: action.payload}
        
        default:
            return state
    }
}