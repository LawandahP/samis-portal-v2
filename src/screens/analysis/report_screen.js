import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'


import { makeStyles } from '@mui/styles';

import { readAcademicReports, readAnalysisByTermAndYear } from './actions'

import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import Paper from '@mui/material/Paper';
import TableRow from '@mui/material/TableRow';


import Container from '@mui/material/Container'
import { Grid, IconButton } from '@mui/material';

import Box from '@mui/material/Box';
import Collapse from '@mui/material/Collapse';
// import IconButton from '@mui/material/IconButton';
import Table from '@mui/material/Table';
import TableHead from '@mui/material/TableHead';
import Typography from '@mui/material/Typography';

import AnalysisScreen from './analysis_screen';
import { NgLink, NgPageContainer, NgPaper } from '../../components/display/elements';
import { MainForm, useForm } from '../../components/useForm';
import TableComponent from '../../components/useTable';
import Controls from '../../components/controls/Controls';
import Loader from '../../components/display/Loader';
import ToastAlert from '../../components/display/ToastAlert';
import { TabTitle } from '../../utils/globalFunc';





const useStyles = makeStyles(theme => ({
    container: {
        paddingTop: "2px"
    },

	paper: {
		display: 'flex',
		flexWrap: 'wrap',
		listStyle: 'none',
		marginTop: "2px",
		p: 0.5,
		m: 0,
	},

	icon: {
		fontSize: '12px',
	}

}))

const headCells = [
    { id: 'id', label: 'id', minWidth: 100 },
    { id: 'analysis', label: 'Analysis', minWidth: 250 },
    { id: 'term', label: 'Term', minWidth: 100 },
    { id: 'formPosition', label: 'Form Position', minWidth: 70 },
    { id: 'streamPosition', label: 'Stream Position', minWidth: 50 },
] 


const ReportScreen = () => {
    TabTitle('Reports - Samis Systems')

    const dispatch = useDispatch()

    const reportsList = useSelector(state => state.reportsList)
    const {loading, error, academicReports} = reportsList

    


    const [year, setYear] = useState()
	const [semister, setSemister] = useState()

    const analysisRead = useSelector(state => state.analysisRead);
	const { loading: loadingAnalysis, error: errorAnalysis, analysis } = analysisRead

    const [ search, setSearch ] = useState({fn:items => {return items;}})

    const { TblContainer, 
        TblHead, 
        TblPagination, 
        recordsAfterPaginatingAndSorting,
        page, rowsPerPage
    } = TableComponent (academicReports, headCells, search)

    const initialValues = {
        term: '2019/1',
        analysisName: ""
    }

    const { 
        values,  
        handleInputChange
    } = useForm(initialValues);

    let analysisName = values.analysisName
    let term = values.term

    useEffect(() => {
        setYear(() => {
            let str_year = term;
            if (str_year) {
                str_year = str_year.split("/")[0];
                return str_year
            }
        })
        setSemister(() => {
            let sem = term;
            if (sem) {
                sem = sem.split("/")[1];
                return sem
            }
        })
        dispatch(readAcademicReports(analysisName,page,rowsPerPage,term))
        dispatch(readAnalysisByTermAndYear(year, semister))
    }, [dispatch, analysisName, term, semister, year, page, rowsPerPage])

    return (
        <>
        {/* <AnalysisScreen /> */}
            <NgPageContainer> 
                <NgPaper>
                <MainForm>
                    <Grid container>
                        <Grid item md={6} xs={4}>
                            <Controls.InputField
                                name="term"
                                label="Enter Term"
                                variant="outlined"
                                value={term}
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <Grid item md={6} xs={8}>
                            <Controls.SelectAnalysisField
                                name="analysisName"
                                label="Select Analysis"
                                variant="outlined"
                                options={analysis}
                                value={analysisName}
                                onChange={handleInputChange}
                                error = {analysis?.length < 1 ? `Analysis for ${term} not found` : errorAnalysis ? errorAnalysis : ""}
                                loading = {loadingAnalysis && <Loader />}
                            />
                        </Grid>
                    </Grid>
                </MainForm>

                { loading
                ? <Loader />
                : error
                ? <ToastAlert severity="error">{error}</ToastAlert>
                : (
                    academicReports?.length > 0 ?
                    <>  
                        <ToastAlert severity="info"><strong>{academicReports.length}</strong> reports found for analysis <strong>{analysisName}</strong> term <strong>{term}</strong></ToastAlert>        
                        <TblContainer>
                            <TblHead />
                            {
                                recordsAfterPaginatingAndSorting()?.map(report => 
                                    (
                                        <>
                                            <TableRow key={report?.id}>
                                                <TableCell>
                                                    <NgLink to={`/report/${report?.id}`}>
                                                        {report?.id}
                                                    </NgLink>
                                                </TableCell>
                                                <TableCell>{report?.analysis}</TableCell>
                                                <TableCell>{report?.term}</TableCell>
                                                <TableCell>{report?.form_position}</TableCell>
                                                <TableCell>{report?.stream_position}</TableCell>                                                                                                
                                            </TableRow>
                                        </>
                                    )
                            )}
                        </TblContainer>
                        <TblPagination />
                    </>
                    :   <ToastAlert severity="info">No Reports found for analysis <strong>{analysisName}</strong> term <strong>{term}</strong>
                        </ToastAlert> 
                    
                )}       
        
                </NgPaper> 
            </NgPageContainer>
        </>
    )
    
};

export default ReportScreen
