import axios from 'axios';
import { backend } from '../../proxy';
import { 
    ANALYSIS_READ_REQUEST,
    ANALYSIS_READ_SUCCESS,
    ANALYSIS_READ_FAIL,

    REPORTS_READ_REQUEST,
    REPORTS_READ_SUCCESS,
    REPORTS_READ_FAIL,

    REPORT_DETAILS_READ_REQUEST,
    REPORT_DETAILS_READ_SUCCESS,
    REPORT_DETAILS_READ_FAIL,

    STUDENT_REPORT_READ_REQUEST,
    STUDENT_REPORT_READ_SUCCESS,
    STUDENT_REPORT_READ_FAIL

} from './constants';


export const readAnalysisByTermAndYear = (year, term) => async(dispatch) => {
    try {
        dispatch({ type: ANALYSIS_READ_REQUEST })
        
        const { data } = await axios.get(`${backend}/school/academics/reports/v1/analysis/${year}/${term}`)

        dispatch({
            type: ANALYSIS_READ_SUCCESS,
            payload: data
        })

    }   catch(error) {
            dispatch({
                type: ANALYSIS_READ_FAIL,
                payload: error.response && error.response.message
                        ? error.response.data.detail
                        : error.message
            })
    }
}

export const readAcademicReports = (analysis, page, size, term) => async(dispatch) => {
    try {
        dispatch({ type: REPORTS_READ_REQUEST })
        
        const { data } = await axios.get(
            `${backend}/school/academics/reports/v1?analysis=${analysis}&page=${page}&size=${size}&term=${term}`
        )

        dispatch({
            type: REPORTS_READ_SUCCESS,
            payload: data
        })

    }   catch(error) {
            dispatch({
                type: REPORTS_READ_FAIL,
                payload: error.response && error.response.message
                        ? error.response.data.detail
                        : error.message
            })
    }
}

export const readAcademicSubjectDetails = (reportId) => async (dispatch) => {
    try {
        dispatch({ type: REPORT_DETAILS_READ_REQUEST })
        const { data } = await axios.get(`${backend}/school/academics/reports/v1/${reportId}`)

        dispatch({
            type:REPORT_DETAILS_READ_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type:REPORT_DETAILS_READ_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })
    }
}

export const readStudentReports = (adm, analysis, term) => async(dispatch) => {
    try {
        dispatch({ type: STUDENT_REPORT_READ_REQUEST })
        
        const { data } = await axios.get(
            `${backend}/school/academics/reports/v1/single-report?admission-no=${adm}&analysis=${analysis}&term=${term}`
        )
        dispatch({
            type: STUDENT_REPORT_READ_SUCCESS,
            payload: data
        })

    }   catch(error) {
            dispatch({
                type: STUDENT_REPORT_READ_FAIL,
                payload: error.response && error.message
                        ? error.response.data.message
                        : error.message
            })
    }
}