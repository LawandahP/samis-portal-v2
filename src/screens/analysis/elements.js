import styled from 'styled-components';

export const AnalysisWrapper = styled.div`
    list-style: none;
    width: 100%;
    
`;

export const Analysis = styled.div`
    border-radius: 50px;
    background: ${({theme}) => theme.bg};
    padding: 10px;
    margin-bottom: 5px;
    display: inline-block;
    font-size: 12px;
    box-shadow: 0 1px 3px rgba(120, 255, 132, 0.2);
    transition: #01bf71 0.2s ease-in-out;

    &:hover {
        transform: scale(1.01);
        transition: all 0.2s ease-in-out;
        cursor: pointer;
    }
`;

export const TableWrapper = styled.div`
    display: grid;
    grid-template-columns: repeat(2, auto);
    grid-gap: 10px;
    
    @media screen and (max-width: 768px) {
        grid-template-columns: auto;
    }
`;
 
