import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux';

import { Grid, ListItem, Paper, Table, TableBody, TableCell, TableHead, TableRow, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { Box } from '@mui/system';



import { readAcademicSubjectDetails } from './actions';
import Loader from '../../components/display/Loader';
import ToastAlert from '../../components/display/ToastAlert';
import { useParams } from 'react-router';
import { NgPageContainer, NgPaper } from '../../components/display/elements';
import TableComponent from '../../components/useTable';
import { TableWrapper } from './elements';


const useStyles = makeStyles(theme => ({
	container: {
		paddingTop: "10px"
	},

	paper: {
		display: 'flex',
		// flexWrap: 'wrap',
		listStyle: 'none',
		margin: "2px",
		padding: "2px",
		// margin: theme.spacing(2),
		overflow: "auto",
	},

	paperScore: {
		// marginTop: theme.spacing(2),
		padding: "2px",
		margin: "2px",
		overflow: "auto",
		display: 'flex',
		width: "100%"
	},

	value: {
		marginLeft: "2px"
	},

	header: {
		fontWeight: "300"
	}
}))

const headCells = [
	{ id: 'code', label: 'Code', minWidth: 50 },
	{ id: 'name', label: 'Name', minWidth: 150 },
	{ id: 'score', label: 'Score', minWidth: 70 },
	{ id: 'out_of', label: 'Out of', minWidth: 120 },
	{ id: 'position', label: 'Pos', minWidth: 50 },
	{ id: 'grade', label: 'Grade', minWidth: 70 },
	{ id: 'points', label: 'Points', minWidth: 70 },
	{ id: 'teacher', label: 'Teacher', minWidth: 150 },
	{ id: 'teacher_initials', label: 'T.I', minWidth: 50 },
	{ id: 'comments', label: 'Comments', minWidth: 150 },
]

const ReportDetailScreen = () => {
	const match = { params: useParams() }
	const classes = useStyles();
	const dispatch = useDispatch();

	const [open, setOpen] = useState(false)

	const reportDetails = useSelector(state => state.reportDetails)
	const { error, loading, academicReport } = reportDetails

	const { TblContainer,
		TblHead,
		TblPagination,
		recordsAfterPaginatingAndSorting,
		page, rowsPerPage
	} = TableComponent(academicReport, headCells)

	useEffect(() => {
		dispatch(readAcademicSubjectDetails(match.params.id))
	}, [dispatch])
	return (
		<NgPageContainer>
			{
				loading ? <Loader />
					: error ? <ToastAlert severity="error">{error}</ToastAlert>
						: (
							<>

								<NgPaper className={classes.paper}>
									<ListItem>
										<Typography className={classes.header} variant="body2">Analysis: </Typography>
										<Typography className={classes.value} variant="body2">{academicReport?.analysis}</Typography>
									</ListItem>
									<ListItem>
										<Typography className={classes.header} variant="body2">Term: </Typography>
										<Typography className={classes.value} variant="body2">{academicReport?.term}</Typography>
									</ListItem>
									<ListItem>
										<Typography className={classes.header} variant="body2">subject Position: </Typography>
										<Typography className={classes.value} variant="body2">{academicReport?.subject_position}</Typography>
									</ListItem>
									<ListItem>
										<Typography className={classes.header} variant="body2">Form Position: </Typography>
										<Typography className={classes.value} variant="body2">{academicReport?.form_position}</Typography>
									</ListItem>
								</NgPaper>

								<TableWrapper>
									{academicReport?.exams?.map((exam) => (
										<NgPaper>
											<h5>{exam?.name}</h5>
											<TblContainer>
												<TableHead>
													<TableRow>
														<TableCell>Code</TableCell>
														<TableCell>Name</TableCell>
														<TableCell>Score</TableCell>
														<TableCell>Out of</TableCell>
													</TableRow>
												</TableHead>
												
													<>
													{
														exam?.subjects?.map(subject => 
															(<TableRow key={subject.subject_code}>
																<TableCell>{subject.subject_code}</TableCell>
																<TableCell>{subject.subject_name}</TableCell>
																<TableCell>{subject.score}</TableCell>
																<TableCell>{subject.out_of}</TableCell>
															</TableRow>)
													)}
													</>
												
											</TblContainer>
										</NgPaper>
									))}
								</TableWrapper>
								


								<NgPaper>
									<TblContainer>
										<TblHead />
										{academicReport?.score?.subjects?.map((subject) => (
											<TableRow key={subject.subject_code}>
												<TableCell>{subject.subject_code}</TableCell>
												<TableCell>{subject.subject_name}</TableCell>
												<TableCell>{subject.score}</TableCell>
												<TableCell>{subject.out_of}</TableCell>
												<TableCell>{subject.position}</TableCell>
												<TableCell>{subject.grade}</TableCell>
												<TableCell>{subject.points}</TableCell>
												<TableCell>{subject.teacher}</TableCell>
												<TableCell>{subject.teacher_initials}</TableCell>
												<TableCell>{subject.comment}</TableCell>
											</TableRow>
										))}
									</TblContainer>
								</NgPaper>

								
								<NgPaper>
									<h5>Score</h5>
									<TblContainer>
										<TableHead>
											<TableRow>
												<TableCell>Total Marks</TableCell>
												<TableCell>Out Of</TableCell>
												<TableCell>Total Points</TableCell>
												<TableCell>Mean Marks</TableCell>
												<TableCell>Mean Points</TableCell>
												<TableCell>Mean Grade</TableCell>
											</TableRow>
										</TableHead>
										<TableRow>
											<TableCell>{academicReport?.score?.total_marks}</TableCell>
											<TableCell>{academicReport?.score?.total_out_of}</TableCell>
											<TableCell>{academicReport?.score?.total_points}</TableCell>
											<TableCell>{academicReport?.score?.mean_marks}</TableCell>   
											<TableCell>{academicReport?.score?.mean_points}</TableCell>
											<TableCell>{academicReport?.score?.mean_grade}</TableCell>                                                                                             
										</TableRow>
									</TblContainer>
								</NgPaper>
							</>

						)
			}
		</NgPageContainer>
	)
}

export default ReportDetailScreen