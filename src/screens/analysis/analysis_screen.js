import React, {useState} from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from 'react';
import { readAnalysisByTermAndYear } from './actions';

import { styled } from '@mui/material/styles';
import Chip from '@mui/material/Chip';
import { makeStyles } from '@mui/styles';
import { Grid, Paper } from '@mui/material';
import Loader from '../../components/display/Loader';
import ToastAlert from '../../components/display/ToastAlert';
import Controls from '../../components/controls/Controls';
import { MainForm } from '../../components/useForm';
import { FcCopyleft } from 'react-icons/fc';
import { NgPageContainer, NgPaper } from '../../components/display/elements';
import { Analysis, AnalysisWrapper } from './elements';
import { TabTitle } from '../../utils/globalFunc';


const terms = [
	{id: '1', title: 'One'},
	{id: '2', title: 'Two'},
	{id: '3', title: 'Three'},
]

const AnalysisScreen = () => {
	TabTitle('Analysis - Samis Systems')

	const dispatch = useDispatch();
	
	const analysisRead = useSelector(state => state.analysisRead);
	const { loading, error, analysis } = analysisRead

	const [year, setYear] = useState(2019)
	const [term, setTerm] = useState(1)

	const [clicked, setClicked] = useState(false)


	useEffect(() => {
		dispatch(readAnalysisByTermAndYear(year, term))
	}, [dispatch, year, term])

	return (
		<NgPageContainer>
			<NgPaper>
				<MainForm>
					<Grid container>
						<Grid item md={6} xs={8}>
							<Controls.InputField
								label="Enter Year"
								variant="outlined"
								value={year}
								onChange={(e) => setYear(e.target.value)}
							/>
						</Grid>
						<Grid item md={6} xs={4}>
							<Controls.SelectField
								label="Select Term"
								value={term}
								options={terms}
								onChange={(e) => setTerm(e.target.value)}
							/>
						</Grid>
					</Grid>
				</MainForm>
				
				
			{ loading
				? <Loader  />
				: error
				? <ToastAlert severity="error">{error}</ToastAlert>
				: (
					analysis?.length > 0 ?
					
						<AnalysisWrapper>
							<ToastAlert severity="info">{analysis?.length} analysis found for {year} term {term}</ToastAlert>
							{analysis?.map((analise) => {
								let icon;

								if (clicked) {
								icon = <FcCopyleft />;
								}
								return (
									
									<li key={analise}>
										<Analysis onClick={() => {navigator.clipboard.writeText(analise)}}>{analise}</Analysis>
									</li>
								)
							})}
						</AnalysisWrapper>
					: <ToastAlert severity="info">No Analysis Available for year {year} term {term}</ToastAlert> 
			)}
		</NgPaper>
	</NgPageContainer>
	)
}

export default AnalysisScreen