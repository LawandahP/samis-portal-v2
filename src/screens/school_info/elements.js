import styled from 'styled-components'
import { spacing } from '../../styles/variables';

export const SchoolInfoContainer = styled.div`
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    grid-gap: ${spacing.smSpacing};

    @media screen and (max-width: 768px) {
        grid-template-columns: 1fr 1fr;
    }

    @media screen and (max-width: 660px) {
        grid-template-columns: 1fr;
    }
`;

export const SchoolInfoCard = styled.div`
    background: ${({theme}) => theme.bg};
    padding: 10px;
    border-radius: 5px;
    box-shadow: 0 1px 3px rgba(0,0,0,0.2);
    justify-content: center;
    align-items: center;
`;

export const SchoolInfoCardText = styled.p`
    font-size: 14px;
`

export const SchoolInfoWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`
