import React, { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from "react-router-dom";

import Controls from '../../components/controls/Controls'
import ToastAlert from '../../components/display/ToastAlert'
import Loader from '../../components/display/Loader'

import { Grid } from '@mui/material';

import { createStatus, readStatus } from '../stepper/status/actions';
import { processPathName } from '../../utils/globalFunc';
import { MainForm, useForm } from '../../components/useForm';
import { FormButton, FormButtonWrapper } from '../../components/useForm/formElements';


const SchoolInfoForm = (props) => {
    const { handleCreateSchoolInfo, handleNext } = props;

    const dispatch = useDispatch();
    const location = useLocation();

    const schoolInfoCreate = useSelector(state => state.schoolInfoCreate)
    const { error, loading, success: successCreateSchoolInfo } = schoolInfoCreate


    const validate = (fieldValues = values) => {
        let temp = { ...errors }
        if ('vision' in fieldValues)
            temp.vision = fieldValues.vision ? "" : "Vision is Required"
        if ('school_code' in fieldValues)
            temp.school_code = fieldValues.school_code ? "" : "School Code is Required"
        if ('motto' in fieldValues)
            temp.motto = fieldValues.motto ? "" : "School Motto is Required"
        if ('mission' in fieldValues)
            temp.mission = fieldValues.mission ? "" : "School Mission is Required"

        setErrors({ ...temp })

        // tests whether post array elements passes text implemented by validate() function
        if (fieldValues === values)
            return Object.values(temp).every(x => x === "")

    }

    const initialValues = {
        school_code: '',
        vision: '',
        mission: '',
        motto: '',
    }

    const {
        values,
        errors,
        setErrors,
        handleResetForm,
        handleInputChange
    } = useForm(initialValues, true, validate);

    const submitHandler = (e) => {
        e.preventDefault()
        if (validate()) {
            const formData = new FormData()
            formData.append('school_code', values.school_code)
            formData.append('vision', values.vision)
            formData.append('mission', values.mission)
            formData.append('motto', values.motto)
            dispatch(handleCreateSchoolInfo(formData, handleResetForm))
            // handleResetForm()
        }
    }


    const statusRead = useSelector(state => state.statusRead)
    const {getStep} = statusRead

    let currentStep = getStep?.step
    console.log(currentStep)

    // const detectStepChange = useMemo(() => {
    //     return stepChange(currentStep)
    // }, [stepChange])


    // function stepChange(currentStep) {
    //     if (location.pathname === processPathName && currentStep > 1)
    //         handleNext()
    // }

    useEffect(() => {
        dispatch(readStatus())
        if (location.pathname === processPathName && currentStep > 1) {
            handleNext()
        }
        if (successCreateSchoolInfo) {
            dispatch(createStatus(2))
            // handleNext()
        }
    }, [successCreateSchoolInfo])

    return (


        <>
            {/* {error && <ToastAlert severity='error'>{error}</ToastAlert>} */}
            {/* <Typography className={classes.title}>Sign Up</Typography> */}
            <MainForm onSubmit={submitHandler}>
                <>
                    
                    <Grid item md={12} >
                        <Controls.Field
                            type="number"
                            error={errors.school_code}
                            label="School Code"
                            value={values.school_code}
                            name='school_code'
                            onChange={handleInputChange}>
                        </Controls.Field>

                        <Controls.InputField
                            multiline
                            rows={2}
                            error={errors.motto}
                            label="Motto"
                            value={values.motto}
                            name='motto'
                            onChange={handleInputChange}>
                        </Controls.InputField>

                        <Controls.InputField
                            multiline
                            rows={3}
                            error={errors.vision}
                            label="Vision"
                            value={values.vision}
                            name='vision'
                            onChange={handleInputChange}>
                        </Controls.InputField>

                        <Controls.InputField
                            multiline
                            rows={3}
                            error={errors.mission}
                            label="Mission"
                            value={values.mission}
                            name='mission'
                            onChange={handleInputChange}>
                        </Controls.InputField>
                    </Grid>

                    {loading ? <Loader />
                        :   <FormButtonWrapper>
                                <FormButton type='submit'>
                                    Submit
                                </FormButton>
                            </FormButtonWrapper>
                    }
                </>

            </MainForm>
        </>

    );
};

export default SchoolInfoForm;
