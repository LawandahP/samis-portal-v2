import axios from 'axios';
import { backend, bff } from '../../proxy';


import { 
    SCHOOL_INFO_CREATE_REQUEST,
    SCHOOL_INFO_CREATE_SUCCESS,
    SCHOOL_INFO_CREATE_FAIL,

    SCHOOL_INFO_READ_REQUEST,
    SCHOOL_INFO_READ_SUCCESS,
    SCHOOL_INFO_READ_FAIL,

    SCHOOL_INFO_DETAILS_REQUEST,
    SCHOOL_INFO_DETAILS_SUCCESS,
    SCHOOL_INFO_DETAILS_FAIL,

    SCHOOL_INFO_UPDATE_REQUEST,
    SCHOOL_INFO_UPDATE_SUCCESS,
    SCHOOL_INFO_UPDATE_FAIL,

    SCHOOL_INFO_DELETE_REQUEST,
    SCHOOL_INFO_DELETE_SUCCESS,
    SCHOOL_INFO_DELETE_FAIL,

} from './constants';



export const createSchoolInfo = (info) => async (dispatch, getState) => {
    try {
        dispatch({
            type: SCHOOL_INFO_CREATE_REQUEST
        })

        const config = {
            headers: {
                'Content-type':'multipart/form-data'
            }
        }
        
        const { data } = await axios.post(
            `${bff}/school-info`,
            info,
            config
        )

        dispatch({
            type: SCHOOL_INFO_CREATE_SUCCESS,
            success: true,
            payload: data
        })
        sessionStorage.setItem('schoolInfo', JSON.stringify(data))

    } catch(error) {
        dispatch({
            type: SCHOOL_INFO_CREATE_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.detail
                : error.message
        })

    }
}


export const readSchoolInfo = () => async (dispatch) => {
    try {
        dispatch({
            type: SCHOOL_INFO_READ_REQUEST
        })
        const { data } = await axios.get(`${bff}/school-info`)
        dispatch({
            type: SCHOOL_INFO_READ_SUCCESS,
            payload: data
        })

    } catch(error) {
        dispatch({
            type: SCHOOL_INFO_READ_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.detail
                : error.message
        })

    }
}



export const updateSchoolInfo = (schoolInfo) => async (dispatch) => {
    try {
        dispatch({
            type: SCHOOL_INFO_UPDATE_REQUEST
        })
        
        const { data } = await axios.put(
            `${backend}/school-info/v1/`,
            schoolInfo,
        )

        dispatch({
            type: SCHOOL_INFO_UPDATE_SUCCESS,
            payload: data
        })

        //update details
        dispatch({
            type: SCHOOL_INFO_DETAILS_SUCCESS,
            payload: data
        })

    } catch(error) {
        dispatch({
            type: SCHOOL_INFO_UPDATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


export const deleteSchoolInfo = () => async (dispatch, getState) => {
    try {
        dispatch({
            type: SCHOOL_INFO_DELETE_REQUEST
        })
        
        const { data } = await axios.delete(
            `${backend}/school-info/v1/`,
        )

        dispatch({
            type: SCHOOL_INFO_DELETE_SUCCESS,
        })

    } catch(error) {
        dispatch({
            type: SCHOOL_INFO_DELETE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


export const schoolInfoDetails = () => async (dispatch) => {
    try {
        dispatch({ type: SCHOOL_INFO_DETAILS_REQUEST })
        const { data } = await axios.get(`${backend}/school-info/v1`) //backend in package.json "http://127.0.0.1:8000/"

        dispatch({
            type:SCHOOL_INFO_DETAILS_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type:SCHOOL_INFO_DETAILS_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })
    }
}
