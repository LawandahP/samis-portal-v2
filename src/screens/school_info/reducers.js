  import { 
    SCHOOL_INFO_CREATE_REQUEST,
    SCHOOL_INFO_CREATE_SUCCESS,
    SCHOOL_INFO_CREATE_FAIL,

    SCHOOL_INFO_READ_REQUEST,
    SCHOOL_INFO_READ_SUCCESS,
    SCHOOL_INFO_READ_FAIL,

    SCHOOL_INFO_DETAILS_REQUEST,
    SCHOOL_INFO_DETAILS_SUCCESS,
    SCHOOL_INFO_DETAILS_FAIL,

    SCHOOL_INFO_UPDATE_REQUEST,
    SCHOOL_INFO_UPDATE_SUCCESS,
    SCHOOL_INFO_UPDATE_FAIL,

    SCHOOL_INFO_DELETE_REQUEST,
    SCHOOL_INFO_DELETE_SUCCESS,
    SCHOOL_INFO_DELETE_FAIL,

} from './constants';


export const schoolInfoCreateReducer = (state = { }, action) =>{
    switch(action.type) {
        case SCHOOL_INFO_CREATE_REQUEST:
            return {loading: true}
        
        case SCHOOL_INFO_CREATE_SUCCESS:
            return {loading: false, success: true, schoolInfo: action.payload.data}
        
        case SCHOOL_INFO_CREATE_FAIL:
            return {loading: false, error: action.payload}
        
        // case DEFAULT_USER_LOGOUT:
        //     return {}   
             
        default:
            return state
    }
}

export const schoolInfoReadReducer = (state = { schoolInfo: {}}, action) => {
    switch(action.type) {
        case SCHOOL_INFO_READ_REQUEST:
            return {
                loading: true,
                schoolInfo: {}
            }
        case SCHOOL_INFO_READ_SUCCESS:
            return {
                loading: false,
                schoolInfo: action.payload.data
            }

        case SCHOOL_INFO_READ_FAIL:
            return {
                loading: false,
                error: action.payload,
            }
        
        default:
            return state
    }
}

export const schoolInfoDetailsReducer = (state = { schoolInfo: { } }, action) =>{
    switch(action.type) {
        case SCHOOL_INFO_DETAILS_REQUEST:
            return {loading: true, ...state}
        
        case SCHOOL_INFO_DETAILS_SUCCESS:
            return {loading: false, schoolInfo: action.payload.data}
        
        case SCHOOL_INFO_DETAILS_FAIL:
            return {loading: false, error: action.payload}
        
        default:
            return state
    }
}


export const schoolInfoUpdateReducer = (state = { schoolInfo: {} }, action) =>{
    switch(action.type) {
        case SCHOOL_INFO_UPDATE_REQUEST:
            return {loading: true}
        
        case SCHOOL_INFO_UPDATE_SUCCESS:
            return {loading: false, success: true, schoolInfo: action.payload}
        
        case SCHOOL_INFO_UPDATE_FAIL:
            return {loading: false, error: action.payload}
        
        default:
            return state
    }
}

export const schoolInfoDeleteReducer = (state = {}, action) =>{
    switch(action.type) {
        case SCHOOL_INFO_DELETE_REQUEST:
            return {loading: true}
        
        case SCHOOL_INFO_DELETE_SUCCESS:
            return {loading: false, success: true}
        
        case SCHOOL_INFO_DELETE_FAIL:
            return {loading: false, error: action.payload}
        
        default:
            return state
    }
}

