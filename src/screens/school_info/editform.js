import React, {useState, useEffect, useMemo} from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Controls from '../../components/controls/Controls'
import ToastAlert from '../../components/display/ToastAlert'
import Loader from '../../components/display/Loader'

import { makeStyles } from '@mui/styles';
import { Grid } from '@mui/material';

// import { createStatus, getStatus } from '../stepper/stepperStatus/statusActions';
import { useLocation } from 'react-router-dom';
// import { processPathName } from '../utils/globalFunc';
import { MainForm, useForm } from '../../components/useForm';
import { FormButton } from '../../components/useForm/formElements';

function SchoolInfoEditForm(props) {
    const { editEntry, recordForEdit, schoolInfoId } = props;
    const dispatch = useDispatch();
 
    const schoolInfoUpdate = useSelector(state => state.schoolInfoUpdate)
    const { loading, error, success } = schoolInfoUpdate

    // const [successMessage, setSuccessMessage] = useState('')
    
    const validate = (fieldValues = values) => {
        let temp = {...errors}
        if('vision' in fieldValues)
            temp.vision = fieldValues.vision ? "" : "Vision is Required"
        if('school_code' in fieldValues)
            temp.school_code = fieldValues.school_code ? "" : "School Code is Required"
        if('motto' in fieldValues)
            temp.motto = fieldValues.motto ? "" : "School Motto is Requied"
        if('mission' in fieldValues)
            temp.mission = fieldValues.mission ? "" : "School Mission is Required"
       
        setErrors({ ...temp })

        // tests whether post array elements passes text implemented by validate() function
        if (fieldValues === values)
            return Object.values(temp).every(x => x === "")
        
    }

    const initialFValues = {
        school_code: '',
        vision: '',
        mission: '',
        motto: '',
    }

    const { values,  
            setValues,
            errors, 
            setErrors, 
            handleResetForm, 
            handleInputChange } = useForm(initialFValues, true, validate);

    
    
    useEffect(() => {
        if(recordForEdit != null)
            setValues({
                ...recordForEdit
            })
    
    }, [dispatch, recordForEdit, success, loading, error, setValues, schoolInfoId ])


    const submitHandler = (e) => {
        e.preventDefault()
        if (validate()) {
            editEntry(values, handleResetForm);
            handleResetForm()
        }
        
    }

   
    return (        
            
        <>
            {/* <Typography className={classes.title}>Sign Up</Typography> */}

                {error && <ToastAlert severity='error'>{error}</ToastAlert>}
                <MainForm onSubmit={submitHandler}>
                {loading ? <Loader /> 
                    :  (
                        <>
                            <Grid item md={12} >
                                <Controls.InputField
                                    type="number"
                                    error={errors.school_code}
                                    label="School Code"
                                    value={values.school_code}
                                    name='school_code'
                                    onChange={handleInputChange}>
                                </Controls.InputField>

                                <Controls.InputField
                                    multiline
                                    rows={2}
                                    error={errors.motto}
                                    label="Motto"
                                    value={values.motto}
                                    name='motto'
                                    onChange={handleInputChange}>
                                </Controls.InputField>
                                
                                <Controls.InputField
                                    multiline
                                    rows={3}
                                    error={errors.vision}
                                    label="Vision"
                                    value={values.vision}
                                    name='vision'
                                    onChange={handleInputChange}>
                                </Controls.InputField>

                                <Controls.InputField
                                    multiline
                                    rows={3}
                                    error={errors.mission}
                                    label="Mission"
                                    value={values.mission}
                                    name='mission'
                                    onChange={handleInputChange}>
                                </Controls.InputField>
                            </Grid>

                            <FormButton type='submit'>
                                Submit 
                            </FormButton>
                        </>
                    )
                }
                </MainForm>
            
            </>
    );
};

export default SchoolInfoEditForm
