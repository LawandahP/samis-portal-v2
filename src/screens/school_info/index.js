import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import ToastAlert from '../../components/display/ToastAlert'
import Loader from '../../components/display/Loader'
import Modal from '../../components/display/modal'

import { createSchoolInfo, deleteSchoolInfo,
         readSchoolInfo, updateSchoolInfo 
} from './actions'

import { TabTitle } from '../../utils/globalFunc'
import { Avatar, Card, CardContent, CardHeader, Grid, Typography } from '@mui/material'
import ConfirmDialog from '../../components/display/dialog'

import SchoolInfoEditForm from './editform';
import SchoolInfoForm from './form';
import { SchoolInfoCard, SchoolInfoCardText, SchoolInfoContainer, SchoolInfoWrapper } from './elements'
import { FlexWrapper } from '../user.elements'
import Controls from '../../components/controls/Controls'
import { ActionButtonWrapper } from '../../components/controls/Button'
import { FiDelete, FiEdit3 } from 'react-icons/fi'



function SchoolInfoReadScreen() {

    TabTitle('School Info - Samis Systems')

    const dispatch = useDispatch();
 
    const [ recordForEdit, setRecordForEdit ] = useState(null);

    const [ openPopup, setOpenPopup ] = useState(false)
    const [ openModal, setOpenModal ] = useState(false)

    const [ message, setMessage ] = useState('')

    const schoolInfoRead = useSelector(state => state.schoolInfoRead)
    const { loading, error, schoolInfo } = schoolInfoRead

    const schoolInfoCreate = useSelector(state => state.schoolInfoCreate)
    const { success: successCreate } = schoolInfoCreate

    const schoolInfoUpdate = useSelector(state => state.schoolInfoUpdate)
    const { success: successUpdate } = schoolInfoUpdate

    const schoolInfoDelete = useSelector(state => state.schoolInfoDelete)
    const { success: successDelete } = schoolInfoDelete

    const [ confirmDialog, setConfirmDialog ] = useState({isOpen: false, title: '', subTitle: ''})

    
    useEffect(() => {
        dispatch(readSchoolInfo())
        if(successUpdate){
            setMessage("Successfully Updated")
        }
    }, [dispatch, successCreate, successUpdate, successDelete])



    const handleCreateSchoolInfo = (schoolInfo, handleResetForm) => {
        dispatch(createSchoolInfo(schoolInfo))
        setOpenModal(false);
        handleResetForm()
    }

    const editEntry = (schoolInfo, handleResetForm) => {
        dispatch(updateSchoolInfo(schoolInfo))
        if (successUpdate)
            setOpenPopup(false);
            handleResetForm()   
    }

    const editHandler = (schoolInfo) => { 
        setRecordForEdit(schoolInfo)
        setOpenPopup(true)
    }

    const deleteHandler = () => {
        setConfirmDialog({
            ...confirmDialog,
            isOpen: false
        })
        dispatch(deleteSchoolInfo())  
    }

    
    return (
        <>  
            { message && <ToastAlert severity="success">{message}</ToastAlert>}

            { loading
            ? <Loader />
            : error
            ? <ToastAlert severity="error">{error}</ToastAlert>
            : schoolInfo ? (
                <SchoolInfoContainer>
                    <SchoolInfoCard>
                        <SchoolInfoWrapper>
                            <Avatar src={schoolInfo?.logo_url} aria-label="recipe">
                                R
                            </Avatar>

                            <ActionButtonWrapper>
                                <Controls.ActionButton
                                    title="edit"
                                    onClick={() => editHandler(schoolInfo)}
                                    edit>
                                    <FiEdit3 />
                                </Controls.ActionButton>
                                <Controls.ActionButton
                                    title="deactivate"
                                    onClick={() => {
                                        setConfirmDialog({
                                            isOpen: true,
                                            title: "Are you sure you want to delete this Contact?",
                                            subTitle: "You can't undo this operation",
                                            onConfirm: () => { deleteHandler() }
                                        })
                                    }}>
                                    <FiDelete />
                                </Controls.ActionButton>
                            </ActionButtonWrapper>
                        </SchoolInfoWrapper>

                        <SchoolInfoCardText>{schoolInfo?.school_name}</SchoolInfoCardText>
                        <SchoolInfoCardText>{schoolInfo?.school_code}</SchoolInfoCardText>
                    </SchoolInfoCard>

                    <SchoolInfoCard>
                        <h4>Motto</h4>
                        <SchoolInfoCardText>{schoolInfo?.motto}</SchoolInfoCardText>
                    </SchoolInfoCard>

                    <SchoolInfoCard>
                        <h4>Mission</h4>
                        <SchoolInfoCardText>{schoolInfo?.mission}</SchoolInfoCardText>
                    </SchoolInfoCard>

                    <SchoolInfoCard>
                        <h4>Vision</h4>
                        <SchoolInfoCardText>{schoolInfo?.vision}</SchoolInfoCardText>
                    </SchoolInfoCard>


                    
                </SchoolInfoContainer>
                
            ): <Typography sx={{paddingTop: "5rem"}}>No data</Typography>}
        
            <Modal
                openModal = {openModal}
                setOpenModal = {setOpenModal}
                title="Create School Info"
            >
                <SchoolInfoForm 
                    recordForEdit = {recordForEdit}
                    handleCreateSchoolInfo = {handleCreateSchoolInfo}/>  
            </Modal>  

            <Modal
                openPopup = {openPopup}
                setOpenPopup = {setOpenPopup}
                title="Edit School Info"
            >
                <SchoolInfoEditForm
                    recordForEdit = {recordForEdit}
                    editEntry = {editEntry}
                />
            </Modal> 

            <ConfirmDialog 
                confirmDialog={confirmDialog}
                setConfirmDialog={setConfirmDialog} />

        </>
        
    )
}

export default SchoolInfoReadScreen



{/* <Grid container>
                    <Grid item md={12} xs={12}>
                        <Card>
                            <CardHeader
                                avatar={
                                <Avatar src={schoolInfo.logo_url} aria-label="recipe">
                                    R
                                </Avatar>
                                }

                                title={schoolInfo?.school_name}
                                subheader={schoolInfo?.school_code}
                            />
                            <CardContent>
                                <Typography variant="body2" color="text.secondary">
                                    {schoolInfo.motto}
                                </Typography>

                                <Typography variant="body2" color="text.secondary">
                                    {schoolInfo.mission}
                                </Typography>

                                <Typography variant="body2" color="text.secondary">
                                    {schoolInfo.vision}
                                </Typography>
                            </CardContent>

                            <CardActions disableSpacing>
                                <Controls.ActionButton
                                        color="primary">
                                        <EditOutlinedIcon 
                                            onClick={() => editHandler(schoolInfo)}
                                            fontSize="small" />
                                    </Controls.ActionButton>
                                    <Controls.ActionButton 
                                        color="secondary"
                                        onClick={() => {
                                            setConfirmDialog({
                                                isOpen: true,
                                                title: "Are you sure you want to delete this schoolInfo?",
                                                subTitle: "You can't undo this operation",
                                                onConfirm: () => { deleteHandler()  }
                                            })
                                            
                                        }}>
                                        <DeleteOutlineIcon 
                                            fontSize="small" />
                                    </Controls.ActionButton>
                            </CardActions>
                        </Card>
                    </Grid>
                </Grid> */}