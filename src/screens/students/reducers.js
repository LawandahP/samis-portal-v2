import { 
    BUILDING_CREATE_REQUEST,
    BUILDING_CREATE_SUCCESS,
    BUILDING_CREATE_FAIL,
    BUILDING_CREATE_RESET,

    BUILDING_READ_REQUEST,
    BUILDING_READ_SUCCESS,
    BUILDING_READ_FAIL,
    BUILDING_READ_RESET,

    BUILDING_UPDATE_REQUEST,
    BUILDING_UPDATE_SUCCESS,
    BUILDING_UPDATE_FAIL,
    BUILDING_UPDATE_RESET,

    BUILDING_DELETE_REQUEST,
    BUILDING_DELETE_SUCCESS,
    BUILDING_DELETE_FAIL,
    
    BUILDING_DETAILS_REQUEST,
    BUILDING_DETAILS_SUCCESS,
    BUILDING_DETAILS_FAIL,
    BUILDING_DETAILS_RESET,

    BUILDING_TYPE_READ_REQUEST,
    BUILDING_TYPE_READ_SUCCESS,
    BUILDING_TYPE_READ_FAIL

} from './constants';


export const buildingCreateReducer = (state = {}, action) => {
    switch(action.type) {
        case BUILDING_CREATE_REQUEST:
            return {loading: true}
        
        case BUILDING_CREATE_SUCCESS:
            return {loading: false, success: true, message: action.success, buildings: action.payload}
        
        case BUILDING_CREATE_FAIL:
            return {loading: false, error: action.payload}
        
        case BUILDING_CREATE_RESET:
            return {}
        default:
            return state
    }
}


export const buildingReadReducer = (state = { buildings:[] }, action) =>{
    switch(action.type) {
        case BUILDING_READ_REQUEST:
            return {loading: true, buildings:[]}
        
        case BUILDING_READ_SUCCESS:
            return {
                        loading: false,
                        buildings: action.payload.data.payload,
                        count: action.payload.data.count
                    }
        
        case BUILDING_READ_FAIL:
            return {loading: false, error: action.payload}
        
        case BUILDING_READ_RESET:
            return { buildings: [] }
        
        default:
            return state
    }
}


export const buildingDetailsReducer = (state = { building: {} }, action) => {
    switch(action.type) {
        case BUILDING_DETAILS_REQUEST:
            return {loading: true, ...state}
        
        case BUILDING_DETAILS_SUCCESS:
            return {loading: false, building: action.payload.data.payload}
        
        case BUILDING_DETAILS_FAIL:
            return {loading: false, error: action.payload}
        
        case BUILDING_DETAILS_RESET:
            return {building: {} }
        
        default:
            return state
    }
}


export const buildingUpdateReducer = (state = { building:{} }, action) => {
    switch(action.type) {
        case BUILDING_UPDATE_REQUEST:
            return { loading: true }
        
        case BUILDING_UPDATE_SUCCESS:
            return { loading: false, success: true }
        
        case BUILDING_UPDATE_FAIL:
            return { loading: false, error: action.payload }  
        
        case BUILDING_UPDATE_RESET:
            return { building:{} }
             
        default:
            return state
    }
}



export const buildingDeleteReducer = (state = {}, action) => {
    switch(action.type) {
        case BUILDING_DELETE_REQUEST:
            return { loading: true }
        
        case BUILDING_DELETE_SUCCESS:
            return { loading: false, success: true }
        
        case BUILDING_DELETE_FAIL:
            return { loading: false, error: action.payload }  
             
        default:
            return state
    }
}


// Building types Reducer

export const buildingTypeReadReducer = (state = { building_types:[] }, action) =>{
    switch(action.type) {
        case BUILDING_TYPE_READ_REQUEST:
            return {loading: true, building_types:[]}
        
        case BUILDING_READ_SUCCESS:
            return {
                        loading: false,
                        building_types: action.payload.data.payload,
                        count: action.payload.data.count
                    }
        
        case BUILDING_READ_FAIL:
            return {loading: false, error: action.payload}
        
        case BUILDING_READ_RESET:
            return { building_types: [] }
        
        default:
            return state
    }
}