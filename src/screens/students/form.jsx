// import React, {useContext} from 'react'
// import { useDispatch, useSelector } from 'react-redux'
// import Controls from '../../components/controls/Controls'
// import ToastAlert from '../../components/display/ToastAlert'
// import Loading from '../../components/display/Loader'

// import { MainForm, useForm } from '../../components/useForm'
// import { toTitleCase } from '../../utils/globalFunc'
// import { FormButton } from '../../components/useForm/formElements';

// import TextField from '@mui/material/TextField';
// import Autocomplete from '@mui/material/Autocomplete';
// import { readBuildingTypesAction } from './actions';
// import { readLandlordsAction } from '../landlords/actions';


// import { useTheme } from '@mui/material/styles';
// import Box from '@mui/material/Box';
// import OutlinedInput from '@mui/material/OutlinedInput';
// import InputLabel from '@mui/material/InputLabel';
// import MenuItem from '@mui/material/MenuItem';
// import FormControl from '@mui/material/FormControl';
// import Select from '@mui/material/Select';
// import Chip from '@mui/material/Chip';
// import { BuildingPropertiesContext } from '../../context';

// const ITEM_HEIGHT = 48;
// const ITEM_PADDING_TOP = 8;
// const MenuProps = {
//   PaperProps: {
//     style: {
//       maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
//       width: 250,
//     },
//   },
// };

// const amenitiez = [
//   'Oliver Hansen',
//   'Van Henry',
//   'April Tucker',
//   'Ralph Hubbard',
//   'Omar Alexander',
//   'Carlos Abbott',
//   'Miriam Wagner',
//   'Bradley Wilkerson',
//   'Virginia Andrews',
//   'Kelly Snyder',
// ];

// function getStyles(name, val, theme) {
//   return {
//     fontWeight:
//       val.indexOf(name) === -1
//         ? theme.typography.fontWeightRegular
//         : theme.typography.fontWeightMedium,
//   };
// }




// const BuildingForm = (props) => {
//     const { newEntry } = props;

//     const { types, setTypes, landlords, setLandlords, amenitiez,
//         setAmenities, loading, setLoading, error, setError} = useContext(BuildingPropertiesContext)

//     const createBuilding = useSelector(state => state.createBuilding)
//     const { loading: loadingCreate, error: errorCreate, success } = createBuilding

//     const readBuildingTypes = useSelector(state => state.readBuildingTypes)
//     const { loading: loadingtypes, success: successTypes, building_types } = readBuildingTypes



//     const options = types?.map((option) => {
//         const firstLetter = option.building_model[0].toUpperCase();
//         return {
//             firstLetter:/[0-9]/.test(firstLetter) ? '0-9' : firstLetter,
//             ...option,
//         }
//     });

//     const validate = (fieldValues = values) => {
//         let temp = {...errors}
//         if('name' in fieldValues)
//             temp.name = fieldValues.name ? "" : "Name is Required"        
//         setErrors({ ...temp })

//         // tests whether post array elements passes text implemented by validate() function
//         if (fieldValues === values)
//             return Object.values(temp).every(x => x === "")
//     }
    
//     const initialValues = {
//         name: '',
//         address: '',
//         building_type: '',
//         amenities: [

//         ],
//         description: '',
//         owner: ''
//     }

//     const { 
//         values,  
//         errors, 
//         setValues,
//         setErrors,
//         handleResetForm,
//         handleInputChange
//     } = useForm(initialValues, true, validate);

//     const submitHandler = (e) => {
//         e.preventDefault()
//         if (validate()) {
//             newEntry(values, handleResetForm);
//         } 
//     }


//     const theme = useTheme();
//     const [val, setVal] = React.useState(values.amenities);

//     const handleAmenitiesOnChange = e => {
//         let amenities = values.amenities;
//         var key = e.target.name;
//         var value = e.target.value;
//         amenities[key] = value;
//         setValues({
//             ...values,
//             amenities,
//             [key]:value
//         });
//     }
  
//     const handleChange = (event) => {
//       const {
//         target: { name, value },
//       } = event;
//       setVal(
//         // On autofill we get a stringified value.
//         typeof value === 'string' ? value.split(',') : value,
//       );
//     };

  
//     return (
//         <>
//             {/* { loading && <Loading />} */}
//             { errorCreate && <ToastAlert severity="error">{errorCreate}</ToastAlert>}

           
//                 <MainForm onSubmit={submitHandler}>
//                     <Controls.InputField 
//                         value={values.name}
//                         name='name'
//                         onChange={handleInputChange}
//                         label='Name' 
//                         error={errors.name}
//                         onInput={(e) => e.target.value = toTitleCase(e.target.value)}>        
//                     </Controls.InputField>
                    
//                     <Controls.InputField
//                         value={values.address}
//                         name='address'
//                         onChange={handleInputChange}
//                         label='Address' 
//                         error={errors.address}>
//                     </Controls.InputField>

//                     <Controls.SelectField
//                         label="Building Type"
//                         value={values.building_type}
//                         name="building_type"
//                         onChange={handleInputChange}
//                         list_value="building_model"
//                         options={types}
//                     />

//                     <Controls.SelectField
//                         label="Owner"
//                         value={values.owner}
//                         name="owner"
//                         onChange={handleInputChange}
//                         list_value="full_name"
//                         options={landlords}
//                     />
                    

//                     <FormControl sx={{ m: 1, width: 300 }}>
//                         <InputLabel id="demo-multiple-chip-label">Amenities</InputLabel>
//                         <Select
//                             labelId="demo-multiple-chip-label"
//                             id="demo-multiple-chip"
//                             multiple
//                             value={val}
//                             onChange={handleChange}
//                             input={<OutlinedInput id="select-multiple-chip" label="Amenities" />}
//                             renderValue={(selected) => (
//                             <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
//                                 {selected.map((value) => (
//                                 <Chip key={value} label={value} />
//                                 ))}
//                             </Box>
//                             )}
//                             MenuProps={MenuProps}
//                         >
//                             {amenitiez?.map((amenity) => (
//                                 <MenuItem
//                                     key={amenity.name}
//                                     value={amenity.name}
//                                     style={getStyles(amenity, val, theme)}
//                                     >
//                                     {amenity.name}
//                                 </MenuItem>
//                             ))}
//                         </Select>
//                     </FormControl>
//                     {/* <Autocomplete
//                         disablePortal
//                         id="combo-box-demo"
//                         options={options?.sort((a, b) => -b.firstLetter.localeCompare(a.firstLetter))}
//                         groupBy={(option) => option.firstLetter}
//                         getOptionLabel={(option) => option.building_model}
//                         value={values.building_type}
//                         onChange={handleInputChange}
//                         // sx={{ width: 300 }}
//                         // renderInput={
//                         //     types?.map((item) => (
//                         //         <TextField key={item.id} value={item.id}>
//                         //             {item.building_model}
//                         //         </TextField>
//                         //     ))}
                            
//                         renderInput={(params) => <TextField {...params} label="Building Type" />}
//                     />
//                      */}
//                     <Controls.InputField
//                         value={values.description}
//                         name='description'
//                         onChange={handleInputChange}
//                         label='Description' 
//                         error={errors.description}>
//                     </Controls.InputField>
                            
//                     <FormButton type='submit'>
//                         Submit { loadingCreate ? <Loading /> : ""}
//                     </FormButton> 
//                 </MainForm>
                 
//             {/* </Container> */}
//         </>
//     )
// }

// export default BuildingForm