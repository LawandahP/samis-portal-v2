import React, { useEffect, useState } from 'react'
import axios from 'axios';

import { useDispatch, useSelector } from 'react-redux'

import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';

import { FiDelete, FiEdit3 } from 'react-icons/fi'

import { TabTitle, toTitleCase } from '../../utils/globalFunc'



import studentForm from './form'
import studentEditForm from './editform'

import Controls from '../../components/controls/Controls'
import ToastAlert from '../../components/display/ToastAlert'
import Loading from '../../components/display/Loader'
import TableComponent from '../../components/useTable/index'
import { Chip, TableTop } from '../../components/useTable/elements'
import { ActionButtonWrapper } from '../../components/controls/Button'
import ConfirmDialog from '../../components/display/dialog'
import Modal from '../../components/display/modal'
import { List, NgLink, NgPageContainer, NgPaper } from '../../components/display/elements'

import { studentPropertiesContext } from '../../context';
import { backend } from '../../proxy';


const headCells = [
    { id: 'student_name', label: 'Student Name', minWidth:170 },
    { id: 'parent_name', label: 'Parent Name', minWidth:170 },
    { id: 'phone_no', label: 'Phone No', minWidth:150 },
    { id: 'email', label: 'Email' },
    { id: 'gender', label: 'Gender'},
    { id: 'birth_cert_no', label: 'Birth Cert No', minWidth:200  },
    { id: 'date_of_bith', label: 'D.O.B'},
    { id: 'term_admitted', label: 'Term of Admission' },
    { id: 'enrollment_type', label: 'Enrollment Type' },
    { id: 'admission_date', label: 'Adm Date' },
    { id: 'admission_no', label: 'Adm No.'},
    { id: 'stream_name', label: 'Stream' },
    { id: 'class_of', label: 'Class Of' },
    { id: 'house', label: 'House' },
    { id: 'upi', label: 'Upi' },
    { id: 'KCPE_grade', label: 'Kcpe Grade'},
    { id: 'KCPE_marks', label: 'Marks'},
    { id: 'KCPE_position', label: 'Position'},
    { id: 'KCPE_index_no', label: 'Index No.'},
    { id: 'KCPE_meanMarks', label: 'Mean'},
    { id: 'KCPE_points', label: 'Points', minWidth: 100 },
];



function StudentReadScreen() {
    TabTitle('Students - Samis Systems')

    const [readError, setReadError] = useState(null);
    const [readLoading, setReadLoading] = useState(false)
    const [students, setStudents] = useState([])

    const [message, setMessage] = useState(null)

    const [openModal, setOpenModal] = useState(false);
    const [openPopup, setOpenPopup] = useState(false);
    const [confirmDialog, setConfirmDialog] = useState({ isOpen: false, title: '', subTitle: '' })

    const [search, setSearch] = useState({ fn: items => { return items; } })
    const [recordForEdit, setRecordForEdit] = useState(null);

    const getStudents = async (page, size) => {
        setReadLoading(true)
        await axios.get(`${backend}/school/students/v1?page=${page}&size=${size}`).then(res => {
            setReadLoading(false)
            setStudents(res?.data?.data?.items)
        }).catch(err => {
            setReadError(err.response && err.response.data.detail ?
                <>
                    {Object.keys(err.response.data.detail).map(function (s) {
                        return (
                            <List>{err.response.data.detail[s]}</List>
                        )
                    })}
                </>
                : err.message)
            setReadLoading(false)
        })
    }


    const { TblContainer,
            TblHead,
            TblPagination,
            recordsAfterPaginatingAndSorting,
            page, rowsPerPage
    } = TableComponent(students, headCells, search)

    useEffect(() => {
        getStudents(page, rowsPerPage);
    }, [page, rowsPerPage])

    const handleSearch = (e) => {
        e.preventDefault()
        let target = e.target
        setSearch({
            fn: items => {
                if (target.value === "")
                    return items;
                else
                    return items.filter(x => x.student_name.includes(toTitleCase(e.target.value)))
            }
        })
    }

    return (
        <NgPageContainer>
        <NgPaper>
                {/* {successCreate && <ToastAlert severity="success">{message}</ToastAlert>} */}
                <TableTop>
                    <Controls.SearchInputField
                        label="Search students"
                        onChange={handleSearch}
                    />
                    <Controls.AddButton
                        onClick={() => setOpenModal(true)}
                    >
                    </Controls.AddButton>
                </TableTop>


                {readLoading
                    ? <Loading />
                    : readError
                        ? <ToastAlert severity="error">{readError}</ToastAlert>
                        : (

                            <div>
                                <TblContainer>
                                    <TblHead />

                                    <TableBody>
                                        {
                                            recordsAfterPaginatingAndSorting()?.map(student =>
                                            (
                                                <TableRow key={student.admission_no}>
                                                    <TableCell>{student.student_name}</TableCell>
                                                    <TableCell>{student.parent_name}</TableCell>
                                                    <TableCell>{student.phone_no}</TableCell>
                                                    <TableCell>{student.email}</TableCell>
                                                    <TableCell>{student.gender}</TableCell>
                                                    <TableCell>{student.birth_cert_no}</TableCell>
                                                    <TableCell>{student.date_of_birth}</TableCell>
                                                    <TableCell>{student.term_admitted}</TableCell>
                                                    <TableCell>{student.enrollment_type}</TableCell>
                                                    <TableCell>{student.admission_date}</TableCell>
                                                    <TableCell>{student.admission_no}</TableCell>
                                                    <TableCell>{student.stream_name}</TableCell>
                                                    <TableCell>{student.class_of}</TableCell>
                                                    <TableCell>{student.house}</TableCell>
                                                    <TableCell>{student.upi}</TableCell>

                                                    <TableCell>{student.KCPE_info.KCPE_grade}</TableCell>
                                                    <TableCell>{student.KCPE_info.KCPE_marks}</TableCell>
                                                    <TableCell>{student.KCPE_info.KCPE_position}</TableCell>
                                                    <TableCell>{student.KCPE_info.KCPE_index_no}</TableCell>
                                                    <TableCell>{student.KCPE_info.KCPE_meanMarks}</TableCell>
                                                    <TableCell>{student.KCPE_info.KCPE_points}</TableCell>

                                                    {/* <TableCell> 
                                                        <ActionButtonWrapper>
                                                            <Controls.ActionButton
                                                                title="edit"
                                                                onClick={() => editHandler(student)}
                                                                edit>
                                                                <FiEdit3 />
                                                            </Controls.ActionButton>
                                                            <Controls.ActionButton
                                                                title="deactivate"
                                                                onClick={() => {
                                                                    setConfirmDialog({
                                                                        isOpen: true,
                                                                        title: "Are you sure you want to delete this student?",
                                                                        subTitle: "You can't undo this operation",
                                                                        onConfirm: () => { deleteHandler(student.slug) }
                                                                    })
                                                                }}>
                                                                <FiDelete />
                                                            </Controls.ActionButton>
                                                        </ActionButtonWrapper>
                                                    </TableCell> */}
                                                </TableRow>
                                            )
                                            )}
                                    </TableBody>
                                </TblContainer>
                                <TblPagination />
                            </div>

                        )}
            


            {/* <Modal
                    openModal={openModal}
                    setOpenModal={setOpenModal}
                    title="Create student"
                >
                    <studentForm
                        newEntry={newEntry}
                    />
                </Modal>

                <Modal
                    openPopup={openPopup}
                    setOpenPopup={setOpenPopup}
                    title="Edit student"
                >
                    <studentEditForm
                        recordForEdit={recordForEdit}
                        editEntry={editEntry}
                    />
                </Modal> */}

            <ConfirmDialog
                confirmDialog={confirmDialog}
                setConfirmDialog={setConfirmDialog}
            />
        </NgPaper>
        </NgPageContainer>

    )
}


export default StudentReadScreen



