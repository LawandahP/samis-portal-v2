import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom'
import Loading from '../../components/display/Loader';
import ToastAlert from '../../components/display/ToastAlert';

import { buildingDetailsAction } from './actions';



const BuildingDetailsScreen = () => {
    const match = { params: useParams() }
    const dispatch = useDispatch()

    const buildingDetails = useSelector(state => state.buildingDetails)
    const { loading, error, building } = buildingDetails

    useEffect(() => {
        dispatch(buildingDetailsAction(match.params.id))
    }, [dispatch])

    return (
        <>
            {error && <ToastAlert severity="error">{error}</ToastAlert>}
            {
                loading ? <Loading />
                : 
                <>
                    {building.full_name}
                </>
            }

        </>
    )
}

export default BuildingDetailsScreen;
