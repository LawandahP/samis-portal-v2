import React, { useEffect } from 'react';
import { MdEmail, MdOutlineHouse, MdPhoneAndroid } from 'react-icons/md';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom'
import Loading from '../../components/display/Loader';
import ToastAlert from '../../components/display/ToastAlert';

import { tenantDetailsAction } from './actions';
import { UserInfoWrapper, ProfilePicture, UserCard, UserName, DetailsWrapper, Status, Icon, IconText, FlexWrapper, InfoWrapper } from '../user.elements';



const TenantDetailsScreen = () => {
    const match = { params: useParams() }
    const dispatch = useDispatch()

    const tenantDetails = useSelector(state => state.tenantDetails)
    const { loading, error, tenant, unit } = tenantDetails

    useEffect(() => {
        dispatch(tenantDetailsAction(match.params.id))
    }, [dispatch])

    return (
        <>
            {error && <ToastAlert severity="error">{error}</ToastAlert>}
            {
                loading ? <Loading />
                    :
                    <UserCard>
                        <UserInfoWrapper>
                            <ProfilePicture src="https://images.pexels.com/photos/11650772/pexels-photo-11650772.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" />
                            <DetailsWrapper>
                                <UserName>{tenant?.full_name}</UserName>
                                {tenant?.is_active === true ? <Status active>active</Status> : <Status>dormant</Status>}
                                <FlexWrapper>
                                    <Icon><MdOutlineHouse /></Icon>
                                    <IconText>{unit?.unit_no}</IconText>
                                </FlexWrapper>
                            </DetailsWrapper>
                        </UserInfoWrapper>

                        <InfoWrapper>
                            <FlexWrapper>
                                <Icon><MdOutlineHouse /></Icon>
                                <IconText>{unit?.unit_no}</IconText>
                            </FlexWrapper>
                            <FlexWrapper>
                                <Icon><MdPhoneAndroid /></Icon>
                                <IconText>{tenant?.phone_number}</IconText>
                            </FlexWrapper>
                            <FlexWrapper>
                                <Icon><MdEmail /></Icon>
                                <IconText>{tenant?.email}</IconText>
                            </FlexWrapper>
                        </InfoWrapper>
                    </UserCard>
            }

        </>
    )
}

export default TenantDetailsScreen;
