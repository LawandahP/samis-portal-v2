import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';

import { FiDelete, FiEdit3 } from 'react-icons/fi'

import { TabTitle, toTitleCase } from '../../utils/globalFunc'
import { createTenantAction, deleteTenantAction, readTenantsAction, updateTenantAction } from './actions'


import TenantForm from './form'
import TenantEditForm from './editform'

import Controls from '../../components/controls/Controls'
import ToastAlert from '../../components/display/ToastAlert'
import Loading from '../../components/display/Loader'
import TableComponent from '../../components/useTable/index'
import { Chip, TableTop } from '../../components/useTable/elements'
import { ActionButtonWrapper } from '../../components/controls/Button'
import ConfirmDialog from '../../components/display/dialog'
import Modal from '../../components/display/modal'
import { NgLink, NgPageContainer, NgPaper } from '../../components/display/elements'


const headCells = [
    { id: 'full_name', label: 'Full Name', minWidth: 170 },
    { id: 'email', label: 'email', minWidth: 170 },
    { id: 'phone_number', label: 'Contact', minWidth: 170 },
    { id: 'is_active', label: 'Status', maxWidth: 10 },
    { id: 'action', label: 'Action', minWidth: 170 },
    // { id: 'birth_cert_no', label: 'Birth Cert No', minWidth: 170 },
    // { id: 'date_of_bith', label: 'D.O.B', minWidth: 170 },
    // { id: 'term_admitted', label: 'Term of Admission', minWidth: 170 },

];



function TenantReadScreen({ history }) {
    // TabTitle('Students | Samis Systems')
    TabTitle('Pide Piper')

    const dispatch = useDispatch();

    const [message, setMessage] = useState(null)

    const [openModal, setOpenModal] = useState(false);
    const [openPopup, setOpenPopup] = useState(false);
    const [confirmDialog, setConfirmDialog] = useState({ isOpen: false, title: '', subTitle: '' })

    const [search, setSearch] = useState({ fn: items => { return items; } })
    const [recordForEdit, setRecordForEdit] = useState(null);

    const createTenant = useSelector(state => state.createTenant)
    const { success: successCreate } = createTenant

    const readTenants = useSelector(state => state.readTenants)
    const { loading, error, tenants } = readTenants

    const updateTenant = useSelector(state => state.updateTenant)
    const { success: successUpdate } = updateTenant

    const deleteTenant = useSelector(state => state.deleteTenant)
    const { success: successDelete } = deleteTenant



    const signInUser = useSelector(state => state.signInUser)
    const { userInfo } = signInUser

    const { TblContainer,
        TblHead,
        TblPagination,
        recordsAfterPaginatingAndSorting,
    } = TableComponent(tenants, headCells, search)


    const handleSearch = (e) => {
        e.preventDefault()
        let target = e.target
        setSearch({
            fn: items => {
                if (target.value === "")
                    return items;
                else
                    return items.filter(x => x.full_name.includes(toTitleCase(e.target.value)))
            }
        })
    }

    const newEntry = (tenant, handleResetForm) => {
        dispatch(createTenantAction(tenant))
        if (successCreate) {
            handleResetForm()
            setOpenModal(false);
        }
    }

    const editEntry = (tenant, handleResetForm) => {
        dispatch(updateTenantAction(tenant))
        // setOpenPopup(false);
        // handleResetForm()   
    }

    const editHandler = (tenant) => {
        setRecordForEdit(tenant)
        setOpenPopup(true)
    }

    const deleteHandler = (id) => {
        setConfirmDialog({
            ...confirmDialog,
            isOpen: false
        })
        dispatch(deleteTenantAction(id))
    }

    useEffect(() => {
        dispatch(readTenantsAction())
        if (successCreate) {
            setMessage("Tenant Added Successfully")
        }
    }, [dispatch, successCreate, successUpdate, successDelete, userInfo, history])

    return (
        <NgPageContainer>
            <NgPaper>
                {successCreate && <ToastAlert severity="success">{message}</ToastAlert>}
                <TableTop>
                    <Controls.SearchInputField
                        label="Search Tenants"
                        onChange={handleSearch}
                    />
                    <Controls.AddButton
                        onClick={() => setOpenModal(true)}
                    >
                    </Controls.AddButton>
                </TableTop>


                {loading
                    ? <Loading />
                    : error
                        ? <ToastAlert severity="error">{error}</ToastAlert>
                        : (

                            <div>
                                <TblContainer>
                                    <TblHead />

                                    <TableBody>
                                        {
                                            recordsAfterPaginatingAndSorting() && recordsAfterPaginatingAndSorting().map(tenant =>
                                            (
                                                <TableRow key={tenant.id}>
                                                    <TableCell>
                                                        <NgLink to={`/tenant/${tenant.slug}`}>
                                                            {tenant.full_name}
                                                        </NgLink>
                                                    </TableCell>
                                                    <TableCell>{tenant.email}</TableCell>
                                                    <TableCell>{tenant.phone_number}</TableCell>
                                                    <TableCell>{tenant.is_active == true ? <Chip active>active</Chip> : <Chip>inactive</Chip>}</TableCell>
                                                    <TableCell>
                                                        <ActionButtonWrapper>
                                                            <Controls.ActionButton
                                                                title="edit"
                                                                onClick={() => editHandler(tenant)}
                                                                edit>
                                                                <FiEdit3 />
                                                            </Controls.ActionButton>
                                                            <Controls.ActionButton
                                                                title="deactivate"
                                                                onClick={() => {
                                                                    setConfirmDialog({
                                                                        isOpen: true,
                                                                        title: "Are you sure you want to delete this Tenant?",
                                                                        subTitle: "You can't undo this operation",
                                                                        onConfirm: () => { deleteHandler(tenant.slug) }
                                                                    })
                                                                }}>
                                                                <FiDelete />
                                                            </Controls.ActionButton>
                                                        </ActionButtonWrapper>
                                                    </TableCell>
                                                </TableRow>
                                            )
                                            )}
                                    </TableBody>
                                </TblContainer>

                                <TblPagination />
                            </div>

                        )}
            </NgPaper>

            <Modal
                openModal={openModal}
                setOpenModal={setOpenModal}
                title="Create Tenant"
            >
                <TenantForm
                    newEntry={newEntry}
                />
            </Modal>

            <Modal
                openPopup={openPopup}
                setOpenPopup={setOpenPopup}
                title="Edit Tenant"
            >
                <TenantEditForm
                    recordForEdit={recordForEdit}
                    editEntry={editEntry}
                />
            </Modal>

            <ConfirmDialog
                confirmDialog={confirmDialog}
                setConfirmDialog={setConfirmDialog}
            />
        </NgPageContainer>

    )
}


export default TenantReadScreen



