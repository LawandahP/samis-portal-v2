import axios from 'axios';
import { backend } from '../../proxy';
import {
    CONTACT_INFO_READ_REQUEST,
    CONTACT_INFO_READ_SUCCESS,
    CONTACT_INFO_READ_FAIL,

    CONTACT_INFO_CREATE_REQUEST,
    CONTACT_INFO_CREATE_SUCCESS,
    CONTACT_INFO_CREATE_FAIL,
    CONTACT_INFO_CREATE_RESET,

    CONTACT_INFO_DETAILS_REQUEST,
    CONTACT_INFO_DETAILS_SUCCESS,
    CONTACT_INFO_DETAILS_FAIL,

    CONTACT_INFO_UPDATE_REQUEST,
    CONTACT_INFO_UPDATE_SUCCESS,
    CONTACT_INFO_UPDATE_FAIL,

    CONTACT_INFO_DELETE_REQUEST,
    CONTACT_INFO_DELETE_SUCCESS,
    CONTACT_INFO_DELETE_FAIL,



} from './constants';


export const readContactInfo = () => async (dispatch) => {
    try {
        dispatch({
            type: CONTACT_INFO_READ_REQUEST
        })
        const { data } = await axios.get(`${backend}/contact-info/v1`)
        dispatch({
            type: CONTACT_INFO_READ_SUCCESS,
            payload: data
        })

    } catch(error) {
        dispatch({
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.message
        })

    }
}
// error.response.data.message



export const createContactInfo = (testimonial) => async (dispatch, getState) => {
    try {
        dispatch({
            type: CONTACT_INFO_CREATE_REQUEST
        })

        const { data } = await axios.post(
            `${backend}/contact-info/v1`,
            testimonial,
        )

        dispatch({
            type: CONTACT_INFO_CREATE_SUCCESS,
            success: true,
            payload: data
        })

    } catch(error) {
        dispatch({
            type: CONTACT_INFO_CREATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


export const updateContactInfo = (testimonial) => async (dispatch) => {
    try {
        dispatch({
            type: CONTACT_INFO_UPDATE_REQUEST
        })

        const { data } = await axios.put(
            `${backend}/contact-info/v1/`,
            testimonial,
        )

        dispatch({
            type: CONTACT_INFO_UPDATE_SUCCESS,
            payload: data
        })

        //update details
        dispatch({
            type: CONTACT_INFO_DETAILS_SUCCESS,
            payload: data
        })

    } catch(error) {
        dispatch({
            type: CONTACT_INFO_UPDATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


export const deleteContactInfo = () => async (dispatch, getState) => {
    try {
        dispatch({
            type: CONTACT_INFO_DELETE_REQUEST
        })

        const { data } = await axios.delete(
            `${backend}/contact-info/v1/`,
        )

        dispatch({
            type: CONTACT_INFO_DELETE_SUCCESS,
        })

    } catch(error) {
        dispatch({
            type: CONTACT_INFO_DELETE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


