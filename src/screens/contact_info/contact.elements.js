import styled from 'styled-components'

export const ContactContainer = styled.div`
    align-items: center;
    justify-content: center;
    /* display: flex; */
    /* padding: 20px; */
`


export const ContactWrapper = styled.div`
    background: ${({theme}) => theme.bg};
    border-radius: 5px;
    padding: 10px;
    list-style: none;
    overflow: auto;
`

export const ContactHeader = styled.h4`
    margin-top: 10px;
    
`

export const ContactInfo = styled.p`
    font-size: 14px;

`
