import React, {useEffect, useMemo, useState} from 'react'
import { useDispatch, useSelector } from 'react-redux';

import Grid from '@mui/material/Grid';
// import { createStatus, getStatus } from '../stepper/stepperStatus/statusActions';

import { MainForm, useForm } from '../../components/useForm';
import ToastAlert from '../../components/display/ToastAlert';
import Loader from '../../components/display/Loader';
import { FormButton } from '../../components/useForm/formElements';
import Controls from '../../components/controls/Controls';



function ContactInfoEditForm(props) {
    const { editEntry, recordForEdit, contactInfoId  } = props;
    const dispatch = useDispatch();
    
    const contactInfoUpdate = useSelector(state => state.contactInfoUpdate)
    const { loading, error, success } = contactInfoUpdate

    // const [successMessage, setSuccessMessage] = useState('')
    
    const validate = (fieldValues = values) => {
        let temp = {...errors}
        if('physical_address' in fieldValues)
            temp.physical_address = fieldValues.physical_address ? "" : "Physical Address is Required"
        if('postal_address' in fieldValues)
            temp.postal_address = fieldValues.postal_address ? "" : "Postal Address is Required"
        if('primary_phone_no' in fieldValues)
            temp.primary_phone_no = (/^0([0-9](?:(?:[129][0-9])|(?:0[0-8])|(4[0-1]))[0-9]{6})$/).test(fieldValues.primary_phone_no) ? "" : "Enter a Valid Phone Number"
        if('email' in fieldValues)
            temp.email = fieldValues.email ? "" : "Email is Required"
        // if('latitude' in fieldValues)
        //     temp.location.latitude = fieldValues.location.latitude ? "" : "Latitude is Required"
        // if('longitude' in fieldValues)
        //     temp.location = fieldValues.location.longitude ? "" : "Longitude is Required"
        // if('altitude' in fieldValues)
        //     temp.location = fieldValues.location ? "" : "Altitude is Required"
        // temp.phone_no = (\\+?\\d{9,13}).test(values.phone_no) ? "" : "Student Name is Required"
        setErrors({ ...temp })

        // tests whether post array elements passes text implemented by validate() function
        if (fieldValues === values)
            return Object.values(temp).every(x => x === "")
        
    }
    

    const initialValues = {
        physical_address: '',
        location: {
            latitude: '',
            longitude: '',
            altitude: ''
        },
        postal_address: '',
        country_code: '',
        primary_phone_no: '',
        other_phone_nos: [

        ],
        email: '',
        other_emails: [
        ],
    }

    
    const { values,  
            setValues,
            errors, 
            setErrors, 
            handleResetForm, 
            handleInputChange,

        } = useForm(initialValues, true, validate);


   
    

    const handleLocationChange = e => {
        let location = values.location;
        var key = e.target.name;
        var value = e.target.value;
        location[key] = value;
        setValues({
            ...values,
            location,
            [key]:value
        });
    }
   


    useEffect(() => {
        if(recordForEdit != null)
            setValues({
                ...recordForEdit
            })
    
    }, [dispatch,  recordForEdit, success, loading, error, setValues, contactInfoId ])



    const submitHandler = (e) => {
        e.preventDefault()
        if (validate()) {
            editEntry(values, handleResetForm);
            // handleResetForm()
        }
    }

   
    return (
        
        <>
            { error && <ToastAlert severity="error">{error}</ToastAlert>}
                <MainForm onSubmit={submitHandler}>
                    { loading 
                      ? <Loader /> 
                    : (
                        <Grid container>
                            <Grid item md={6} xs={12}>
                                <Controls.InputField
                                    error={errors.physical_address}
                                    label="Physical Address"
                                    value={values.physical_address}
                                    name='physical_address'
                                    onChange={handleInputChange}
                                />
                            </Grid>

                            <Grid item md={6} xs={12}>
                                <Controls.InputField
                                    error={errors.postal_address}
                                    label="Postal Address"
                                    value={values.postal_address}
                                    name='postal_address'
                                    onChange={handleInputChange}
                                />
                            </Grid>
                        
                        
                            <Grid item md={6} xs={12}>
                                <Controls.InputField
                                    label="Country Code"
                                    value={values.country_code}
                                    name='country_code'
                                    onChange={handleInputChange}
                                />
                            </Grid>

                            <Grid item md={12} xs={12}>
                                <Controls.InputField 
                                    error={errors.primary_phone_no}
                                    label="Phone No. (Primary)"
                                    name='primary_phone_no'
                                    value={values.primary_phone_no}
                                    onChange={handleInputChange}
                                />
                            </Grid>

                            <Grid item md={12} xs={12}>
                                <Controls.InputField 
                                    error={errors.email}
                                    label="Email"
                                    name='email'
                                    value={values.email}
                                    onChange={handleInputChange}
                                />
                            </Grid>
                            
                         

                        <Grid item md={12}>
                            Location
                        </Grid>
                        
                        <Grid item md={4} xs={12}>
                            <Controls.InputField
                                label="Latitude"
                                value={values.location.latitude}
                                name='latitude'
                                onChange={handleLocationChange}
                            />
                        </Grid>

                        <Grid item md={4} xs={12}>
                            <Controls.InputField
                                label="Longitude"
                                value={values.location.longitude}
                                name='longitude'
                                onChange={handleLocationChange}
                            />
                        </Grid>

                        <Grid item md={4} xs={12}>
                            <Controls.InputField
                                label="Altitude"
                                value={values.location.altitude}
                                name='altitude'
                                onChange={handleLocationChange}
                            />
                        </Grid>

                        <FormButton type='submit'>
                            Submit 
                        </FormButton>                        

                    </Grid>
                    )}
                    
                </MainForm>
        </>
        
            
            
                       
    )
}

export default ContactInfoEditForm
