import React, { useEffect, useMemo, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';

import Grid from '@mui/material/Grid';

import { MainForm, useForm } from '../../components/useForm';
import ToastAlert from '../../components/display/ToastAlert';
import Loader from '../../components/display/Loader';
import { FormButton, FormButtonWrapper } from '../../components/useForm/formElements';
import Controls from '../../components/controls/Controls';

import { useLocation } from 'react-router-dom';
import { createStatus, readStatus } from '../stepper/status/actions';
import { processPathName } from '../../utils/globalFunc';



function ContactInfoForm(props) {
    const { handleContactInfoCreate, handleNext } = props;

    const dispatch = useDispatch();
    const location = useLocation();

    const contactInfoCreate = useSelector(state => state.contactInfoCreate)
    const { loading: loading, error: error, success: successCreateContactInfo } = contactInfoCreate

    // const [successMessage, setSuccessMessage] = useState('')

    const validate = (fieldValues = values) => {
        let temp = { ...errors }
        if ('physical_address' in fieldValues)
            temp.physical_address = fieldValues.physical_address ? "" : "Physical Address is Required"
        if ('postal_address' in fieldValues)
            temp.postal_address = fieldValues.postal_address ? "" : "Postal Address is Required"
        if ('primary_phone_no' in fieldValues)
            temp.primary_phone_no = (/^0([0-9](?:(?:[129][0-9])|(?:0[0-8])|(4[0-1]))[0-9]{6})$/).test(fieldValues.primary_phone_no) ? "" : "Enter a Valid Phone Number"
        if ('email' in fieldValues)
            temp.email = fieldValues.email ? "" : "Email is Required"
        // if('latitude' in fieldValues)
        //     temp.location.latitude = fieldValues.location.latitude ? "" : "Latitude is Required"
        // if('longitude' in fieldValues)
        //     temp.location = fieldValues.location.longitude ? "" : "Longitude is Required"
        // if('altitude' in fieldValues)
        //     temp.location = fieldValues.location ? "" : "Altitude is Required"
        // temp.phone_no = (\\+?\\d{9,13}).test(values.phone_no) ? "" : "Student Name is Required"
        setErrors({ ...temp })

        // tests whether post array elements passes text implemented by validate() function
        if (fieldValues === values)
            return Object.values(temp).every(x => x === "")

    }


    const initialFValues = {
        physical_address: '',
        location: {
            latitude: '',
            longitude: '',
            altitude: ''
        },
        postal_address: '',
        country_code: '',
        primary_phone_no: '',
        other_phone_nos: [

        ],
        email: '',
        other_emails: [
        ],
    }


    const { values,
        setValues,
        errors,
        setErrors,
        handleResetForm,
        handleInputChange,

    } = useForm(initialFValues, true, validate);





    const handleLocationChange = e => {
        let location = values.location;
        var key = e.target.name;
        var value = e.target.value;
        location[key] = value;
        setValues({
            ...values,
            location,
            [key]: value
        });
    }

    const [emails, setEmails] = useState(values.other_emails)
    const [phoneNumber, setPhoneNumber] = useState(values.other)
    const [counter, setCounter] = React.useState(0);

    const addField = () => {
        if (counter < 4) {
            setEmails(prevIndexes => [...prevIndexes, counter])
            setCounter(prevCounter => prevCounter + 1)
        } else {
            console.log("max 4 emails")
        }
    }

    const removeField = index => () => {
        setEmails(prevIndexes => [...prevIndexes.filter(item => item !== index)]);
        setCounter(prevCounter => prevCounter - 1);
    };

    const clearEmails = () => {
        setEmails([]);
        setCounter(0);
    };

    // const handleListChange = e => {
    //     let other_emails = values.other_emails;
    //     var key = e.target.name;
    //     var value = e.target.value;
    //     other_emails[key] = value;
    //         setValues( prevState => ({
    //             prevState,
    //             ...values,
    //             other_emails: [...prevState.other_emails, value],                    
    // }))} 

    const handleListChange = e => {
        // let other_emails = values.other_emails
        // other_emails[e.target.dataset.id][e.target.name] = e.target.value
        let other_emails = values.other_emails;
        var key = e.target.name;
        var value = e.target.value;
        other_emails[key] = value;
        setValues({
            ...values,
            other_emails,
            [key]: value
        });
    }
    console.log(values.other_emails[2])






    const statusRead = useSelector(state => state.statusRead)
    const {getStep} = statusRead
    let currentStep = getStep.step

    const detectStepChange = useMemo(() => {
        return stepChange(currentStep)
    }, [stepChange,])


    function stepChange(currentStep) {
        if(location.pathname === processPathName  && currentStep > 4)
            handleNext()
    }

    useEffect(() => {
        dispatch(readStatus())
        if (successCreateContactInfo) {
            dispatch(createStatus(5))
            console.log("success")
        }

    }, [successCreateContactInfo])



    const submitHandler = (e) => {
        e.preventDefault()
        if (validate()) {
            handleContactInfoCreate(values, handleResetForm);
            // handleResetForm()
        }
    }


    return (

        <>
            {error && <ToastAlert severity="error">{error}</ToastAlert>}
            <MainForm onSubmit={submitHandler}>
                <Grid container>
                    <Grid item md={6} xs={12}>
                        <Controls.InputField
                            error={errors.physical_address}
                            label="Physical Address"
                            value={values.physical_address}
                            name='physical_address'
                            onChange={handleInputChange}
                        />
                    </Grid>

                    <Grid item md={6} xs={12}>
                        <Controls.InputField
                            error={errors.postal_address}
                            label="Postal Address"
                            value={values.postal_address}
                            name='postal_address'
                            onChange={handleInputChange}
                        />
                    </Grid>


                    <Grid item md={6} xs={12}>
                        <Controls.InputField
                            label="Country Code"
                            value={values.country_code}
                            name='country_code'
                            onChange={handleInputChange}
                        />
                    </Grid>

                    <Grid item md={12} xs={12}>
                        <Controls.InputField
                            error={errors.primary_phone_no}
                            label="Phone No. (Primary)"
                            name='primary_phone_no'
                            value={values.primary_phone_no}
                            onChange={handleInputChange}
                            InputProps={{
                                endAdornment:
                                    <>
                                        <Controls.ActionButton
                                            title="Add Phone Number"
                                            color="primary"
                                            onClick={addField}>
                                            {/* <Add />          */}
                                        </Controls.ActionButton>

                                        <Controls.ActionButton
                                            title="Clear All"
                                            color="secondary"
                                            onClick={clearEmails}>
                                            {/* <ClearAllIcon />          */}
                                        </Controls.ActionButton>
                                    </>
                            }}
                        />
                    </Grid>



                    {/* <Grid item md={12} sx={12}>
                        { emails.map(index => {
                        const fieldName = `other_emails[${index}]`;
                        return (
                            <Grid item md={12} xs={12}>
                                <Controls.Input
                                    type="number"
                                    // label="Secondary Phone Number"   
                                    name={fieldName}
                                    value={values.other_emails[index]}
                                    onChange={handleListChange}
                                    InputProps={{
                                        endAdornment: 
                                        <Controls.ActionButton
                                            color="secondary"
                                            title="Remove Phone Number"
                                            onClick={removeField(index)}>
                                            <ClearIcon />
                                        </Controls.ActionButton>
                                    }}
                                />
                            </Grid>
                        )})}
                    </Grid> */}

                    {/* <Grid item md={12} xs={12}>
                        <Controls.Input 
                            error={errors.email}
                            label="Email (Primary)"
                            name='email'
                            value={values.email}
                            onChange={handleInputChange}
                            InputProps={{
                                endAdornment: 
                                <>
                                <Controls.ActionButton
                                    title="Add Email"
                                    color="primary"
                                    onClick={addField}>
                                    <Add />         
                                </Controls.ActionButton>

                                <Controls.ActionButton
                                    title="Clear All"
                                    color="secondary"
                                    onClick={clearEmails}>
                                    <ClearAllIcon />         
                                </Controls.ActionButton>
                                </>
                            }}
                        />
                    </Grid> */}



                    <Grid item md={12} sx={12}>
                        {emails.map(index => {
                            const fieldName = `other_emails[${index}]`;
                            return (
                                <Grid item md={12} xs={12}>
                                    {/* <Controls.InputField
                                    type="text"
                                    label="Seondary Email"
                                    name={fieldName}
                                    value={values.other_emails[index]}
                                    onChange={handleListChange}
                                    InputProps={{
                                        endAdornment: 
                                        <Controls.ActionButton
                                            color="secondary"
                                            title="Remove Email"
                                            onClick={removeField(index)}>
                                            <ClearIcon />
                                        </Controls.ActionButton>
                                    }}
                                /> */}
                                </Grid>
                            )
                        })}
                    </Grid>



                    <Grid item md={12}>
                        Location
                    </Grid>

                    <Grid item md={4} xs={12}>
                        <Controls.InputField
                            label="Latitude"
                            value={values.location.latitude}
                            name='latitude'
                            onChange={handleLocationChange}
                        />
                    </Grid>

                    <Grid item md={4} xs={12}>
                        <Controls.InputField
                            label="Longitude"
                            value={values.location.longitude}
                            name='longitude'
                            onChange={handleLocationChange}
                        />
                    </Grid>

                    <Grid item md={4} xs={12}>
                        <Controls.InputField
                            label="Altitude"
                            value={values.location.altitude}
                            name='altitude'
                            onChange={handleLocationChange}
                        />
                    </Grid>
                </Grid>

                {loading ? <Loader />
                    :   <FormButtonWrapper>
                            <FormButton type='submit'>
                                Submit
                            </FormButton>
                        </FormButtonWrapper>
                }

            </MainForm>
        </>




    )
}

export default ContactInfoForm
