import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { createContactInfo, deleteContactInfo, readContactInfo, updateContactInfo } from './actions'

import { NgPageContainer, NgPaper } from '../../components/display/elements';
import { FiDelete, FiEdit3 } from 'react-icons/fi';
import Loader from '../../components/display/Loader';
import { TabTitle, toTitleCase } from '../../utils/globalFunc'
import Controls from '../../components/controls/Controls';
import Modal from '../../components/display/modal';
import TableComponent from '../../components/useTable';
import { TableTop } from '../../components/useTable/elements';
import ToastAlert from '../../components/display/ToastAlert';
import { ActionButtonWrapper } from '../../components/controls/Button';
import ContactInfoEditForm from './editform';
import ContactInfoForm from './form';
import ConfirmDialog from '../../components/display/dialog';
import { ContactContainer, ContactHeader, ContactInfo, ContactWrapper } from './contact.elements';


const headCells = [
    { id: 'physical_address', label: 'Physical Address', minWidth: 170 },
    { id: 'location', label: 'Location', minWidth: 200 },
    { id: 'postal_address', label: 'Postal Address', minWidth: 170 },
    { id: 'country_code', label: 'Country Code',  minWidth: 170  },
    { id: 'primary_phone_no', label: 'Phone No.',  minWidth: 170  },
    { id: 'other_phone_nos', label: 'Other Phone Nos',  minWidth: 170  },
    { id: 'email', label: 'Email',  minWidth: 170  },
    { id: 'other_emails', label: 'Other Emails',  minWidth: 170  },
    { id: 'actions', label: 'Actions', disableSorting: true },

]



function ContactInfoReadScreen() {

    TabTitle('Contact - Samis Systems')
    const dispatch = useDispatch();
    const [ recordForEdit, setRecordForEdit ] = useState(null);

    const [ openPopup, setOpenPopup ] = useState(false)
    const [ openModal, setOpenModal ] = useState(false)

    const [ message, setMessage ] = useState('')

    const contactInfoRead = useSelector(state => state.contactInfoRead)
    const { loading, error, contactInfo } = contactInfoRead

    const contactInfoCreate = useSelector(state => state.contactInfoCreate)
    const { success: successCreate } = contactInfoCreate
    const contactInfoUpdate = useSelector(state => state.contactInfoUpdate)
    const { success: successUpdate } = contactInfoUpdate
    const contactInfoDelete = useSelector(state => state.contactInfoDelete)
    const { success: successDelete } = contactInfoDelete

    const [ confirmDialog, setConfirmDialog ] = useState({isOpen: false, title: '', subTitle: ''})
    
    useEffect(() => {
        dispatch(readContactInfo())
        if(successUpdate){
            setMessage("Successfully Updated")
        }
    }, [dispatch, successCreate, successUpdate, successDelete])


    const handleContactInfoCreate = (testimonial, handleResetForm) => {
        dispatch(createContactInfo(testimonial))
        if(successCreate)
            handleResetForm()
            setOpenModal(false);
    }

    const editEntry = (testimonial, handleResetForm) => {
        dispatch(updateContactInfo(testimonial))
        setOpenPopup(false);
        // handleResetForm()   
    }

    const editHandler = (contactInfo) => { 
        setRecordForEdit(contactInfo)
        setOpenPopup(true)
    }

    const deleteHandler = () => {
        setConfirmDialog({
            ...confirmDialog,
            isOpen: false
        })
        dispatch(deleteContactInfo())  
    }


    return (
            <>    
                { message && <ToastAlert severity="success">{message}</ToastAlert>}
                <ContactContainer>
                    
                    <TableTop>
                        <p></p>
                        <Controls.AddButton
                            onClick={() => setOpenModal(true)}
                        >
                        </Controls.AddButton>
                    </TableTop>

                    <ContactWrapper>
                        {error && <ToastAlert severity="error">{error}</ToastAlert>}

                        { loading ? <Loader /> :
                            <>
                                <ContactHeader>Physical Address</ContactHeader> 
                                <ContactInfo>{contactInfo?.physical_address}</ContactInfo>

                                <ContactHeader>Location</ContactHeader> 
                                { contactInfo?.location ? 
                                    Object.entries(contactInfo.location).map(([key, value]) => (
                                        <li>
                                            <ContactInfo disablePadding>
                                                {key}: {value ? value : "not set"}
                                            </ContactInfo>
                                        </li>
                                )): "" }

                                <ContactHeader>Postal Address</ContactHeader>
                                <ContactInfo>{contactInfo?.postal_address}</ContactInfo>

                                <ContactHeader>Country Code</ContactHeader>
                                <ContactInfo>{contactInfo?.country_code}</ContactInfo>

                                <ContactHeader>Phone Number</ContactHeader>
                                <ContactInfo>{contactInfo?.primary_phone_no}</ContactInfo>

                                <ContactHeader>Email</ContactHeader>
                                <ContactInfo>{contactInfo?.email}</ContactInfo>
                                
                                { contactInfo?.other_emails?.length > 0 ? <ContactHeader>Other Emails</ContactHeader> : "" }
                                { contactInfo?.other_emails?.map(other_emails => (
                                    <li key={other_emails}>
                                        <ContactInfo disablePadding>
                                            {other_emails}
                                        </ContactInfo>
                                    </li>
                                ))}

                                <ActionButtonWrapper>
                                    <Controls.ActionButton
                                        title="edit"
                                        onClick={() => editHandler(contactInfo)}
                                        edit>
                                        <FiEdit3 />
                                    </Controls.ActionButton>
                                    <Controls.ActionButton
                                        title="deactivate"
                                        onClick={() => {
                                            setConfirmDialog({
                                                isOpen: true,
                                                title: "Are you sure you want to delete this Contact?",
                                                subTitle: "You can't undo this operation",
                                                onConfirm: () => { deleteHandler() }
                                            })
                                        }}>
                                        <FiDelete />
                                    </Controls.ActionButton>
                                </ActionButtonWrapper>
                            </>
                        }
                    </ContactWrapper>
                    
                </ContactContainer>

                
                <Modal
                    openModal = {openModal}
                    setOpenModal = {setOpenModal}
                    title="Create Contact"
                >
                    <ContactInfoForm 
                        recordForEdit = {recordForEdit}
                        handleContactInfoCreate = {handleContactInfoCreate}/>  
                </Modal>  

                <Modal
                    openPopup = {openPopup}
                    setOpenPopup = {setOpenPopup}
                    title="Edit Contact"
                >
                    <ContactInfoEditForm
                        recordForEdit = {recordForEdit}
                        editEntry = {editEntry}
                    />
                </Modal> 

                <ConfirmDialog 
                    confirmDialog={confirmDialog}
                    setConfirmDialog={setConfirmDialog} />
                
            </>
        
    )
}

export default ContactInfoReadScreen




// { loading
//     ? "loading...."
//     : error
//     ? <ToastAlert severity="error">{error}</ToastAlert>
//     : contactInfo ? (
        
//         <Paper className={classes.paper}>
            
//             <TblContainer>
//                 <TblHead />

//                 <TableBody> 
                                        
//                     <TableRow>
                    
//                         <TableCell>{contactInfo.physical_address ? contactInfo.physical_address : "-"}</TableCell>
//                         <TableCell>
//                             <ul>
//                                 { contactInfo.location ? 
//                                     Object.entries(contactInfo.location).map(([key, value]) => (
//                                         <List>
//                                             <ListItem disablePadding>
//                                                 {key}: {value ? value : "not set"}
//                                             </ListItem>
//                                         </List>
//                                 ))
//                             : "-"}
//                             </ul>
//                         </TableCell>
//                         <TableCell>{contactInfo.postal_address ? contactInfo.postal_address : "-"}</TableCell>
//                         <TableCell>{contactInfo.country_code}</TableCell>
//                         <TableCell>{contactInfo.primary_phone_no}</TableCell>
//                         <TableCell>
//                             <ul>
//                                  
//                             </ul>
//                         </TableCell>
//                         <TableCell>{contactInfo.email}</TableCell>
//                         <TableCell>
//                             <ul>
//                                 { contactInfo.other_emails && contactInfo.other_emails.map(other_emails => (
//                                     <List>
//                                         <ListItem disablePadding>
//                                             {other_emails}
//                                         </ListItem>
//                                     </List>
//                                 ))}
//                             </ul>
//                         </TableCell>
//                         <TableCell>
                            // <ActionButtonWrapper>
                            //     <Controls.ActionButton
                            //         title="edit"
                            //         onClick={() => editHandler(contactInfo)}
                            //         edit>
                            //         <FiEdit3 />
                            //     </Controls.ActionButton>
                            //     <Controls.ActionButton
                            //         title="deactivate"
                            //         onClick={() => {
                            //             setConfirmDialog({
                            //                 isOpen: true,
                            //                 title: "Are you sure you want to delete this Contact?",
                            //                 subTitle: "You can't undo this operation",
                            //                 onConfirm: () => { deleteHandler() }
                            //             })
                            //         }}>
                            //         <FiDelete />
                            //     </Controls.ActionButton>
                            // </ActionButtonWrapper>
                            
//                         </TableCell>
                                                
//                     </TableRow>
                
//                 </TableBody>
//             </TblContainer>
//         </Paper>
//     ) : <Typography sx={{paddingTop: "7rem", color: "red"}}>Hey Therer</Typography>}