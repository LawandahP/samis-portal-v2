import axios from 'axios';
import { backend } from '../../proxy';
import {
    ACADEMIC_SUBJECT_READ_REQUEST,
    ACADEMIC_SUBJECT_READ_SUCCESS,
    ACADEMIC_SUBJECT_READ_FAIL,

    ACADEMIC_SUBJECT_CREATE_REQUEST,
    ACADEMIC_SUBJECT_CREATE_SUCCESS,
    ACADEMIC_SUBJECT_CREATE_FAIL,

    ACADEMIC_SUBJECT_DETAILS_REQUEST,
    ACADEMIC_SUBJECT_DETAILS_SUCCESS,
    ACADEMIC_SUBJECT_DETAILS_FAIL,

    ACADEMIC_SUBJECT_UPDATE_REQUEST,
    ACADEMIC_SUBJECT_UPDATE_SUCCESS,
    ACADEMIC_SUBJECT_UPDATE_FAIL,

    ACADEMIC_SUBJECT_DELETE_REQUEST,
    ACADEMIC_SUBJECT_DELETE_SUCCESS,
    ACADEMIC_SUBJECT_DELETE_FAIL,

} from './constants';



export const readAcademicSubjects = (page, size) => async (dispatch) => {
    try {
        dispatch({ type: ACADEMIC_SUBJECT_READ_REQUEST })
        const { data } = await axios.get(`${backend}/school/academic_subjects/v1?page=${page}&size=${size}`)

        dispatch({
            type:ACADEMIC_SUBJECT_READ_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type:ACADEMIC_SUBJECT_READ_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })
    }
}



export const createAcademicSubject = (academicSubject) => async (dispatch, getState) => {
    try {
        dispatch({
            type: ACADEMIC_SUBJECT_CREATE_REQUEST
        })

        const { data } = await axios.post(
            `${backend}/school/academic_subjects/v1`,
            academicSubject,
        )

        dispatch({
            type: ACADEMIC_SUBJECT_CREATE_SUCCESS,
            success: true,
            payload: data
        })

    } catch(error) {
        dispatch({
            type: ACADEMIC_SUBJECT_CREATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


export const academicSubjectDetails = (id) => async (dispatch) => {
    try {
        dispatch({ type: ACADEMIC_SUBJECT_DETAILS_REQUEST })
        const { data } = await axios.get(`${backend}/school/academic_subjects/${id}`) //backend in package.json "http://127.0.0.1:8000/"

        dispatch({
            type:ACADEMIC_SUBJECT_DETAILS_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type:ACADEMIC_SUBJECT_DETAILS_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })
    }
}


export const updateAcademicSubject = (academicSubject) => async (dispatch) => {
    try {
        dispatch({
            type: ACADEMIC_SUBJECT_UPDATE_REQUEST
        })

        const { data } = await axios.put(
            `${backend}/school/academic_subjects/v1/${academicSubject.id}`,
            academicSubject,
        )

        dispatch({
            type: ACADEMIC_SUBJECT_UPDATE_SUCCESS,
            payload: data
        })

        //update details
        dispatch({
            type: ACADEMIC_SUBJECT_DETAILS_SUCCESS,
            payload: data
        })

    } catch(error) {
        dispatch({
            type: ACADEMIC_SUBJECT_UPDATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


export const deleteAcademicSubject = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: ACADEMIC_SUBJECT_DELETE_REQUEST
        })

        const { data } = await axios.delete(
            `${backend}/school/academic_subjects/v1/${id}`,
            //config
        )

        dispatch({
            type: ACADEMIC_SUBJECT_DELETE_SUCCESS,
        })

    } catch(error) {
        dispatch({
            type: ACADEMIC_SUBJECT_DELETE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}
