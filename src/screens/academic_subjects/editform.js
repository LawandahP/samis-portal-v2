import React, {useEffect} from 'react'
import { useDispatch, useSelector } from 'react-redux';

import { Grid } from '@mui/material';

import ToastAlert from '../../components/display/ToastAlert';
import Loader from '../../components/display/Loader';
import { MainForm, useForm } from '../../components/useForm';
import Controls from '../../components/controls/Controls';
import { FormButton } from '../../components/useForm/formElements';


function AcademicSubjectEditForm(props) {
    const { editEntry, recordForEdit, academicSubjectId } = props;
    const dispatch = useDispatch();

    const academicSubjectUpdate = useSelector(state => state.academicSubjectUpdate)
    const { loading: loading, error: error, success: successUpdate } = academicSubjectUpdate

    // const [successMessage, setSuccessMessage] = useState('')
    
    const validate = (fieldValues = values) => {
        let temp = {...errors}
        if('name' in fieldValues)
            temp.name = fieldValues.name ? "" : "Subject Name is Required"
        if('alias' in fieldValues)
            temp.alias = fieldValues.alias ? "" : "Alias is Required"
        if('category' in fieldValues)
            temp.category = fieldValues.category ? "" : "Category is Required"
        if('code' in fieldValues)
            temp.code = fieldValues.code ? "" : "Subject code is Required"
        // temp.phone_no = (\\+?\\d{9,13}).test(values.phone_no) ? "" : "Student Name is Required"
        setErrors({ ...temp })

        // tests whether post array elements passes text implemented by validate() function
        if (fieldValues === values)
            return Object.values(temp).every(x => x === "")
        
    }

    const initialFValues = {
        name: '',
        alias: '',
        category: '',
        code: '',
    }

    const { values,  
            setValues,
            errors, 
            setErrors, 
            handleResetForm, 
            handleInputChange } = useForm(initialFValues, true, validate);

    
    
    useEffect(() => {
        if(recordForEdit != null)
            setValues({
                ...recordForEdit
            })
    
    }, [dispatch, recordForEdit, successUpdate, loading, error, setValues, academicSubjectId ])


    const submitHandler = (e) => {
        e.preventDefault()
        if (validate()) {
            editEntry(values, handleResetForm);
            handleResetForm()
        }
    }

   
    return (
        
        <>
            { error && <ToastAlert severity="error">{error}</ToastAlert>}
            { loading ? <Loader /> : 
                <MainForm onSubmit={submitHandler}>
                    <Grid container>
                        <Grid item md={6} xs={12}>
                            <Controls.InputField
                                error={errors.name}
                                label="Name"
                                value={values.name}
                                name='name'
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <Grid item md={6} xs={12}>
                            <Controls.InputField 
                                error={errors.alias}
                                label="Alias"
                                name='alias'
                                value={values.alias}
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <Grid item md={6} xs={12}>
                            <Controls.InputField 
                                error={errors.category}
                                label="Category"
                                name='category'
                                value={values.category}
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <Grid item md={6} xs={12}>
                            <Controls.InputField 
                                type="number"
                                error={errors.code}
                                label="Code"
                                name='code'
                                value={values.code}
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <FormButton type='submit'>
                            Submit 
                        </FormButton>
                        

                    </Grid>
                </MainForm>
            }
        </>
        
            
            
                       
    )
}

export default AcademicSubjectEditForm
