import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import AcademicSubjectForm from './form'
import { createAcademicSubject, deleteAcademicSubject, 
         readAcademicSubjects, updateAcademicSubject 
} from './actions'

import { makeStyles } from '@mui/styles'

import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';

import TableRow from '@mui/material/TableRow';

import AcademicSubjectEditForm from './editform'
import { ACADEMIC_SUBJECT_CREATE_REQUEST } from './constants'
import { TabTitle, toTitleCase } from '../../utils/globalFunc'
import Modal from '../../components/display/modal'
import { NgPageContainer, NgPaper } from '../../components/display/elements'
import Controls from '../../components/controls/Controls'
import { TableTop } from '../../components/useTable/elements'
import { ActionButtonWrapper } from '../../components/controls/Button'
import ToastAlert from '../../components/display/ToastAlert'
import { FiDelete, FiEdit3 } from 'react-icons/fi'
import Loader from '../../components/display/Loader'
import TableComponent from '../../components/useTable'
import ConfirmDialog from '../../components/display/dialog'


const headCells = [
    { id: 'name', label: 'Name', minWidth: 100 },
    { id: 'alias', label: 'Alias', minWidth: 70 },
    { id: 'category', label: 'Category', minWidth: 100 },
    { id: 'code', label: 'Code', minWidth: 70 },
    { id: 'actions', label: 'Actions', disableSorting: true },
]

function AcademicSubjectReadScreen() {
    TabTitle('Academic Subjects - Samis Systems')

    const dispatch = useDispatch();
    
    const [ search, setSearch ] = useState({fn:items => {return items;}})
    const [ recordForEdit, setRecordForEdit ] = useState(null);

    const [ openPopup, setOpenPopup ] = useState(false);
    const [ openModal, setOpenModal ] = useState(false)
    const [ message, setMessage ] = useState('')

    const academicSubjectRead = useSelector(state => state.academicSubjectRead)
    const { loading, error, academicSubjects } = academicSubjectRead

    const academicSubjectCreate = useSelector(state => state.academicSubjectCreate)
    const { loading: loadingCreate, success: successCreate } = academicSubjectCreate

    const academicSubjectUpdate = useSelector(state => state.academicSubjectUpdate)
    const { success: successUpdate } = academicSubjectUpdate

    const academicSubjectDelete = useSelector(state => state.academicSubjectDelete)
    const { success: successDelete } = academicSubjectDelete

    const [ confirmDialog, setConfirmDialog ] = useState({isOpen: false, title: '', subTitle: ''})

    const { TblContainer, 
            TblHead, 
            TblPagination, 
            recordsAfterPaginatingAndSorting,
            page, rowsPerPage
        } = TableComponent(academicSubjects, headCells, search)


    useEffect(() => {
        dispatch({type: ACADEMIC_SUBJECT_CREATE_REQUEST})
        dispatch(readAcademicSubjects(page, rowsPerPage))
        if(successCreate) {
            setMessage('Academic Subject Created Successfully')
        } else {
            if(successUpdate) {
                setMessage('Academic Subject Updated Successfully')
            }
        }
        if(successDelete) {
            setMessage('Academic Subject Deleted Successfully')
        }
    }, [dispatch, successCreate, successUpdate, successDelete, page, rowsPerPage])



    const handleSearch = (e) => {
        e.preventDefault()
        let target = e.target
        setSearch({
            fn:items => {
                if(target.value === "")
                    return items;
                else
                    return items.filter(x => x.name.includes(toTitleCase(target.value)))
            }
        })
    }


    const newEntry = (academicSubject, handleResetForm) => {
        dispatch(createAcademicSubject(academicSubject))
        setOpenModal(false)
        handleResetForm()
    }

    const editEntry = (academicSubject, handleResetForm) => {
        dispatch(updateAcademicSubject(academicSubject))
        setOpenPopup(false)
        handleResetForm()
    }

    const editHandler = (academicSubject) => { 
        setRecordForEdit(academicSubject)
        setOpenPopup(true)
    }

    const deleteHandler = (id) => {
        setConfirmDialog({
            ...confirmDialog,
            isOpen: false
        })
        dispatch(deleteAcademicSubject(id))  
        dispatch(readAcademicSubjects())
    }

    return (
        <NgPageContainer>  
            <NgPaper>

                <TableTop>
                    <Controls.SearchInputField
                        label="Search Subjects"
                        onChange={handleSearch}
                    />
                    <Controls.AddButton
                        onClick={() => setOpenModal(true)}
                    >
                    </Controls.AddButton>
                </TableTop>

                { loading
                ? <Loader />
                : error
                ? <ToastAlert severity="error">{error}</ToastAlert>
                : (
                    <>
                        <TblContainer>
                            <TblHead />

                            <TableBody>
                            {
                                recordsAfterPaginatingAndSorting()?.map(academicSubject => 
                                    (<TableRow key={academicSubject.id}>
                                        <TableCell>{academicSubject.name}</TableCell>
                                        <TableCell>{academicSubject.alias}</TableCell>
                                        <TableCell>{academicSubject.category}</TableCell>
                                        <TableCell>{academicSubject.code}</TableCell>
                                        <TableCell>
                                            <ActionButtonWrapper>
                                                <Controls.ActionButton
                                                    title="edit"
                                                    onClick={() => editHandler(academicSubject)}
                                                    edit>
                                                    <FiEdit3 />
                                                </Controls.ActionButton>
                                                <Controls.ActionButton
                                                    title="deactivate"
                                                    onClick={() => {
                                                        setConfirmDialog({
                                                            isOpen: true,
                                                            title: "Are you sure you want to delete this Subject?",
                                                            subTitle: "You can't undo this operation",
                                                            onConfirm: () => { deleteHandler(academicSubject.id) }
                                                        })
                                                    }}>
                                                    <FiDelete />
                                                </Controls.ActionButton>
                                            </ActionButtonWrapper>
                                        </TableCell>
                                                                    
                                    </TableRow>)
                            )}
                            </TableBody>
                        </TblContainer>
                        <TblPagination />
                    </>
                )}
            </NgPaper>                 
            
            <Modal
                openModal = {openModal}
                setOpenModal = {setOpenModal}
                title="Create Academic Subject"
            >
                <AcademicSubjectForm 
                    recordForEdit = {recordForEdit}
                    newEntry = {newEntry}/>                
            </Modal>  

            <Modal
                openPopup = {openPopup}
                setOpenPopup = {setOpenPopup}
                title="Update Academic Subject"
            >
                <AcademicSubjectEditForm 
                    recordForEdit = {recordForEdit}
                    editEntry = {editEntry}/>                
            </Modal>
            <ConfirmDialog 
                confirmDialog={confirmDialog}
                setConfirmDialog={setConfirmDialog} />
            
        </NgPageContainer>
    )
}

export default AcademicSubjectReadScreen
