
import { 
    ACADEMIC_SUBJECT_READ_REQUEST,
    ACADEMIC_SUBJECT_READ_SUCCESS,
    ACADEMIC_SUBJECT_READ_FAIL,

    ACADEMIC_SUBJECT_CREATE_REQUEST,
    ACADEMIC_SUBJECT_CREATE_SUCCESS,
    ACADEMIC_SUBJECT_CREATE_FAIL,
    ACADEMIC_SUBJECT_CREATE_RESET,

    ACADEMIC_SUBJECT_DETAILS_REQUEST,
    ACADEMIC_SUBJECT_DETAILS_SUCCESS,
    ACADEMIC_SUBJECT_DETAILS_FAIL,

    ACADEMIC_SUBJECT_UPDATE_REQUEST,
    ACADEMIC_SUBJECT_UPDATE_SUCCESS,
    ACADEMIC_SUBJECT_UPDATE_FAIL,

    ACADEMIC_SUBJECT_DELETE_REQUEST,
    ACADEMIC_SUBJECT_DELETE_SUCCESS,
    ACADEMIC_SUBJECT_DELETE_FAIL,



} from './constants';



export const academicSubjectReadReducer = (state = { academicSubjects:[] }, action) =>{
    switch(action.type) {
        case ACADEMIC_SUBJECT_READ_REQUEST:
            return {loading: true, academicSubjects:[]}
        
        case ACADEMIC_SUBJECT_READ_SUCCESS:
            return {
                        loading: false,
                        academicSubjects: action.payload.data.items,
                        index:action.payload.index,
                        totalPages: action.payload.data.page_count,
                        size: action.payload.data.page_size
                    }
        
        case ACADEMIC_SUBJECT_READ_FAIL:
            return {loading: false, error: action.payload}
        
        default:
            return state
    }
}


export const academicSubjectCreateReducer = (state = {}, action) =>{
    switch(action.type) {
        case ACADEMIC_SUBJECT_CREATE_REQUEST:
            return {loading: true}
        
        case ACADEMIC_SUBJECT_CREATE_SUCCESS:
            return {loading: false, success: true, academicSubject: action.payload.data.items}
        
        case ACADEMIC_SUBJECT_CREATE_FAIL:
            return {loading: false, error: action.payload}
        
        case ACADEMIC_SUBJECT_CREATE_RESET:
            return {}
        default:
            return state
    }
}


export const academicSubjectDetailsReducer = (state = { academicSubject: { } }, action) =>{
    switch(action.type) {
        case ACADEMIC_SUBJECT_DETAILS_REQUEST:
            return {loading: true, ...state}
        
        case ACADEMIC_SUBJECT_DETAILS_SUCCESS:
            return {loading: false, academicSubject: action.payload.data}
        
        case ACADEMIC_SUBJECT_DETAILS_FAIL:
            return {loading: false, error: action.payload}
        
        default:
            return state
    }
}


export const academicSubjectUpdateReducer = (state = { academicSubject: {} }, action) =>{
    switch(action.type) {
        case ACADEMIC_SUBJECT_UPDATE_REQUEST:
            return {loading: true}
        
        case ACADEMIC_SUBJECT_UPDATE_SUCCESS:
            return {loading: false, success: true, academicSubject: action.payload}
        
        case ACADEMIC_SUBJECT_UPDATE_FAIL:
            return {loading: false, error: action.payload}
        
        default:
            return state
    }
}

export const academicSubjectDeleteReducer = (state = {}, action) =>{
    switch(action.type) {
        case ACADEMIC_SUBJECT_DELETE_REQUEST:
            return {loading: true}
        
        case ACADEMIC_SUBJECT_DELETE_SUCCESS:
            return {loading: false, success: true}
        
        case ACADEMIC_SUBJECT_DELETE_FAIL:
            return {loading: false, error: action.payload}
        
        default:
            return state
    }
}

