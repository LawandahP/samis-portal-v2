import React, { useState, useEffect } from 'react';
import axios from 'axios';

import { 
    NgDashboardContainer, 
    NgDashboardOverview, 
    NgDashboardCardWrapper, 
    NgDashboardOverviewWrapper, 
    NgDashboardTableCard, 
} from './dashboardElements'

import DashboardCards from './dashBoardCards'
import { NgDivider } from '../../components/sidebar/sidebarElements'
import { GetCountContext } from '../../context';
import { List } from '../../components/display/elements';
import withRouter from '../../auth/with.router';

const Dashboard = () => {

    const [ error, setError ] = useState();
    const [ count, setCount ] = useState();

    const getCount = async () => {
        await axios.get('/get_count').then(res => {
            setCount(res.data.data.payload)
        }).catch(err => {
            setError(err.response && err.response.data.detail ?
                <>
                    {Object.keys(err.response.data.detail).map(function(s) {
                    return (
                        <List>{err.response.data.detail[s]}</List>
                    )})}
                </> 
                : err.message)
        })
    }

    useEffect(() => {
        getCount();
    }, [])

    return (
        <NgDashboardContainer>
            <NgDashboardCardWrapper>
                <GetCountContext.Provider value={{count, setCount, error, setError}}>
                    <DashboardCards /> 
                </GetCountContext.Provider>                                 
            </NgDashboardCardWrapper>
            <NgDivider />
            <NgDashboardOverviewWrapper>
                <NgDashboardTableCard>
                    Frequent Clients
                </NgDashboardTableCard>
                <NgDashboardOverview>OverView</NgDashboardOverview>
            </NgDashboardOverviewWrapper>

        </NgDashboardContainer>
    )
}

export default Dashboard;