import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'



import { createGame, deleteGame, readGames, updateGame } from './actions'
import { makeStyles } from '@mui/styles'

import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';

import { Avatar, Container, Grid } from '@mui/material'
// import { TESTIMONIAL_CREATE_RESET } from './testimonialConstants'

import { NgPageContainer, NgPaper } from '../../components/display/elements';
import { FiDelete, FiEdit3 } from 'react-icons/fi';
import Loader from '../../components/display/Loader';
import { TabTitle, toTitleCase } from '../../utils/globalFunc'
import Controls from '../../components/controls/Controls';
import Modal from '../../components/display/modal';
import TableComponent from '../../components/useTable';
import { TableTop } from '../../components/useTable/elements';
import ToastAlert from '../../components/display/ToastAlert';
import { ActionButtonWrapper } from '../../components/controls/Button';
import ConfirmDialog from '../../components/display/dialog';
import GameEditForm from './editform';
import GameForm from './form';

const headCells = [
    { id: 'image_url', label: '', disableSorting: true},
    { id: 'name', label: 'Game Name', minWidth: 170 },
    { id: 'description', label: 'Description', minWidth: 370 },
    { id: 'actions', label: 'Actions', disableSorting: true },

]


function GamesReadScreen() {
    TabTitle('Games | Samis Systems')
    const dispatch = useDispatch();

    const [ search, setSearch ] = useState({fn:items => {return items;}})
    const [ recordForEdit, setRecordForEdit ] = useState(null);

    const [ openPopup, setOpenPopup ] = useState(false)
    const [ openModal, setOpenModal ] = useState(false)

    const gameRead = useSelector(state => state.gameRead)
    const { loading, error, games } = gameRead

    const gameCreate = useSelector(state => state.gameCreate)
    const { success: successCreate } = gameCreate

    const gameUpdate = useSelector(state => state.gameUpdate)
    const { success: successUpdate } = gameUpdate

    const gameDelete = useSelector(state => state.gameDelete)
    const { success: successDelete } = gameDelete

    const [ message, setMessage ] = useState('')

    
    
    const [ confirmDialog, setConfirmDialog ] = useState({isOpen: false, title: '', subTitle: ''})

    const { TblContainer, 
            TblHead, 
            TblPagination, 
            recordsAfterPaginatingAndSorting,
            page, rowsPerPage
        } = TableComponent (games, headCells, search)
    
    
    useEffect(() => {
        dispatch(readGames(page, rowsPerPage))
        if(successCreate) {
            setMessage('Game Created Successfully')
        } else {
            if(successUpdate) {
                setMessage('Game Updated Successfully')
            }
        }
        if(successDelete) {
            setMessage('Game Deleted Successfully')
        }

    }, [dispatch, successCreate, successUpdate, successDelete, page, rowsPerPage])

    const handleSearch = (e) => {
        e.preventDefault()
        let target = e.target
        setSearch({
            fn:items => {
                if(target.value === "")
                    return items;
                else
                    return items.filter(x => x.name.includes(toTitleCase(e.target.value)))
            }
        })
    }


    const handleCreateGame = (game, handleResetForm) => {
        dispatch(createGame(game))
        // if(successCreate)
        //     handleResetForm()
        //     setOpenPopup(false);
    }

    const handleEditGame = (game, handleResetForm) => {
        dispatch(updateGame(game))
        if (successUpdate)
            setOpenPopup(false);
    }

    const editHandler = (game) => { 
        setRecordForEdit(game)
        setOpenPopup(true)
    }

    const deleteHandler = (id) => {
        setConfirmDialog({
            ...confirmDialog,
            isOpen: false
        })
        dispatch(deleteGame(id))  
    }

    

    return (
        
            
            <NgPageContainer>  

                <NgPaper>
                    <TableTop>
                        <Controls.SearchInputField
                            label="Search Games"
                            onChange={handleSearch}
                        />
                        <Controls.AddButton
                            onClick={() => setOpenModal(true)}
                        >
                        </Controls.AddButton>
                    </TableTop>

                    { message && <ToastAlert severity="success">{message}</ToastAlert>}                
                    { loading
                    ? <Loader />
                    : error
                    ? <ToastAlert severity="error">{error}</ToastAlert>
                    : (
                        <>                        
                            <TblContainer>
                                <TblHead />

                                {/* <TableBody> */}
                                {
                                    recordsAfterPaginatingAndSorting()?.map(game => 
                                        (<TableRow key={game.id}>
                                            <TableCell><Avatar src={game.image_url} variant="rounded"/></TableCell>
                                            <TableCell>{game.name}</TableCell>
                                            <TableCell>{game.description}</TableCell>
                                            <TableCell>
                                                <ActionButtonWrapper>
                                                    <Controls.ActionButton
                                                        title="edit"
                                                        onClick={() => editHandler(game)}
                                                        edit>
                                                        <FiEdit3 />
                                                    </Controls.ActionButton>
                                                    <Controls.ActionButton
                                                        title="deactivate"
                                                        onClick={() => {
                                                            setConfirmDialog({
                                                                isOpen: true,
                                                                title: "Are you sure you want to delete this Game?",
                                                                subTitle: "You can't undo this operation",
                                                                onConfirm: () => { deleteHandler(game.id) }
                                                            })
                                                        }}>
                                                        <FiDelete />
                                                    </Controls.ActionButton>
                                                </ActionButtonWrapper>
                                            </TableCell>
                                                                        
                                        </TableRow>)
                                )}
                                {/* </TableBody> */}
                            </TblContainer>
                        
                            <TblPagination />

                        </>
                        
                    )}
                </NgPaper> 
                
                                
                <Modal
                    openModal = {openModal}
                    setOpenModal = {setOpenModal}
                    title="Create Game"
                >
                    <GameForm 
                        recordForEdit = {recordForEdit}
                        handleCreateGame = {handleCreateGame}/>  
                </Modal>  

                <Modal
                    openPopup = {openPopup}
                    setOpenPopup = {setOpenPopup}
                    title="Edit Game"
                >
                    <GameEditForm
                        recordForEdit = {recordForEdit}
                        handleEditGame = {handleEditGame}
                    />
                </Modal> 
                
                <ConfirmDialog 
                    confirmDialog={confirmDialog}
                    setConfirmDialog={setConfirmDialog} />
                
            </NgPageContainer>      
        
    )
}

export default GamesReadScreen
