import { 
    GAME_CREATE_REQUEST,
    GAME_CREATE_SUCCESS,
    GAME_CREATE_FAIL,

    GAME_READ_REQUEST,
    GAME_READ_SUCCESS,
    GAME_READ_FAIL,

    GAME_UPDATE_REQUEST,
    GAME_UPDATE_SUCCESS,
    GAME_UPDATE_FAIL,
    GAME_UPDATE_RESET,

    GAME_DELETE_REQUEST,
    GAME_DELETE_SUCCESS,
    GAME_DELETE_FAIL,
    GAME_DELETE_RESET
} from './constants';


export const gameCreateReducer = (state = { }, action) =>{
    switch(action.type) {
        case GAME_CREATE_REQUEST:
            return {loading: true}
        
        case GAME_CREATE_SUCCESS:
            return {loading: false, success: true, game: action.payload.data}
        
        case GAME_CREATE_FAIL:
            return {loading: false, error: action.payload}
        
             
        default:
            return state
    }
}

export const gameReadReducer = (state = { games:[] }, action) =>{
    switch(action.type) {
        case GAME_READ_REQUEST:
            return {loading: true, games:[]}
        
        case GAME_READ_SUCCESS:
            return {
                        loading: false,
                        games: action.payload.data.items,
                        index:action.payload.index,
                        totalPages: action.payload.data.page_count,
                        size: action.payload.data.page_size
                    }
        
        case GAME_READ_FAIL:
            return {loading: false, error: action.payload}
        
        default:
            return state
    }
}

export const gameUpdateReducer = (state = { game: {} }, action) =>{
    switch(action.type) {
        case GAME_UPDATE_REQUEST:
            return {loading: true}
        
        case GAME_UPDATE_SUCCESS:
            return {loading: false, success: true, game: action.payload}
        
        case GAME_UPDATE_FAIL:
            return {loading: false, error: action.payload}
        
        case GAME_UPDATE_RESET:
            return {}
        default:
            return state
    }
}

export const gameDeleteReducer = (state = {}, action) =>{
    switch(action.type) {
        case GAME_DELETE_REQUEST:
            return {loading: true}
        
        case GAME_DELETE_SUCCESS:
            return {loading: false, success: true}
        
        case GAME_DELETE_FAIL:
            return {loading: false, error: action.payload}

        case GAME_DELETE_RESET:
            return {}
        default:
            return state
    }
}

