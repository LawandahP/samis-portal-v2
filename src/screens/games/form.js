import React, { useEffect, useState, useMemo } from 'react'

import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom'


import { MainForm, useForm } from '../../components/useForm'

import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import { styled } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';
import { Avatar, CardActionArea, CardMedia, Card } from '@mui/material';
import Loader from '../../components/display/Loader';
import ToastAlert from '../../components/display/ToastAlert';
import Controls from '../../components/controls/Controls';
import { FormButton, FormButtonWrapper } from '../../components/useForm/formElements';

import { createStatus, readStatus } from '../stepper/status/actions';
import { processPathName } from '../../utils/globalFunc';



const defaultImageSrc = "/images/placeholder.png"

const Input = styled('input')({
    display: 'none',
});

const useStyles = makeStyles(theme => ({
    btn: {
        width: "100%",
        color: "#ffff"
    },

    loader: {
        color: "#ffff",
        fontSize: "1rem"

    }

}));


const GameForm = (props) => {

    const { handleCreateGame, handleNext } = props;
    const classes = useStyles();

    const dispatch = useDispatch();
    const location = useLocation();

    const gameCreate = useSelector(state => state.gameCreate)
    const { error, loading, successGameCreate } = gameCreate

    const validate = (fieldValues = values) => {
        let temp = { ...errors }
        if ('name' in fieldValues)
            temp.name = fieldValues.name ? "" : "Game name is Required"
        if ('image_src' in fieldValues)
            temp.image_src = fieldValues.image_src === defaultImageSrc ? "Role is Required" : ""
        if ('description' in fieldValues)
            temp.description = fieldValues.description ? "" : "Description is Required"

        setErrors({ ...temp })

        // tests whether post array elements passes text implemented by validate() function
        if (fieldValues === values)
            return Object.values(temp).every(x => x === "")

    }

    const initialFValues = {
        name: '',
        description: '',
        image_file: '',
        image_src: defaultImageSrc,

    }

    const {
        values,
        // setValues,
        errors,
        setErrors,
        handleResetForm,
        handleInputChange
    } = useForm(initialFValues, true, validate);



    const submitHandler = (e) => {
        e.preventDefault()
        if (validate()) {
            const formData = new FormData()
            formData.append('name', values.name)
            formData.append('description', values.description)
            formData.append('image_file', values.image_file)
            handleCreateGame(formData, handleResetForm);
            handleResetForm()
        }
    }

    const [message, setMessage] = useState('')

    // Handle Next Step   
    const [ count, setCount ] = useState(0);
    const incrementCount = () => {
        setCount(prevCount => prevCount + 1);
    };

       const statusRead = useSelector(state => state.statusRead)
       const {getStep} = statusRead
       let currentStep = getStep.step

       const detectStepChange = useMemo(() => {
           return stepChange(currentStep)
       }, [stepChange])

       function stepChange(currentStep) {
           if(location.pathname === processPathName && currentStep > 6)
               handleNext()
       }

       useEffect(() => {
            dispatch(readStatus())
            if(successGameCreate) {
                setMessage('Game Created Successfully')
                incrementCount();
            }

            if (count === 3) {
                dispatch(createStatus(7))
                handleNext()
                setCount(0);
            }          
       }, [dispatch, successGameCreate])





    return (
        <>
            {error && <ToastAlert severity='error'>{error}</ToastAlert>}



            {/* {location.pathname === processPathName ? 
                    <Typography>Submit Atleast 4 Games: { count ? count : "0"}</Typography>
                    : ""
                }  */}


            {message && <ToastAlert severity='success'>{message}</ToastAlert>}
            {/* <Typography className={classes.title}>Sign Up</Typography> */}
            <MainForm onSubmit={submitHandler}>
                {loading ? <Loader />
                    : (
                        <>
                            <Grid container>
                                <Grid item md={6} sm={12} xs={12}>
                                    <Card>
                                        <CardActionArea>
                                            <CardMedia
                                                component="img"
                                                height="300"
                                                image={values.image_src}
                                                alt="game image"
                                            />
                                        </CardActionArea>
                                    </Card>

                                    <label htmlFor="contained-button-file">
                                        <Input accept="image/*"
                                            id="contained-button-file"
                                            type="file"
                                            name="image_file"
                                            onChange={handleInputChange}
                                        />

                                        <Button className={classes.input} variant="contained" component="span">
                                            Upload image
                                        </Button>
                                    </label>

                                </Grid>

                                <Grid item md={6} xs={12}>
                                    <Controls.InputField
                                        error={errors.name}
                                        label="Game Name"
                                        value={values.name}
                                        name='name'
                                        onChange={handleInputChange}>
                                    </Controls.InputField>

                                    <Controls.InputField
                                        multiline
                                        rows={8}
                                        error={errors.description}
                                        label="Description"
                                        value={values.description}
                                        name='description'
                                        onChange={handleInputChange}>
                                    </Controls.InputField>

                                    { loading ? <Loader />
                                        :   <FormButtonWrapper>
                                                <FormButton type='submit'>
                                                    Submit
                                                </FormButton>
                                            </FormButtonWrapper>
                                    }


                                </Grid>
                            </Grid>
                        </>
                    )}


            </MainForm>

        </>
    );
};

export default GameForm;
