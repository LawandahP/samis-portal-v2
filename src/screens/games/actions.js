import axios from 'axios';
import { backend, bff } from '../../proxy';

import { 
    GAME_CREATE_REQUEST,
    GAME_CREATE_SUCCESS,
    GAME_CREATE_FAIL,

    GAME_READ_REQUEST,
    GAME_READ_SUCCESS,
    GAME_READ_FAIL,

    GAME_UPDATE_REQUEST,
    GAME_UPDATE_SUCCESS,
    GAME_UPDATE_FAIL,

    GAME_DETAILS_REQUEST,
    GAME_DETAILS_SUCCESS,
    GAME_DETAILS_FAIL,

    GAME_DELETE_REQUEST,
    GAME_DELETE_SUCCESS,
    GAME_DELETE_FAIL,


} from './constants';



export const createGame = (game) => async (dispatch, getState) => {
    try {
        dispatch({
            type: GAME_CREATE_REQUEST
        })
        
        const config = {
            headers: {
                'Content-type':'multipart/form-data',
            }
        }
        const { data } = await axios.post(
            `${bff}/games`,
            game,
            config
        )

        dispatch({
            type: GAME_CREATE_SUCCESS,
            success: true,
            payload: data
        })
  

    } catch(error) {
        dispatch({
            type: GAME_CREATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}



export const readGames = (page, size) => async (dispatch) => {
    try {
        dispatch({ type: GAME_READ_REQUEST })
        const { data } = await axios.get(`${bff}/games?page=${page}&size=${size}`)

        dispatch({
            type:GAME_READ_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type:GAME_READ_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })
    }
}


export const gameDetails = (id) => async (dispatch) => {
    try {
        dispatch({ type: GAME_DETAILS_REQUEST })
        const { data } = await axios.get(`${backend}/games/v1/${id}`) //game in package.json "http://127.0.0.1:8000/"

        dispatch({
            type:GAME_DETAILS_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type:GAME_DETAILS_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })
    }
}


export const updateGame = (game) => async (dispatch) => {
    try {
        dispatch({
            type: GAME_UPDATE_REQUEST
        })
        
        const { data } = await axios.put(
            `${backend}/games/${game.id}`,
            game,
        )

        dispatch({
            type: GAME_UPDATE_SUCCESS,
            payload: data
        })

        //update details
        dispatch({
            type: GAME_DETAILS_SUCCESS,
            payload: data
        })

    } catch(error) {
        dispatch({
            type: GAME_UPDATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


export const deleteGame = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: GAME_DELETE_REQUEST
        })
        
        const { data } = await axios.delete(
            `${backend}/portal/bff/games/${id}`,
        )

        dispatch({
            type: GAME_DELETE_SUCCESS,
        })

    } catch(error) {
        dispatch({
            type: GAME_DELETE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}

