import React, {useEffect, useState } from 'react'

import { useDispatch, useSelector } from 'react-redux';


import { MainForm, useForm } from '../../components/useForm'

import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import { styled } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';
import { Avatar, CardActionArea, CardMedia, Card } from '@mui/material';
import Loader from '../../components/display/Loader';
import ToastAlert from '../../components/display/ToastAlert';
import Controls from '../../components/controls/Controls';
import { FormButton } from '../../components/useForm/formElements';


// const defaultImageSrc = "/images/placeholder.png"

const Input = styled('input')({
    display: 'none',
});

const useStyles = makeStyles(theme => ({
    btn: {
        width: "100%",
        color: "#ffff"
    }, 
    loader: {
        color: "#ffff",
        fontSize: "1rem"

    }

}));


const GameEditForm = (props) => {

    const { handleEditGame, recordForEdit, gameId } = props;
    const classes = useStyles();

    const gameUpdate = useSelector(state => state.gameUpdate)
    const { error, loading, success } = gameUpdate

    const [ message, setMessage ] = useState('')

    const validate = (fieldValues = values) => {
        let temp = {...errors}
        if('name' in fieldValues)
            temp.name = fieldValues.name ? "" : "Game name is Required"
        // if('image_src' in fieldValues)
        //     temp.image_src = fieldValues.image_src === defaultImageSrc ? "Role is Required" : ""
        if('description' in fieldValues)
            temp.description = fieldValues.description ? "" : "Description is Required"
       
        setErrors({ ...temp })

        // tests whether post array elements passes text implemented by validate() function
        if (fieldValues === values)
            return Object.values(temp).every(x => x === "")
        
    }

    const initialFValues = {
        name: '',
        description: '',
        image_file: '',
        // image_src: defaultImageSrc,
        
    }
    
    const { 
        values,  
        setValues,
        errors, 
        setErrors,
        handleResetForm,
        handleInputChange
    } = useForm(initialFValues, true, validate);
    


    const submitHandler = (e) => {
        e.preventDefault()
        if (validate()) {
            // const formData = new FormData()
            // formData.append('name', values.name)
            // formData.append('description', values.description)
            // formData.append('image_file', values.image_file)
            handleEditGame(values);
        }
    }
    
    useEffect(() => {
        if(recordForEdit != null)
            setValues({
                ...recordForEdit
            })            
    }, [success, recordForEdit, setValues, gameId])

    return (
        <>
            {error && <ToastAlert severity='error'>{error}</ToastAlert>}
                {/* {location.pathname === processPathName ? 
                    <Typography>Submit Atleast 4 Games: { count ? count : "0"}</Typography>
                    : ""
                }  */}
                    
                    
                    {message && <ToastAlert severity='success'>{message}</ToastAlert>}
                    {/* <Typography className={classes.title}>Sign Up</Typography> */}
                    <MainForm onSubmit={submitHandler}>
                        {loading ? <Loader /> 
                         :  (
                            <>
                                <Grid container>
                                    <Grid item md={6} sm={12} xs={12}>
                                        <Card>
                                            <CardActionArea>
                                                <CardMedia
                                                    component="img"
                                                    height="300"
                                                    image={values.image_src}
                                                    alt="game image"
                                                />
                                            </CardActionArea>
                                        </Card>

                                        <label htmlFor="contained-button-file">
                                            <Input accept="image/*" 
                                                id="contained-button-file" 
                                                type="file"
                                                name="image_file"
                                                onChange={handleInputChange}
                                            />

                                            <Button className={classes.input} variant="contained" component="span">
                                                Upload image
                                            </Button>
                                        </label>
                                        
                                    </Grid>

                                    <Grid item md={6} xs={12}>
                                        <Controls.InputField
                                            error={errors.name}
                                            label="Game Name"
                                            value={values.name}
                                            name='name'
                                            onChange={handleInputChange}>
                                        </Controls.InputField> 

                                        <Controls.InputField
                                            multiline
                                            rows={8}
                                            error={errors.description}
                                            label="Description"
                                            value={values.description}
                                            name='description'
                                            onChange={handleInputChange}>
                                        </Controls.InputField>
                                                                                
                                    </Grid>
                                </Grid>
                                            <FormButton type='submit'>
                                                Submit 
                                            </FormButton>
                            </>
                        )}
                        
                        
                    </MainForm>
            
        </>
     );
};

export default GameEditForm;
