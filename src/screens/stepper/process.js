import React, { useEffect } from 'react';
import { useLocation } from 'react-router-dom'

import { makeStyles } from '@mui/styles';

import Typography from '@mui/material/Typography';
import MuiSteppUp from './stepper';
import { useSelector } from 'react-redux';

import ToastAlert from '../../components/display/ToastAlert'
import Loader from '../../components/display/Loader'
import { TabTitle } from '../../utils/globalFunc'
import { NgFormWrapper, Container, FormWrapper } from '../../components/useForm/formElements';



const useStyles = makeStyles(theme => ({
    root: {
        paddingTop: "7px",
        alignItems: 'center'
    }
}));



const StepperScreen = () => {
    TabTitle('Sign Up Process - Samis Systems')
    const classes = useStyles();

    const location = useLocation();

    // const statusRead = useSelector(state => state.statusRead)
    // const {getStep} = statusRead
    // let currentStep = getStep.step

    // const detectStepChange = () => {
    //     return stepChange(currentStep)
    // }

    // function stepChange(currentStep) {
    //     if(currentStep > 7)
    //         history.push('/login')
    // }

    const statusRead = useSelector(state => state.statusRead)
    const { getStep } = statusRead

    let currentStep = getStep.step

    // if (currentStep > 8) {
    //     history.push('/login', { from: "Process Page" })
    // }


    // useEffect(() => {
        // if(userInfo) {
        //     history.push('/')
        // }

    // })

    return (
        <>
            <Container>
                <FormWrapper>
                    {location.state ? <ToastAlert severity="info">You have been redirected from the <strong>{location.state.from ? location.state.from : ""} Page.</strong>You are required to Complete the following steps to proceed. <strong>Do not reload this page.</strong></ToastAlert>
                    : ""}               
                    <MuiSteppUp />
                </FormWrapper>
            </Container>

        </>

    );
};

export default StepperScreen;
