import { 
    STATUS_CREATE_REQUEST,
    STATUS_CREATE_SUCCESS,
    STATUS_CREATE_FAIL,

    STATUS_READ_REQUEST,
    STATUS_READ_SUCCESS,
    STATUS_READ_FAIL
} from './constants';


export const statusCreateReducer = (state = { }, action) =>{
    switch(action.type) {
        case STATUS_CREATE_REQUEST:
            return {loading: true}
        
        case STATUS_CREATE_SUCCESS:
            return {loading: false, success: true, createStep: action.payload}
        
        case STATUS_CREATE_FAIL:
            return {loading: false, error: action.payload}
        
        // case DEFAULT_USER_LOGOUT:
        //     return {}   
             
        default:
            return state
    }
}


export const statusReadReducer = (state = { getStep:{} }, action) =>{
    switch(action.type) {
        case STATUS_READ_REQUEST:
            return {loading: true, getStep:{}}
        
        case STATUS_READ_SUCCESS:
            return {
                    loading: false,
                    getStep: action.payload,
                }
        
        case STATUS_READ_FAIL:
            return {loading: false, error: action.payload}
        
        default:
            return state
    }
}