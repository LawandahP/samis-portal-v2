import axios from 'axios';
import { backend } from '../../../proxy';



import {
    STATUS_CREATE_REQUEST,
    STATUS_CREATE_SUCCESS,
    STATUS_CREATE_FAIL,

    STATUS_READ_REQUEST,
    STATUS_READ_SUCCESS,
    STATUS_READ_FAIL

} from './constants';

export const createStatus = (step) => async (dispatch, getState) => {
    try {
        dispatch({
            type: STATUS_CREATE_REQUEST
        })

        const config = {
            headers: {
                'Content-type':'application/json'
            }
        }

        const { data } = await axios.post(
            `${backend}/school-info/v1/setup/status`,
            {"step": step},
            config
        )

        dispatch({
            type: STATUS_CREATE_SUCCESS,
            success: true,
            payload: data
        })


    } catch(error) {
        dispatch({
            type: STATUS_CREATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


export const readStatus = () => async (dispatch) => {
    try {
        dispatch({ type: STATUS_READ_REQUEST })
        const { data } = await axios.get(`${backend}/school-info/v1/setup/status`)

        dispatch({
            type:STATUS_READ_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type:STATUS_READ_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })
    }
}
