import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import PropTypes from 'prop-types';

import StepConnector, { stepConnectorClasses } from '@mui/material/StepConnector';
import { styled } from '@mui/material/styles';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel'

import { makeStyles, useTheme } from '@mui/styles';
import useMediaQuery from '@mui/material/useMediaQuery';


import { MdAppRegistration, MdSchool, MdOutlineHistoryEdu,
         MdOutlinePhone, MdOutlinePersonAddAlt, MdGames,
         MdOutlineFormatQuote
} from 'react-icons/md';

import { Container, NgFormWrapper, FormWrapper } from '../../components/useForm/formElements';

import { signUpUserAction } from '../../auth/users/actions';
import { createSchoolInfo } from '../school_info/actions';
import { createGame } from '../games/actions';
import { createStaff } from '../staff/actions';
import { createSchoolHistory } from '../school_history/actions';
import { createTestimonial } from '../testimonials/actions';
import { createContactInfo } from '../contact_info/actions';

import SchoolInfoForm from '../school_info/form';
import SchoolHistoryForm from '../school_history/form';
import ContactInfoForm from '../contact_info/form';
import StaffForm from '../staff/form';
import GameForm from '../games/form';
import TestimonialForm from '../testimonials/form';
import SignUpPage from '../../auth/users/signup';
import { StepText } from './elements';
import { readStatus } from './status/actions';



const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        paddingTop: "12px",
        justifyContent: "center",
        alignItems: "center", 
    },
    link: {
        textDecoration: "none",
        // color: theme.palette.primary.main
    },
    stepper: {
        // width: "100vh",
        // margin: "auto",
        justifyContent: "center",
        alignItems: "center", 
        paddingBottom: "none",
        // [theme.breakpoints('sm')] : {
        //     display: "block",
        // }
    },
    button: {
        justifyContent: "center"
    },
    content: {
        paddingTop: "2px"
    },
    icons: {
        // [theme.breakpoints.down('sm')] : {
        //     fontSize: "14px",
        // }
    },
    text: {
        // [theme.breakpoints.down('sm')]: {
        //     fontSize: "10px"
        // }
        
    }
}));


const ColorlibConnector = styled(StepConnector)(({ theme }) => ({
    [`&.${stepConnectorClasses.alternativeLabel}`]: {
        top: 22,
        [theme.breakpoints.down('sm')] : {
            top: 12,
            display: "none"
        }
    },
    [`&.${stepConnectorClasses.active}`]: {
        [`& .${stepConnectorClasses.line}`]: {
            backgroundColor: "theme.palette.primary.main",
            // 'linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(233,64,87) 50%,rgb(138,35,135) 100%)',
        },
    },
    [`&.${stepConnectorClasses.completed}`]: {
        [`& .${stepConnectorClasses.line}`]: {
            backgroundColor: theme.palette.primary.main,
            // 'linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(233,64,87) 50%,rgb(138,35,135) 100%)',
        },
    },
    [`& .${stepConnectorClasses.line}`]: {
        height: 3,
        border: 0,
        backgroundColor:
            theme.palette.mode === 'dark' ? theme.palette.grey[800] : '#eaeaf0',
        borderRadius: 1,
        [theme.breakpoints.down('sm')] : {
            height: 1,
        }
    },
  }));
  
    const ColorlibStepIconRoot = styled('div')(({ theme, ownerState }) => ({
        backgroundColor: theme.palette.mode === 'dark' ? theme.palette.grey[700] : '#ccc',
        zIndex: 1,
        color: "#fff",
        width: 50,
        height: 50,
        display: 'flex',
        border: "2px solid",
        borderRadius: '50%',
        justifyContent: 'center',
        alignItems: 'center',
        [theme.breakpoints.down('sm')] : {
            width: 30,
            height: 30,
        },
        ...(ownerState.active && {
        backgroundColor: "#fff",
        color: theme.palette.primary.main,
            // 'linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(233,64,87) 50%, rgb(138,35,135) 100%)',
        borderColor: theme.palette.primary.main,
        [theme.breakpoints.down('sm')] : {
            width: 30,
            height: 30,
        }
    }),
        ...(ownerState.completed && {
            backgroundColor: theme.palette.primary.main,
            borderColor: theme.palette.primary.main,
                // 'linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(233,64,87) 50%, rgb(138,35,135) 100%)',
        }),
    }));
  
    function ColorlibStepIcon(props) {
        const { active, completed, className } = props;
        const classes = useStyles()
        const icons = {
        1: <MdAppRegistration className={classes.icons}/>,
        2: <MdSchool className={classes.icons}/>,
        3: <MdOutlineHistoryEdu className={classes.icons}/>,
        4: <MdOutlinePhone className={classes.icons}/>,
        5: <MdOutlinePersonAddAlt className={classes.icons}/>,
        6: <MdGames className={classes.icons}/>,
        7: <MdOutlineFormatQuote className={classes.icons}/>,
        // 8: <DarkModeIcon className={classes.icons}/>,

        };
  
        return (
        <ColorlibStepIconRoot ownerState={{ completed, active }} className={className}>
            {icons[String(props.icon)]}
        </ColorlibStepIconRoot>
        );
    }
  
    ColorlibStepIcon.propTypes = {
        active: PropTypes.bool,
        className: PropTypes.string,
        completed: PropTypes.bool,
        icon: PropTypes.node,
    };
  


const MuiSteppUp = () => {
    const classes = useStyles();
    const steps = getSteps();
    const dispatch = useDispatch()

    // const breakP = useTheme();
    // const matches = useMediaQuery(theme.breakpoints.down('sm'));
    
   
    const [ activeStep, setActiveStep ] = useState(0)

    const handleRegisterUser = (defaultUser, handleResetForm) => {
        dispatch(signUpUserAction(defaultUser))
    }

    const handleCreateSchInfo = (info) => {
        dispatch(createSchoolInfo(info))
    }

    const handleStaffCreate = (staff) => {
        dispatch(createStaff(staff))
    }

    const handleGameCreate = (game) => {
        dispatch(createGame(game))
    }

    const handleTestimonyCreate = (testimonial) => {
        dispatch(createTestimonial(testimonial))
    }

    const handleSchoolHistCreate = (schoolHistory) => {
        dispatch(createSchoolHistory(schoolHistory))
    }

    const handleContactCreate = (contactInfo) => {
        dispatch(createContactInfo(contactInfo))
    }
    
    const handleNext = () => {
        setActiveStep(prevActiveStep => prevActiveStep + 1 )
    }

    // useEffect(() => {
    //     dispatch(readStatus())
    // }, [])

    function getSteps() {
        return [
                
                <StepText>SIGN UP</StepText>, 
                <StepText>SCHOOL INFO</StepText>,
                <StepText>SCH HISTORY</StepText>,
                <StepText>CONTACTS</StepText>,
                <StepText>STAFF</StepText>,
                <StepText>GAMES</StepText>,
                <StepText>TESTIMONIALS</StepText>,
            ];
    }

    function getStepsContent(stepIndex) {
        switch(stepIndex) {
            case 0:
                return <SignUpPage 
                            handleRegisterDefaultUser={handleRegisterUser}
                            handleNext={handleNext}
                            activeStep={activeStep}
                            steps={steps}
                        />
            case 1:
                return  <NgFormWrapper dark>
                            <SchoolInfoForm 
                                handleCreateSchoolInfo={handleCreateSchInfo}
                                handleNext={handleNext}
                                activeStep={activeStep}
                                steps={steps}
                            />
                        </NgFormWrapper>
                        
            case 2: 
                return  <NgFormWrapper dark>
                            <SchoolHistoryForm 
                                handleSchoolHistoryCreate={handleSchoolHistCreate}
                                handleNext={handleNext}
                                activeStep={activeStep}
                                steps={steps}
                            />
                        </NgFormWrapper>
                        
                

            // case 3:     
            //     return  <NgFormWrapper dark>
            //                 <ContactInfoForm 
            //                     handleContactInfoCreate={handleContactCreate}
            //                     handleNext={handleNext}
            //                     activeStep={activeStep}
            //                     steps={steps}
            //                 />
            //             </NgFormWrapper>
                        
            // case 4:
            //     return  <NgFormWrapper dark>
            //                 <StaffForm 
            //                     handleStaffCreate={handleStaffCreate}
            //                     handleNext={handleNext}
            //                     activeStep={activeStep}
            //                     steps={steps}
            //                 />
            //             </NgFormWrapper>

            // case 5:
            //     return  <NgFormWrapper dark>
            //                 <GameForm
            //                     handleCreateGame={handleGameCreate}
            //                     handleNext={handleNext}
            //                     activeStep={activeStep}
            //                     steps={steps}
            //                 />
            //             </NgFormWrapper>

            // case 6: 
            //     return  <NgFormWrapper dark>
            //                 <TestimonialForm 
            //                     handleTestimonialCreate={handleTestimonyCreate}
            //                     handleNext={handleNext}
            //                     activeStep={activeStep}
            //                     steps={steps}
            //                 />
            //             </NgFormWrapper>
                  
            default: 
                return ""
        }
    }
    

    return (
        
            <>
                
                <Stepper className={classes.stepper} activeStep={activeStep} connector={<ColorlibConnector />} alternativeLabel>
                    {steps.map(label => (
                        <Step key={label}>
                            <StepLabel StepIconComponent={ColorlibStepIcon}>
                                {label}
                            </StepLabel>
                        </Step>
                    ))}
                </Stepper>

                {activeStep === steps.length ? "Complete" : (
                    <div className={classes.content}>
                        {getStepsContent(activeStep)}
                        {activeStep}
                        
                    </div>  
                )}   

                {/* <Controls.Button
                    display={activeStep === steps.length ? "true" : "false"}
                    className={classes.button}
                    text="Next"
                    onClick={handleNext}>
                </Controls.Button>  */}
            </>
        
        
    );
};

export default MuiSteppUp;
