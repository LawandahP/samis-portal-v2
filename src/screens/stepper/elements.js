import styled from 'styled-components';



export const StepText = styled.p`
    font-size: 12px;

    @media screen and (max-width: 500px) {
        font-size: 6px;
  
    }

`