import React, {useState, useEffect, useMemo} from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Controls from '../../components/controls/Controls'
import ToastAlert from '../../components/display/ToastAlert'
import Loader from '../../components/display/Loader'

import { Grid } from '@mui/material';

// import { createStatus, getStatus } from '../stepper/stepperStatus/statusActions';
// import { processPathName } from '../utils/globalFunc';
import { MainForm, useForm } from '../../components/useForm';
import { FormButton } from '../../components/useForm/formElements';


function SchoolHistoryEditForm(props) {
    const { editEntry, recordForEdit, schoolHistoryId } = props;
    const dispatch = useDispatch();

    const schoolHistoryUpdate = useSelector(state => state.schoolHistoryUpdate)
    const { loading, error, success } = schoolHistoryUpdate

    // const [successMessage, setSuccessMessage] = useState('')
    
    const validate = (fieldValues = values) => {
        let temp = {...errors}
        if('language' in fieldValues)
            temp.language = fieldValues.language ? "" : "Language is Required"
        if('text' in fieldValues)
            temp.text = fieldValues.text ? "" : "Text is Required"
        if('excerpt' in fieldValues)
            temp.excerpt = fieldValues.excerpt ? "" : "Excerpt is Required"
        // temp.phone_no = (\\+?\\d{9,13}).test(values.phone_no) ? "" : "Student Name is Required"
        setErrors({ ...temp })

        // tests whether post array elements passes text implemented by validate() function
        if (fieldValues === values)
            return Object.values(temp).every(x => x === "")
        
    }

    const initialValues = {
        language: '',
        text: '',
        excerpt: '',
        
    }

    const { values,  
            setValues,
            errors, 
            setErrors, 
            handleResetForm, 
            handleInputChange } = useForm(initialValues, true, validate);

    
    
    useEffect(() => {
        if(recordForEdit != null)
            setValues({
                ...recordForEdit
            })
    
    }, [dispatch,  recordForEdit, success, loading, error, setValues, schoolHistoryId ])


    const submitHandler = (e) => {
        e.preventDefault()
        if (validate()) {
            editEntry(values, handleResetForm);
            handleResetForm()
        }
    }

   
    return (
        
        <>
            { loading && <Loader />}
            { error && <ToastAlert severity="error">{error}</ToastAlert>}
                <MainForm onSubmit={submitHandler}>
                    <Grid container>
                        <Grid item md={3} xs={12}>
                            <Controls.InputField
                                error={errors.language}
                                label="Language"
                                value={values.language}
                                name='language'
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <Grid item md={9} xs={12}>
                            <Controls.InputField 
                                error={errors.text}
                                label="Text"
                                name='text'
                                value={values.text}
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <Grid item md={12} xs={12}>
                            <Controls.InputField 
                                multiline="true"
                                error={errors.excerpt}
                                label="Excerpt"
                                name='excerpt'
                                value={values.excerpt}
                                onChange={handleInputChange}
                            />
                        </Grid>

                        <FormButton type='submit'>
                            Submit 
                        </FormButton>

                    </Grid>
                </MainForm>
        </>
        
            
            
                       
    )
}

export default SchoolHistoryEditForm
