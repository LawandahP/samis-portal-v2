import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea, CardActions } from '@mui/material';

import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import SchoolHistoryEditForm from './editform'
import SchoolHistoryForm from './form'
import { createSchoolHistory, deleteSchoolHistory, readSchoolHistory, updateSchoolHistory } from './actions'

import ConfirmDialog from '../../components/display/dialog'
import Modal from '../../components/display/modal'
import ToastAlert from '../../components/display/ToastAlert'
import Loader from '../../components/display/Loader'
import { NgPageContainer, NgPaper } from '../../components/display/elements'
import { ActionButtonWrapper } from '../../components/controls/Button'
import Controls from '../../components/controls/Controls';
import { FiEdit3 } from 'react-icons/fi';
import { TabTitle } from '../../utils/globalFunc';

// import { SCHOOL_HISTORY_CREATE_RESET } from './constants';


const SchoolHistoryReadScreen = () => {

    TabTitle('School History - Samis Systems')

    const dispatch = useDispatch();
    
    const [ search, setSearch ] = useState({fn:items => {return items;}})
    const [ recordForEdit, setRecordForEdit ] = useState(null);
    const [ message, setMessage ] = useState('')

    const [ openPopup, setOpenPopup ] = useState(false)
    const [ openModal, setOpenModal ] = useState(false)

    const schoolHistoryRead = useSelector(state => state.schoolHistoryRead)
    const { loading, error, schoolHistory } = schoolHistoryRead

    const schoolHistoryCreate = useSelector(state => state.schoolHistoryCreate)
    const { success: successCreate } = schoolHistoryCreate

    const schoolHistoryDelete = useSelector(state => state.schoolHistoryDelete)
    const { success: successDelete } = schoolHistoryDelete

    const schoolHistoryUpdate = useSelector(state => state.schoolHistoryUpdate)
    const { success: successUpdate } = schoolHistoryUpdate

    const [ confirmDialog, setConfirmDialog ] = useState({isOpen: false, title: '', subTitle: ''})

    const handleSchoolHistoryCreate = (schoolHistory, handleResetForm) => {
        dispatch(createSchoolHistory(schoolHistory))
        if(successCreate)
            handleResetForm()
            setOpenPopup(false);
    }

    const editEntry = (schoolHistory, handleResetForm) => {
        dispatch(updateSchoolHistory(schoolHistory))
        setOpenPopup(false);
        handleResetForm()   
    }

    const editHandler = (schoolHistory) => { 
        setRecordForEdit(schoolHistory)
        setOpenPopup(true)
    }

    const deleteHandler = (id) => {
        setConfirmDialog({
            ...confirmDialog,
            isOpen: false
        })
        dispatch(deleteSchoolHistory(id))  
    }

    useEffect(() => {
        // dispatch({ type: SCHOOL_HISTORY_CREATE_RESET })
        dispatch(readSchoolHistory())
        if(successCreate) {
            setMessage('School History Created Successfully')
        } else {
            if(successUpdate) {
                setMessage('School History Updated Successfully')
            }
        }
        if(successDelete) {
            setMessage('School History Deleted Successfully')
        }
    }, [dispatch, successCreate, successDelete, successUpdate])

    return (
        <>

            { loading
                ? <Loader />
                : error
                ? <ToastAlert severity="error">{error}</ToastAlert>
                : (
                    <Card >
                        <CardActionArea>
                            <CardMedia
                            component="img"
                            height="auto"
                            image={schoolHistory.image_url}
                            alt="green iguana"
                            />
                            <CardContent>
                            <Typography gutterBottom variant="h5" component="div">
                            {schoolHistory.language}
                            </Typography>
                            <Typography variant="body2" color="text.secondary">
                                {schoolHistory.excerpt}
                            </Typography>
                            </CardContent>
                        </CardActionArea>

                    <CardActions>
                    
                        <ActionButtonWrapper>
                            <Controls.ActionButton
                                title="edit"
                                onClick={() => editHandler(schoolHistory)}
                                edit>
                                <FiEdit3 />
                            </Controls.ActionButton>
                            {/* <Controls.ActionButton
                                title="deactivate"
                                onClick={() => {
                                    setConfirmDialog({
                                        isOpen: true,
                                        title: "Are you sure you want to delete this Subject?",
                                        subTitle: "You can't undo this operation",
                                        onConfirm: () => { deleteHandler(kcseSubject.id) }
                                    })
                                }}>
                                <FiDelete />
                            </Controls.ActionButton> */}
                        </ActionButtonWrapper>

                    </CardActions>
                </Card>
                )}

                    <Modal
                        openModal = {openModal}
                        setOpenModal = {setOpenModal}
                        title="Create School History"
                    >
                        <SchoolHistoryForm 
                            recordForEdit = {recordForEdit}
                            handleSchoolHistoryCreate = {handleSchoolHistoryCreate}/>                
                    </Modal>  

                    <Modal
                        openPopup = {openPopup}
                        setOpenPopup = {setOpenPopup}
                        title="Edit School History"
                    >
                        <SchoolHistoryEditForm
                            recordForEdit = {recordForEdit}
                            editEntry = {editEntry}/>                
                    </Modal>

                    <ConfirmDialog 
                        confirmDialog={confirmDialog}
                        setConfirmDialog={setConfirmDialog} />
        </>
        
    );
}
export default SchoolHistoryReadScreen;