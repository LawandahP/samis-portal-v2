import React, { useEffect, useMemo } from 'react'
import { useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';


import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import { CardActionArea, Grid } from '@mui/material';
import { makeStyles, styled } from '@mui/styles';


import { FiCamera } from 'react-icons/fi';
import Controls from '../../components/controls/Controls';
import ToastAlert from '../../components/display/ToastAlert';
import Loader from '../../components/display/Loader';
import { MainForm, useForm } from '../../components/useForm';
import { FormButton, FormButtonWrapper } from '../../components/useForm/formElements';

import { createStatus, readStatus } from '../stepper/status/actions';
import { processPathName } from '../../utils/globalFunc';


const defaultImageSrc = "/images/placeholder.png"
const Input = styled('input')({
    display: 'none',
});

const useStyles = makeStyles(theme => ({
    input: {
        margin: "5px"
    }
}));


const SchoolHistoryForm = (props) => {
    const { handleSchoolHistoryCreate, handleNext } = props;
    const classes = useStyles();

    const dispatch = useDispatch();
    const location = useLocation();

    const schoolHistoryCreate = useSelector(state => state.schoolHistoryCreate)
    const { loading, error, success } = schoolHistoryCreate

    // const [successMessage, setSuccessMessage] = useState('')

    const validate = (fieldValues = values) => {
        let temp = { ...errors }
        if ('image_file' in fieldValues)
            temp.image_file = fieldValues.image_file ? "" : "Image is Required"
        if ('excerpt' in fieldValues)
            temp.excerpt = fieldValues.excerpt ? "" : "Excerpt is Required"
        // temp.phone_no = (\\+?\\d{9,13}).test(values.phone_no) ? "" : "Student Name is Required"
        setErrors({ ...temp })

        // tests whether post array elements passes text implemented by validate() function
        if (fieldValues === values)
            return Object.values(temp).every(x => x === "")

    }

    const initialFValues = {
        excerpt: '',
        image_file: '',
        image_src: defaultImageSrc,

    }

    const { values,
        errors,
        setErrors,
        handleResetForm,
        handleInputChange } = useForm(initialFValues, true, validate);


    const statusRead = useSelector(state => state.statusRead)
    const {getStep} = statusRead

    let currentStep = getStep?.step

    const detectStepChange = useMemo(() => {
        return stepChange(currentStep)
    }, [stepChange,])


    function stepChange(currentStep) {
        if (location.pathname === processPathName && currentStep > 3)
            handleNext()
    }

    useEffect(() => {
        dispatch(readStatus())
        if (success) {
            dispatch(createStatus(4))
            handleNext() 
        }
    }, [success])



    const submitHandler = (e) => {
        e.preventDefault()
        if (validate()) {
            const formData = new FormData()
            formData.append('excerpt', values.excerpt)
            formData.append('image_file', values.image_file)
            handleSchoolHistoryCreate(formData, handleResetForm);
            // handleResetForm()
        }
    }


    return (

        <>
            {error && <ToastAlert severity='error'>{error}</ToastAlert>}
            <MainForm onSubmit={submitHandler}>
                <Grid container>
                    <Grid item md={6} xs={12}>
                        <Card>
                            <CardActionArea>
                                <CardMedia
                                    component="img"
                                    height="300"
                                    image={values.image_src}
                                    alt="game image"
                                />
                            </CardActionArea>
                        </Card>

                        <label htmlFor="contained-button-file">
                            <Input accept="image/*"
                                id="contained-button-file"
                                type="file"
                                name="image_file"
                                onChange={handleInputChange}
                            />

                            <Button className={classes.input} variant="contained" component="span">
                                <FiCamera /> Upload image
                            </Button>
                        </label>
                    </Grid>

                    <Grid item md={6} xs={12}>
                        <Controls.InputField
                            multiline
                            rows={11}
                            error={errors.excerpt}
                            label="Excerpt"
                            name='excerpt'
                            value={values.excerpt}
                            onChange={handleInputChange}
                        />

                    {loading ? <Loader />
                        :   <FormButtonWrapper>
                                <FormButton type='submit'>
                                    Submit
                                </FormButton>
                            </FormButtonWrapper>
                    }

                    </Grid>
                </Grid>
            </MainForm>
        </>
    )
}

export default SchoolHistoryForm
