import axios from 'axios';
import { backend, bff } from '../../proxy';

import { 
    SCHOOL_HISTORY_READ_REQUEST,
    SCHOOL_HISTORY_READ_SUCCESS,
    SCHOOL_HISTORY_READ_FAIL,

    SCHOOL_HISTORY_CREATE_REQUEST,
    SCHOOL_HISTORY_CREATE_SUCCESS,
    SCHOOL_HISTORY_CREATE_FAIL,
    SCHOOL_HISTORY_CREATE_RESET,

    SCHOOL_HISTORY_DETAILS_REQUEST,
    SCHOOL_HISTORY_DETAILS_SUCCESS,
    SCHOOL_HISTORY_DETAILS_FAIL,

    SCHOOL_HISTORY_UPDATE_REQUEST,
    SCHOOL_HISTORY_UPDATE_SUCCESS,
    SCHOOL_HISTORY_UPDATE_FAIL,

    SCHOOL_HISTORY_DELETE_REQUEST,
    SCHOOL_HISTORY_DELETE_SUCCESS,
    SCHOOL_HISTORY_DELETE_FAIL,
} from './constants';



export const readSchoolHistory = () => async (dispatch) => {
    try {
        dispatch({ type: SCHOOL_HISTORY_READ_REQUEST })
        const { data } = await axios.get(`${bff}/school-history/en`)

        dispatch({
            type:SCHOOL_HISTORY_READ_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type:SCHOOL_HISTORY_READ_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.message
        })
    }
}



export const createSchoolHistory = (schoolHistory) => async (dispatch, getState) => {
    try {
        dispatch({
            type: SCHOOL_HISTORY_CREATE_REQUEST
        })

        const config = {
            headers: {
                'Content-type':'multipart/form-data'
            }
        }
        
        const { data } = await axios.post(
            `${bff}/school-history/en`,
            schoolHistory,
            config
        )

        dispatch({
            type: SCHOOL_HISTORY_CREATE_SUCCESS,
            success: true,
            payload: data
        })

        sessionStorage.setItem('schoolHistory', JSON.stringify(data))
    } catch(error) {
        dispatch({
            type: SCHOOL_HISTORY_CREATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


export const historyDetails = (id) => async (dispatch) => {
    try {
        dispatch({ type: SCHOOL_HISTORY_DETAILS_REQUEST })
        const { data } = await axios.get(`${backend}/school/history/${id}`) //backend in package.json "http://127.0.0.1:8000/"

        dispatch({
            type:SCHOOL_HISTORY_DETAILS_SUCCESS,
            payload: data
        })
    } catch (error) {
        dispatch({
            type:SCHOOL_HISTORY_DETAILS_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })
    }
}


export const updateSchoolHistory = (schoolHistory) => async (dispatch) => {
    try {
        dispatch({
            type: SCHOOL_HISTORY_UPDATE_REQUEST
        })
        
        const { data } = await axios.put(
            `${backend}/school/history/v1/${schoolHistory.language}`,
            schoolHistory,
        )

        dispatch({
            type: SCHOOL_HISTORY_UPDATE_SUCCESS,
            payload: data
        })

        //update details
        dispatch({
            type: SCHOOL_HISTORY_DETAILS_SUCCESS,
            payload: data
        })

    } catch(error) {
        dispatch({
            type: SCHOOL_HISTORY_UPDATE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}


export const deleteSchoolHistory = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: SCHOOL_HISTORY_DELETE_REQUEST
        })
        
        const { data } = await axios.delete(
            `${backend}/school/history/v1/${id}`,
            //config
        )

        dispatch({
            type: SCHOOL_HISTORY_DELETE_SUCCESS,
        })

    } catch(error) {
        dispatch({
            type: SCHOOL_HISTORY_DELETE_FAIL,
            payload: error.response && error.response.data.detail
                ? error.response.data.detail
                : error.message
        })

    }
}
