
import { 
    SCHOOL_HISTORY_READ_REQUEST,
    SCHOOL_HISTORY_READ_SUCCESS,
    SCHOOL_HISTORY_READ_FAIL,

    SCHOOL_HISTORY_CREATE_REQUEST,
    SCHOOL_HISTORY_CREATE_SUCCESS,
    SCHOOL_HISTORY_CREATE_FAIL,
    SCHOOL_HISTORY_CREATE_RESET,

    SCHOOL_HISTORY_DETAILS_REQUEST,
    SCHOOL_HISTORY_DETAILS_SUCCESS,
    SCHOOL_HISTORY_DETAILS_FAIL,

    SCHOOL_HISTORY_UPDATE_REQUEST,
    SCHOOL_HISTORY_UPDATE_SUCCESS,
    SCHOOL_HISTORY_UPDATE_FAIL,

    SCHOOL_HISTORY_DELETE_REQUEST,
    SCHOOL_HISTORY_DELETE_SUCCESS,
    SCHOOL_HISTORY_DELETE_FAIL,



} from './constants';



export const schoolHistoryReadReducer = (state = { schoolHistory:{} }, action) =>{
    switch(action.type) {
        case SCHOOL_HISTORY_READ_REQUEST:
            return {loading: true, schoolHistory: {}}
        
        case SCHOOL_HISTORY_READ_SUCCESS:
            return {
                        loading: false,
                        schoolHistory: action.payload.data,
                        index:action.payload.index,
                        totalPages: action.payload.data.page_count,
                        size: action.payload.data.page_size
                    }
        
        case SCHOOL_HISTORY_READ_FAIL:
            return {loading: false, error: action.payload}
        
        default:
            return state
    }
}




export const schoolHistoryCreateReducer = (state = {}, action) =>{
    switch(action.type) {
        case SCHOOL_HISTORY_CREATE_REQUEST:
            return {loading: true}
        
        case SCHOOL_HISTORY_CREATE_SUCCESS:
            return {loading: false, success: true, schoolHistory: action.payload.data.items}
        
        case SCHOOL_HISTORY_CREATE_FAIL:
            return {loading: false, error: action.payload}
        
        case SCHOOL_HISTORY_CREATE_RESET:
            return {}
        default:
            return state
    }
}


export const schoolHistoryDetailsReducer = (state = { schoolHistory: { } }, action) =>{
    switch(action.type) {
        case SCHOOL_HISTORY_DETAILS_REQUEST:
            return {loading: true, ...state}
        
        case SCHOOL_HISTORY_DETAILS_SUCCESS:
            return {loading: false, schoolHistory: action.payload.data}
        
        case SCHOOL_HISTORY_DETAILS_FAIL:
            return {loading: false, error: action.payload}
        
        default:
            return state
    }
}


export const schoolHistoryUpdateReducer = (state = { schoolHistory: {} }, action) =>{
    switch(action.type) {
        case SCHOOL_HISTORY_UPDATE_REQUEST:
            return {loading: true}
        
        case SCHOOL_HISTORY_UPDATE_SUCCESS:
            return {loading: false, success: true, schoolHistory: action.payload}
        
        case SCHOOL_HISTORY_UPDATE_FAIL:
            return {loading: false, error: action.payload}
        
        default:
            return state
    }
}

export const schoolHistoryDeleteReducer = (state = {}, action) =>{
    switch(action.type) {
        case SCHOOL_HISTORY_DELETE_REQUEST:
            return {loading: true}
        
        case SCHOOL_HISTORY_DELETE_SUCCESS:
            return {loading: false, success: true}
        
        case SCHOOL_HISTORY_DELETE_FAIL:
            return {loading: false, error: action.payload}
        
        default:
            return state
    }
}

