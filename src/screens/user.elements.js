import styled from 'styled-components';
import { Link as RouterLink } from 'react-router-dom';
import { spacing } from '../styles/variables';


export const UserCard = styled.div`
    background: ${({ theme }) => theme.bg};
    display: grid;
    grid-template-columns: auto auto auto auto;
    /* padding: 20px; */
    margin-bottom: 10px;
    /* justify-content: space-evenly;
    align-items: center; */

    @media screen and (max-width: 768px) {
        grid-template-columns: auto auto;
    }
`

export const UserInfoWrapper = styled.div`
    display: flex;
    align-items: center;
    /* padding: 10px; */
`

export const ProfilePicture = styled.img`
    width: 100px;
    height: auto;
    margin-right: 15px;
    border-radius: 5px;
    box-shadow: 0 1px 3px rgba(0,0,0,0.2);
`

export const DetailsWrapper = styled.div`
    display: inline-block;
`;

export const Chip = styled.div`
    font-size: 6px;
    background: grey;
    color: #fff;
    margin: 3px;
    padding: 5px;
    border-radius: 10px;
`

export const UserName = styled.h3`
    font-weight: bold;

    @media screen and (max-width: 768px) {
        font-size: 12px;
        font-weight: bold;
    }
`

export const Status = styled.div`
    /* background: green; */
    background: ${({ active }) => (!active ? 'red' : '#01bf71')};
    text-align: center;
    padding: 5px;
    font-size: 10px;
    color: #fff;
    border-radius: 20px;
    width: ${({ active }) => (!active ? '70px' : '50px')};
    cursor: pointer;
`

export const FlexWrapper = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
`
export const Icon = styled.div`
    font-size: 1.2rem; 
    margin-right: 5px;
`

export const IconText = styled.p`
    font-size: 12px; 
`

export const InfoWrapper = styled.div`
    display: inline-block;
`

export const UserLinkWrapper = styled.div`
    /* font-size: 10px; */
    margin: 3px;
`


export const UserLink = styled(RouterLink)`
    color: #01BF71;
    text-decoration: none;
    /* font-weight: 700; */
    &:hover {
        transform: scale(1.01);
        transition: all 0.2s ease-in-out;
        color: #a0eecd;
    }
    
`
export const InlineWrapper = styled.div`
    display: inline-block;
    flex-direction: column;
    align-items: center;
`

export const TableWrapper = styled.div`
    background: ${({ theme }) => theme.bg};
    display: grid;
    grid-template-columns: auto auto auto;
    padding: 20px;
    margin-bottom: 10px;
    justify-content: space-evenly;
    align-items: center;
`

export const IconWrapper = styled.div`
    /* position: relative; */
    /* top: 3;
    right: 3; */
    /* display: flex;
    flex-direction: row;
    align-items: center; */
`

export const UserCardWrapper = styled.div`
    display: grid;
    grid-template-columns: auto auto auto auto;
    align-items: start;
    margin-bottom: ${spacing.mdSpacing};
           
    @media screen and (max-width: 1024px) {
        grid-template-columns: auto auto;       
    };

    @media screen and (max-width: 600px) {
        grid-template-columns: auto;       
    };

    grid-gap: ${spacing.smSpacing};
    
`;