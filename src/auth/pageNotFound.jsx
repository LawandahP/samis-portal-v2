import { Link } from "react-router-dom"
import { NgPageContainer } from "../components/display/elements"
import { NgDashboardContainer } from '../screens/dashboard/dashboardElements'
import { Container, FormWrapper } from '../components/useForm/formElements'
const PageNotFound = () => {
    return (
        <Container>
            <FormWrapper>
                <NgPageContainer>
                    <article>
                        <h1>Oops! 404</h1>
                        <p>Page Not Found</p>
                        <>
                            <Link to="/dashboard">Go to Dashboard</Link>
                        </>
                    </article>
                </NgPageContainer>
            </FormWrapper>
        </Container>
        
        
    )
}

export default PageNotFound