import { useContext } from "react";
import AuthContext from "./auth.provider";
import { useLocation, Navigate, Outlet } from "react-router-dom";
import { getCookie } from "./cookies";

export const useAuth = () => {
    return useContext(AuthContext);
}

const RequireAuth = ({ allowedRoles }) => {
    const { auth } = useAuth();
    const location = useLocation();

    let user_cookie = getCookie('userId');
    console.log(auth)
    return (
        auth?.roles?.find(role => allowedRoles?.includes(role))
            ? <Outlet />
            : auth?.email 
                ? <Navigate to="/unauthorized" state={{ from: location }} replace />
                : !user_cookie ? <Navigate to="/signin" state={{ from: location }} replace /> 
                : <Outlet />
    );
}

export default RequireAuth;

