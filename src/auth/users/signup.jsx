import React, { useEffect, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useLocation } from 'react-router-dom';

import Controls from '../../components/controls/Controls'
import ToastAlert from '../../components/display/ToastAlert'

import { MainForm, useForm } from '../../components/useForm'
import { processPathName, toTitleCase } from '../../utils/globalFunc'
// import { signUpUserAction } from './actions'
import {
    Container, Icon, FormButton,
    FormContent, FormH1, NgFormWrapper, FormLabel,
    FormWrapper, Text, FormButtonWrapper
}
    from '../../components/useForm/formElements'

import Grid from '@mui/material/Grid'
import { NgLink } from '../../components/display/elements'
import { createStatus, readStatus } from '../../screens/stepper/status/actions'
import Loader from '../../components/display/Loader';

const SignUpPage = (props) => {

    const { handleRegisterDefaultUser, handleNext } = props;
    const dispatch = useDispatch()

    const location = useLocation()
    const signUpUser = useSelector(state => state.signUpUser)
    const { error, loading, success } = signUpUser

    const validate = (fieldValues = values) => {
        let temp = { ...errors }
        if ('company_name' in fieldValues)
            temp.company_name = fieldValues.company_name ? "" : "Company Name is Required"
        if ('full_name' in fieldValues)
            temp.full_name = fieldValues.full_name ? "" : "Full Name is Required"
        if ('email' in fieldValues)
            temp.email = (/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/).test(fieldValues.email) ? "" : "Enter a valid Email"
        if ('phone_no' in fieldValues)
            temp.phone_no = fieldValues.phone_no ? "" : "Phone Number is Required"
        if ('password' in fieldValues)
            temp.password = (/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#.^])[A-Za-z\d@$!%*?&#.^]{8,}$/).test(fieldValues.password) ? "" : "Minimum 8 Characters. Atleast 1 Capital, Small and Special Character"
        setErrors({ ...temp })

        // tests whether post array elements passes text implemented by validate() function
        if (fieldValues === values)
            return Object.values(temp).every(x => x === "")
    }

    const initialValues = {
        username: '',
        full_name: '',
        phone_no: '',
        email: '',
        password: ''
    }

    const {
        values,
        errors,
        setErrors,
        handleResetForm,
        handleInputChange
    } = useForm(initialValues, true, validate);
    
    const submitHandler = (e) => {
        e.preventDefault()
        if (validate()) {
            dispatch(handleRegisterDefaultUser(values))
        }
    }

    const statusRead = useSelector(state => state.statusRead)
    const { getStep } = statusRead

    let currentStep = getStep.step
    console.log(currentStep)

    const detectStepChange = useMemo(() => {
        return stepChange(currentStep)
    }, [stepChange])


    function stepChange(currentStep) {
        if (location.pathname === processPathName && currentStep > 1)
            handleNext()
    }

    useEffect(() => {
        dispatch(readStatus())
        if (location.pathname === processPathName && currentStep > 0) {
            handleNext()
        }
        if (success) {
            dispatch(createStatus(1))
            handleNext()
        }
    }, [success])



    return (
        <>
            <FormWrapper>
                {currentStep}
                <FormContent>
                    <NgFormWrapper dark>
                        <MainForm dark onSubmit={submitHandler}>
                            {error && <ToastAlert severity="error">{error}</ToastAlert>}
                            <FormH1>Create Account</FormH1>

                            <Grid container>
                                <Grid item md={6} xs={12}>
                                    <Controls.InputField
                                        // error={errors.username}
                                        label="Username"
                                        value={values.username}
                                        name='username'
                                        onChange={handleInputChange}>
                                    </Controls.InputField>
                                </Grid>

                                <Grid item md={6} xs={12}>
                                    <Controls.InputField
                                        value={values.full_name}
                                        name='full_name'
                                        onChange={handleInputChange}
                                        label='Full Name'
                                        error={errors.full_name}
                                        onInput={(e) => e.target.value = toTitleCase(e.target.value)}>
                                    </Controls.InputField>
                                </Grid>

                                <Grid item md={6} xs={12}>
                                    <Controls.InputField
                                        value={values.email}
                                        name='email'
                                        onChange={handleInputChange}
                                        label='Email'
                                        error={errors.email}>
                                    </Controls.InputField>
                                </Grid>

                                <Grid item md={6} xs={12}>
                                    <Controls.InputField
                                        value={values.phone_no}
                                        name='phone_no'
                                        onChange={handleInputChange}
                                        label='Phone Number'
                                        error={errors.phone_no}>
                                    </Controls.InputField>
                                </Grid>

                                <Grid item md={6} xs={12}>
                                    <Controls.PasswordInputField
                                        value={values.password}
                                        onChange={handleInputChange}
                                        error={errors.password}
                                    />
                                </Grid>
                            </Grid>

                            {loading ? <Loader />
                                :   <FormButtonWrapper>
                                        <FormButton type='submit'>
                                            Submit
                                        </FormButton>
                                    </FormButtonWrapper>
                            }

                        </MainForm>
                        <Text>Have an account? <NgLink to="/signin">sign in</NgLink></Text>
                    </NgFormWrapper>
                </FormContent>
            </FormWrapper>
        </>
    )
}

export default SignUpPage