import axios from 'axios';
import { backend, bff } from '../../proxy';

import { 
    USER_SIGN_UP_REQUEST, 
    USER_SIGN_UP_SUCCESS, 
    USER_SIGN_UP_FAIL,

    USER_SIGN_IN_REQUEST,
    USER_SIGN_IN_SUCCESS,
    USER_SIGN_IN_FAIL,
    
} from './constants';

import { List } from '../../components/display/elements';


export const signInUserAction = (user) => async (dispatch) => {
    try {
        dispatch({
            type: USER_SIGN_IN_REQUEST
        })

        const { data } = await axios.post(
            `${bff}/users/authenticate`,
            user,
        )
        dispatch({
            type: USER_SIGN_IN_SUCCESS,
            payload: data
        })
        // sessionStorage.setItem('userInfo', JSON.stringify(data))

    } catch(error) {
        dispatch({
            type: USER_SIGN_IN_FAIL,
            payload: error.response && error.response.data.detail
                ?   <>
                    {Object.keys(error.response.data.detail).map(function(s) {
                        return (
                            <List>{error.response.data.detail[s]}</List>
                        )})}
                    </> 
                : error.message
        })
    }
}

export const signUpUserAction = (user) => async (dispatch, getState) => {
    try {
        dispatch({
            type: USER_SIGN_UP_REQUEST
        })
        const { data } = await axios.post(
            `${backend}/users/v1/default-user`,
            user,
        )
        dispatch({
            type: USER_SIGN_UP_SUCCESS,
            success: true,
            payload: data
        })
    } catch(error) {
        dispatch({
            type: USER_SIGN_UP_FAIL,
            payload: error.response && error.response.data.errors.length > 0
                ?  <>
                        {Object.keys(error.response.data.errors).map(function(s) {
                            return (
                                <List>{error.response.data.errors[s]}</List>
                            )})}
                    </> 
                : error.response && error.response.data.message
                ?  error.response.data.message  
                : error.message
        })

    }
}