import React, { useState, useEffect } from 'react'
import axios from 'axios';
import { backend, bff } from '../../proxy';

import { useDispatch } from 'react-redux'
import { useNavigate, Link, useLocation } from 'react-router-dom';

import Controls from '../../components/controls/Controls'
import ToastAlert from '../../components/display/ToastAlert'
import Loader from '../../components/display/Loader'

import { MainForm, useForm } from '../../components/useForm'
import { Container, FormButton, 
        FormContent, FormH1, NgFormWrapper,
        FormWrapper, 
        Text,
        FormButtonWrapper
    } 
from '../../components/useForm/formElements'
import { List, NgLink } from '../../components/display/elements';
import { useAuth } from '../use.auth';
import { deleteCookie, getCookie, setCookie } from '../cookies';
// import GetCookie from '../get.cookie';

const SignInPage = () => {
    
    const { setAuth } = useAuth();

    let navigate = useNavigate();
    const location = useLocation();
    const from = location.state?.from?.pathname || "/"  //get previous page

    const [ loading, setLoading ] = useState(false)
    const [ error, setError ]     = useState(false)
    const [ success, setSuccess ] = useState(false)
    const [ message, setMessage ] = useState(false)

    const validate = (fieldValues = values) => {
        let temp = {...errors}
        if('username' in fieldValues)
            temp.name = fieldValues.username ? "" : "username is required"
        // if('username' in fieldValues)
        //     temp.username = (/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/).test(fieldValues.username) ? "" 
        //     : "Enter a valid Email"
            
        // if('password' in fieldValues)
        //     temp.password = (/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#.^])[A-Za-z\d@$!%*?&#.^]{8,}$/).test(fieldValues.password) ? "" 
        //     : "Minimum 8 Characters. Atleast 1 Capital, Small and Special Character"
        setErrors({ ...temp })

        // tests whether post array elements passes text implemented by validate() function
        if (fieldValues === values)
            return Object.values(temp).every(x => x === "")
    }
    
    const initialValues = {
        username: '',
        password: ''
    }


    const { 
        values,  
        errors, 
        setErrors,
        handleInputChange
    } = useForm(initialValues, true, validate);

    
    const submitHandler = async (e) => {
        e.preventDefault()
        if (validate()) {
            setLoading(true)
            await axios.post(
                `${bff}/users/authenticate`, values,
                { headers: { 'Content-Type': 'application/json'}}
            ).then(res => {
                deleteCookie("userId")
                setLoading(false)
                setSuccess(true)
                setMessage(res?.data.message)
                
                const roles = res?.data?.data?.roles
                const email = res?.data?.data?.email
                const phone_no = res?.data?.data?.phone_no

                let cookieVal = email + roles + phone_no
                setCookie("userId", cookieVal, 3)

                setAuth({ email, roles });
                navigate(from, { replace: true});

            }).catch(err => {
                setError(err.response && err.response.data.detail ?
                    <>
                        {Object.keys(err.response.data.detail).map(function(s) {
                        return (
                            <List>{err.response.data.detail[s]}</List>
                        )})}
                    </> 
                    : err.message)
                setLoading(false)
            });
        }
    }

    useEffect(() => {
        let loggedIn = getCookie("userId")
        if (loggedIn) {
            navigate(from, {replace: true})
        }
    }, [])

    return (
        <>
            <Container>
                <FormWrapper>
                    {/* <Icon to="/">KgHomes</Icon> */}
                    <FormContent>
                        <NgFormWrapper dark>
                            <MainForm onSubmit={submitHandler}>
                            {error && <ToastAlert severity='error'>{error}</ToastAlert>}
                            {success && <ToastAlert severity='success'>{message}</ToastAlert>}
                            <FormH1>Login To Your Account</FormH1>                       
                                <Controls.InputField
                                    value={values.username}
                                    name='username'
                                    onChange={handleInputChange}
                                    label='Username' 
                                    error={errors.username}>
                                </Controls.InputField>
                    
                                <Controls.PasswordInputField
                                    value={values.password}
                                    onChange={handleInputChange}
                                    // error={errors.password}
                                /> 

                            {loading ? <Loader />
                                :   <FormButtonWrapper>
                                        <FormButton type='submit'>
                                            Submit
                                        </FormButton>
                                    </FormButtonWrapper>
                            }   
                                    

                            </MainForm>
                            {/* <Text>Don't have an account ? <NgLink to="/signup">Sign Up</NgLink></Text> */}
                            <Text>Forgot Password ? <NgLink to="/signin">Reset Password</NgLink></Text>
                        </NgFormWrapper>
                    </FormContent>
                </FormWrapper>
            </Container>
        </>
    )
}

export default SignInPage























// const submitHandler = async (e) => {
    //     e.preventDefault()
    //     if (validate()) {
    //         try {
    //             setLoading(true)
    //             const response = await axios.post(
    //                 '/auth/login/', values,
    //                 { headers: { 'Content-Type': 'application/json'}, withCredentials: true }
    //             );
    //             setLoading(false)
    //             setSuccess(true)
    //             console.log(JSON.stringify(response?.data));
    //         } catch(err) {
    //             setError(err.response && err.response.data.detail ?
    //                 <>
    //                     {Object.keys(err.response.data.detail).map(function(s) {
    //                     return (
    //                         <List>{err.response.data.detail[s]}</List>
    //                     )})}
    //                 </> 
    //                 : err.message)
    //             setLoading(false)
    //         }
    //     }
    // }