import { 
    USER_SIGN_UP_REQUEST, 
    USER_SIGN_UP_SUCCESS, 
    USER_SIGN_UP_FAIL,

    USER_SIGN_IN_REQUEST,
    USER_SIGN_IN_SUCCESS,
    USER_SIGN_IN_FAIL,
    
} from './constants';


export const userSignInReducer = (state = { }, action) =>{
    switch(action.type) {
        case USER_SIGN_IN_REQUEST:
            return {loading: true}
        
        case USER_SIGN_IN_SUCCESS:
            return {loading: false, success: true, userInfo: action.payload}
        
        case USER_SIGN_IN_FAIL:
            return {loading: false, error: action.payload}
        
        // case USER_LOGOUT:
        //     return {}

        default:
            return state
    }
}


export const userSignUpReducer = (state = { }, action) =>{
    switch(action.type) {
        case USER_SIGN_UP_REQUEST:
            return {loading: true}
        
        case USER_SIGN_UP_SUCCESS:  
            return {loading: false, success: true, userInfo: action.payload.data}
        
        case USER_SIGN_UP_FAIL:
            return {loading: false, error: action.payload}
        
        // case USER_LOGOUT:
        //     return {}   
             
        default:
            return state
    }
}