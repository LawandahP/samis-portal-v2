import React from 'react';

import { Spinner, SpinnerContainer } from './elements';

const Loader = () => {
    return (
        <SpinnerContainer>
            <Spinner />
        </SpinnerContainer>
    )
}

export default Loader
