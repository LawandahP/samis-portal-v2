import styled from 'styled-components';

import { Link as RouterLink } from 'react-router-dom';
import { spacing } from '../../styles/variables';


export const NgPageContainer = styled.div`
    display: grid;
    /* grid-template-columns: auto auto auto auto; */
    align-items: start;
    padding-top: ${({padding}) => (padding ? "20px" : "")};
    align-items: center;
    margin-bottom: ${spacing.mdSpacing};
    grid-gap: ${spacing.smSpacing};
    
`;

export const NgPaper = styled.div`
    background: ${({theme}) => theme.bg};
    /* display: flex; */
    border-radius: 5px;
    padding: 10px;
    box-shadow: 0 1px 3px rgba(0,0,0,0.2);
    width: 100% !important;
    overflow: auto;

    &:hover {
        /* transform: scale(1.01);
        transition: all 0.2s ease-in-out; */
        cursor: pointer;
    }
`;


export const List = styled.li`
    list-style: none;
`

export const NgLink = styled(RouterLink)`
    color: #01BF71;
    text-decoration: none;
    /* font-weight: 700; */
    &:hover {
        transform: scale(1.01);
        transition: all 0.2s ease-in-out;
        color: #a0eecd;
    }
`


// Loader elements

export const LoaderWrapper = styled.div`
    display: flex;
    align-items: center;
`

export const LoaderBar = styled.div`
    display: inline-block;
    width: 3px;
    height: 20px;
    background-color: rgba(255, 255, 255, 0.5);
    border-radius: 10px;
    animation: scale up 1s linear infinite;

    &:nth-child(2) {
        height: 35px;
        margin: 0 5px;
        animation-delay: .0025s;
    }

    &:nth-child(3) {
        animation-delay: .005s;
    }

    @keyframes scale-up {
        20% {
            background-color: #fff;
            transform: scaleY(1.5);
        }

        40% {
            transform: scaleY(1);
        }
    } 
`;


export const SpinnerContainer = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
`;

export const Spinner = styled.div`
    position: relative;
    width: 40px;
    height: 40px;
    border-radius: 50%;

    ::before,
    :after {
        content: "";
        position: absolute;
        border-radius: inherit;
    }
    :before {
        width: 100%;
        height: 100%;
        background-image: linear-gradient(0deg, #01bf71 0%, #333399 100%);
        animation: spin .5s infinite linear;
    }
    :after {
        width: 85%;
        height: 85%;
        background-color: ${({theme}) => theme.bg};
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }

    @keyframes spin {
        to {
            transform: rotate(360deg)
        }
    }

`