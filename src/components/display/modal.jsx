import React from 'react'
import Dialog  from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import Typography from '@mui/material/Typography';
import { makeStyles } from '@mui/styles'


import { FiX } from 'react-icons/fi';
import Controls from '../controls/Controls';


const useStyles = makeStyles(theme => ({
    dialogWrapper: {
        padding: "2px",
        position: 'absolute',
        top: "5px",
        // alignItems: "center",
    },
    dialogTitle: {
        paddingRight: '0px'
    }
}))

export default function Modal(props) {

    const { title, children, openPopup, setOpenPopup, openModal, setOpenModal } = props;
    const classes = useStyles();

    return (
        <Dialog open={openPopup || openModal} maxWidth="md" classes={{ paper: classes.dialogWrapper }}>
            <DialogTitle className={classes.dialogTitle}>
                <div style={{ display: 'flex' }}>
                    <Typography variant="h6" component="div" style={{ flexGrow: 1 }}>
                        {title}
                    </Typography>
                    <Controls.CancelButton
                        onClick={ setOpenPopup ? () => setOpenPopup(false) : setOpenModal ? () => setOpenModal(false) :  ""}>
                        {/* // onClick={ setOpenModal ? () => setOpenModal(false) :  ""}> */}
                        {/* <FiX /> */}
                    </Controls.CancelButton>
                </div>
            </DialogTitle>
            <DialogContent dividers>
                {children}
            </DialogContent>
        </Dialog>
    )
}