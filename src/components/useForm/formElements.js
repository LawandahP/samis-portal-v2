import styled from 'styled-components';
import { Link as LinkRouter } from 'react-router-dom'
import { fontSize } from '../../styles/variables'

export const Container = styled.div`
    /* height: 100vh; */
    position: fixed;
    bottom: 0;
    left: 0;
    right: 0;
    top: 0;
    z-index: 20;
    overflow: auto;
    background: linear-gradient(
        100deg,
        rgba(1, 147, 86, 1) 10%,
        rgba(10, 201, 122, 1) 100%
    );
`;

export const NgFormWrapper = styled.div`
    background: ${({dark}) => (dark ? '#fff' : "")};
    max-width: 700px;
    /* height: auto; */
    /* overflow: auto; */
    /* width: 100%; */
    z-index: 1;
    display: grid;
    margin: 0 auto;
    
    padding: 40px 32px 40px 40px;
    /* border-right: 4px; */
    box-shadow: 0 1px 3px rgba(0,0,0,0.9);

    @media screen and (max-width: 400px) {
        padding: 32px 32px;
    }
`;



export const FormWrapper = styled.div`
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    /* @media screen and (max-width: 400px) {
        height: 80%;        
    } */
`;

export const Icon = styled(LinkRouter)`
    margin-left: 32px;
    margin-top: 32px;
    text-decoration: none;
    color: #fff;
    font-weight: 700;
    font-size: 32px;
    cursor: pointer;

    @media screen and (max-width: 480px) {
        margin-left: 16px;
        margin-top: 8px;
    }
`;

export const FormContent = styled.div`
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;

    @media screen and (max-width: 480px) {
        padding: 10px;
    }
`;


export const FormH1 = styled.h1`
    margin-bottom: 20px;
    color: #101010;
    font-size: ${fontSize.lgFont};
    font-weight: 600;
    text-align: center;

    @media screen and (max-width: 480px) {
        font-size: ${fontSize.mdFont};
    }
`;

export const FormLabel = styled.label`
    margin-bottom: 8px;
    font-size: 14px;
    color: #fff;
`;

export const FormInput = styled.input`
    padding: 16px 16px;
    margin: 0px 0px 15px 6px;
    border: none;
    display: grid;
    grid-template-columns: auto auto;
    border-radius: 4px;
`;

export const FormButtonWrapper = styled.div`
    /* width: 100%; */
    display: flex;
    justify-content: center;
    align-content: center;
    margin: 5px;
`;

export const FormButton = styled.button`
    background: #01bf71;
    padding: 10px 14px;
    border: none;
    border-radius: 4px;
    color: #fff;
    cursor: pointer;
    justify-self: center;
    align-self: center;
    border: .1px solid #01bf71;
    /* width: 20%; */

    &:hover {
        background: #fff;
        color: #01bf71;
        transition: 0.3s ease-in-out;
        border: .1px solid #01bf71;
    }

    &:disabled {
        background: grey;
        border: none;
    }
`;

export const Text = styled.span`
    text-align: center;
    /* margin-top: 24px; */
    color: #101010;
    font-size: ${fontSize.mdFont};
`;