import { FaBitbucket, FaCar, FaChessBishop, FaHouzz, FaPeopleCarry, FaSearchengin, FaStackExchange } from "react-icons/fa";
import { FcElectroDevices, FcHome, FcManager, FcSearch, FcSettings, FcStatistics, FcWorkflow, FcSupport, FcLineChart, FcTodoList, FcSimCardChip, FcBookmark, FcAddressBook, FcBadDecision } from 'react-icons/fc'

export const sideBarLinks = [

    {
        label: "Dashboard",
        icon: <FcWorkflow />,
        to: "/dashboard",
        // notification: 5
    },
    {
        label: "Streams",
        icon: <FcManager />,
        to: "/streams",
        // notification: 5
    },
    {
        label: "Kcse Subjects",
        icon: <FcTodoList />,
        to: "/kcse_subjects",
        // notification: 5
    },
    {
        label: "Academic Subjects",
        icon: <FcAddressBook />,
        to: "/academic_subjects",
        // notification: 5
    },
    {
        label: "Students",
        icon: <FcLineChart />,
        to: "/students",
        // notification: 5
    },
    {
        label: "Testimonials",
        icon: <FcSupport />,
        to: "/testimonials",
        notification: 5
    },
    {
        label: "Games",
        icon: <FcManager />,
        to: "/games",
        // notification: 5
    },

]

export const sideBarLinks2 = [

    {
        label: "Staff",
        icon: <FcElectroDevices />,
        to: "/staff",
        // notification: 5
    },
    {
        label: "Contacts",
        icon: <FcSettings />,
        to: "/contacts",
        // notification: 5
    },
    {
        label: "School Info",
        icon: <FcSimCardChip />,
        to: "/school_info",
        // notification: 5
    },
    {
        label: "School History",
        icon: <FaChessBishop />,
        to: "/school_history",
        // notification: 5
    },
    {
        label: "Analysis",
        icon: <FcStatistics />,
        to: "/analysis",
        // notification: 5
    },
    {
        label: "Reports",
        icon: <FcBadDecision />,
        to: "/reports",
        // notification: 5
    },
    {
        label: "Student Report",
        icon: <FcBadDecision />,
        to: "/student_report",
        // notification: 5
    }

]