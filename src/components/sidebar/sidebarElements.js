import styled from "styled-components";
import { Link as RouterLink } from "react-router-dom";
import { spacing } from "../../styles/variables";
import { FaPiedPiperPp } from "react-icons/fa";



export const NgSidebarContainer = styled.div`
    /* z-index: 999; */
    width: ${({isOpen}) => (!isOpen ? `auto` : spacing.sidebarWidth)};
    height: 100vh;
    overflow: auto;
    background: ${({theme}) => theme.bg};
    position: sticky;
    transition: all 0.3s ease-in-out;
    top:0;

    
    opacity: ${({isOpen}) => (isOpen ? '100%' : 'auto')};
    right: ${({isOpen}) => (isOpen ? '0' : '0')};
    
    @media screen and (max-width: 700px) {
        display: ${({isOpen}) => (isOpen ? `auto` : 'none')};
        transition: all 0.3s ease-in-out;
    };
    padding: ${spacing.mdSpacing};
`;

export const NgLogoWrapper = styled.div`
    width: 100%;
    height: auto;
    display: flex;
    color: inherit;
    text-decoration: none;
    align-items: center;
`;

export const NgLogo = styled.img`
    width: 35px;
    height: auto;
    cursor: pointer;
    margin-right: ${spacing.smSpacing};
`;

export const NgCompanyName = styled.div`
    font-size: 1rem;
    font-weight: 700;
    display: ${({ isOpen }) => (isOpen ? 'block' : 'none')};

    @media screen and (max-width: 600px) {
        font-size: 1rem;
    }
`;

export const NgDivider = styled.div`
    height: 1px;
    width: 100%;
    background: ${({theme}) => theme.bg3};
    margin: ${spacing.mdSpacing} 0;
`;

export const NgSidebarLinkContainer = styled.div`
    border-radius: ${spacing.borderRadius} 0px 0px ${spacing.borderRadius};
    background: ${({ theme, isActive }) => (!isActive ? `transparent` : theme.bg3)};
    border-right: ${({ theme, isActive }) => (isActive ? `3px solid #01bf71` : theme.bg3)};
    font-weight: ${({ isActive }) => (isActive ? `600` : `normal`)};
    margin: 2px, 0;
    font-size: .95rem;

    &:hover {
        box-shadow: inset 0 0 0 1 ${({theme}) => theme.bg3};
        font-weight: 600;
        border-right: 3px solid #01bf71;

        transition: all 0.2s ease-in-out;
    }

    @media screen and (max-width: 600px) {
        font-size: 0.7rem;
    }
`;

export const NgSidebarLinkWrapper = styled(RouterLink)`
    cursor: pointer;
    width: ${({ isOpen }) => (!isOpen ? `fit-content` : {})};
    align-items: center;
    display: flex;
    color: inherit;
    /* font-size: 16px; */
    text-decoration: none;
    padding-bottom: calc(${spacing.smSpacing} - 2px) 0;
`;

export const NgSidebarLinkIcon = styled.div`
    padding: ${spacing.smSpacing} ${spacing.smSpacing};
    display: flex;
    /* font-size: 18px; */
`;

export const NgSidebarLinkLabel = styled.div`
    margin-left: ${spacing.smSpacing};
    display: block;
    flex: 1;
    color: inherit;
    font-size: 12px;
    
    display: ${({ isOpen }) => (isOpen ? 'block' : 'none')};
`;


export const NgThemeContainer = styled.div`
    display: flex;
    align-items: center;
    font-size: 16px;
`;

export const NgThemeWrapper = styled.div`
    cursor: pointer;
    width: ${({ isOpen }) => (!isOpen ? `fit-content` : {})};
    align-items: center;
    display: flex;
    color: inherit;
    font-size: 16px;
    text-decoration: none;
    padding: calc(${spacing.smSpacing} - 2px) 0;
`;

export const NgThemeLabel = styled.span`
    flex: 1;
    display: ${({ isOpen }) => (isOpen ? 'block' : 'none')};
`;

export const NgThemeToggler = styled.div`
    padding: ${spacing.smSpacing} ${spacing.smSpacing};
    display: flex;
    font-size: 18px;
    margin: 0 auto;
    cursor: pointer;
    /* transform: rotate(180deg); */
`;


