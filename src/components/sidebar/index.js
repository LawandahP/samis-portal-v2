import React, { useState } from 'react';
import { useSelector } from 'react-redux';

import { useContext } from 'react'
import { FcIdea } from 'react-icons/fc'
import { MdDarkMode } from 'react-icons/md'
import { useLocation } from 'react-router-dom'
import { ThemeContext } from '../../App'

import { 
    NgSidebarContainer,
    NgLogo,
    NgDivider,
    NgCompanyName,
    NgLogoWrapper,
    NgSidebarLinkContainer,
    NgSidebarLinkWrapper,
    NgSidebarLinkLabel,
    NgSidebarLinkIcon,
    NgThemeContainer,
    NgThemeLabel,
    NgThemeToggler,
    NgThemeWrapper,
} from './sidebarElements'

import { sideBarLinks, sideBarLinks2 } from './sidebarLinks'
import { Avatar } from '@mui/material';
import { FaAngleLeft } from 'react-icons/fa';
import { NgSideBarToggleButton } from '../navbar/navbarElements';


const Sidebar = ({isOpen, toggle}) => {
    const { theme, setTheme } = useContext(ThemeContext)

    const { pathname } = useLocation();

    const schoolInfoRead = useSelector(state => state.schoolInfoRead)
    const { schoolInfo } = schoolInfoRead

    const handleToggleTheme = () => {
        setTheme(prevTheme => prevTheme === 'light' ? 'dark' : 'light')
        console.log(theme)
    }

    return (
        <NgSidebarContainer isOpen={isOpen}>           
            <NgLogoWrapper to='/'>
                <NgLogo src={schoolInfo?.logo_url} alt={schoolInfo?.school_name} />
                <NgCompanyName isOpen={isOpen}>{schoolInfo?.school_name}</NgCompanyName>                
            </NgLogoWrapper>

            <NgDivider />

            {sideBarLinks.map((link) => (
                <NgSidebarLinkContainer key={link.label} isActive = {pathname === link.to}>
                    <NgSidebarLinkWrapper  isOpen={isOpen} to={link.to}>
                        <NgSidebarLinkIcon>{link.icon}</NgSidebarLinkIcon>
                            <NgSidebarLinkLabel isOpen={isOpen}>{link.label}</NgSidebarLinkLabel>
                    </NgSidebarLinkWrapper>
                </NgSidebarLinkContainer>
            ))}
            
            <NgDivider/>

            {sideBarLinks2.map((link) => (
                <NgSidebarLinkContainer key={link.label} isActive = {pathname === link.to}>
                    <NgSidebarLinkWrapper  isOpen={isOpen} to={link.to}>
                        <NgSidebarLinkIcon>{link.icon}</NgSidebarLinkIcon>
                        <NgSidebarLinkLabel isOpen={isOpen}>{link.label}</NgSidebarLinkLabel>
                    </NgSidebarLinkWrapper>
                </NgSidebarLinkContainer>
            ))}

            <NgDivider />

            <NgThemeContainer>
                <NgThemeWrapper>
                <NgThemeToggler 
                    title={ theme === 'light' ? "switch to dark mode" : "switch to light mode" } 
                    onClick={handleToggleTheme}>
                    { theme === 'dark' ? <FcIdea /> : <MdDarkMode /> }
                </NgThemeToggler>
                <NgThemeLabel isOpen={isOpen}></NgThemeLabel>
                </NgThemeWrapper>
                
                
            </NgThemeContainer>
            

        </NgSidebarContainer>
    )
}

export default Sidebar