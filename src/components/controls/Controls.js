
import InputField from './Input';
import PasswordInputField from './PasswordInput';
import SearchInputField from './SearchInput';

import { SelectField, SelectAnalysisField, MultipleSelectField } from './Select';

import { CancelButton, AddButton, ActionButton } from './Button'


const Controls = {
    InputField,
    SelectField,
    SelectAnalysisField,
    PasswordInputField,
    SearchInputField,
    CancelButton,
    AddButton,
    ActionButton
}

export default Controls;

