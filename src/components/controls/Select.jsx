import React from 'react'
import { Select as MuiSelect, FormHelperText } from '@mui/material';

import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';

import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Chip from '@mui/material/Chip';
  

export function SelectField(props) {
    const { name, label, value, error=null, onChange, options, size, list_value } = props;

    return (
        <FormControl variant="outlined"
            {...(error && {error:true})}>
            <InputLabel>{label}</InputLabel>
                <MuiSelect
                    inputProps={{style: {fontSize: 12, fontFamily: 'Poppins'}}}
                    InputLabelProps={{style: {fontSize: 14, fontFamily: 'Poppins'}}}
                    size={size || 'medium'}
                    label={label}
                    name={name}
                    value={value}
                    onChange={onChange}
                  >
                    <MenuItem value="">None</MenuItem>
                      {options?.map((item) => (
                          <MenuItem key={item.id} value={item.id}>
                              {item.title}
                          </MenuItem>
                      ))}
                </MuiSelect>
            {error && <FormHelperText>{error}</FormHelperText>}
        </FormControl>
    )
}


export function SelectAnalysisField(props) {
  const { name, label, value, error=null, onChange, options, size, loading } = props;

  return (
      <FormControl variant="outlined"
          {...(error && {error:true})}>
          <InputLabel>{label}</InputLabel>
              <MuiSelect
                  inputProps={{style: {fontSize: 12, fontFamily: 'Poppins'}}}
                  InputLabelProps={{style: {fontSize: 14, fontFamily: 'Poppins'}}}
                  size={size || 'medium'}
                  label={label}
                  name={name}
                  value={value}
                  onChange={onChange}
                >
                  <MenuItem value="">None</MenuItem>
                    {options?.map((item) => (
                        <MenuItem loading={loading} key={item} value={item}>
                            {item}
                        </MenuItem>
                    ))}
              </MuiSelect>
          {error && <FormHelperText>{error}</FormHelperText>}
      </FormControl>
  )
}

export function AutoCompleteField(props) {
    const { name, label, value, error=null, onChange, options, size, list_name } = props;
    return (
        <Autocomplete 
            disablePortal
            id="combo-box-demo"
            options={options}
            value={value}
            onChange={onChange}
            // sx={{ width: 300 }}
            renderInput={(params) => <TextField {...params} label="Building Type" />}
        />
    )
}




const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const names = [
  'Oliver Hansen',
  'Van Henry',
  'April Tucker',
  'Ralph Hubbard',
  'Omar Alexander',
  'Carlos Abbott',
  'Miriam Wagner',
  'Bradley Wilkerson',
  'Virginia Andrews',
  'Kelly Snyder',
];

function getStyles(name, personName, theme) {
  return {
    fontWeight:
      personName.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}

export function MultipleSelect(props) {
    const theme = useTheme();
    const [personName, setPersonName] = React.useState([]);
  
    const handleChange = (event) => {
      const {
        target: { value },
      } = event;
      setPersonName(
        // On autofill we get a stringified value.
        typeof value === 'string' ? value.split(',') : value,
      );
    };
  
    return (
      <div>
        <FormControl sx={{ m: 1, width: 300 }}>
          <InputLabel id="demo-multiple-chip-label">Chip</InputLabel>
          <Select
            labelId="demo-multiple-chip-label"
            id="demo-multiple-chip"
            multiple
            value={personName}
            onChange={handleChange}
            input={<OutlinedInput id="select-multiple-chip" label="Chip" />}
            renderValue={(selected) => (
              <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
                {selected.map((value) => (
                  <Chip key={value} label={value} />
                ))}
              </Box>
            )}
            MenuProps={MenuProps}
          >
            {names.map((name) => (
              <MenuItem
                key={name}
                value={name}
                style={getStyles(name, personName, theme)}
              >
                {name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </div>
    );
  }