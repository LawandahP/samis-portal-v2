import { TextField } from '@mui/material'
import React from 'react'

export default function InputField(props) {

    const { label, variant, name, value, error=null, onChange, type, size, multiline, ...other }  = props;
    
    return (
        <TextField
            inputProps={{style: {fontSize: 12, fontFamily: 'Poppins'}}}
            InputLabelProps={{style: {fontSize: 14, fontFamily: 'Poppins'}}}
            multiline={multiline}
            size={size || "medium"}
            type={type}
            variant={variant || "outlined"}
            label={label}
            value={value}
            name={name}
            {...other}
            onChange={onChange}
            {...(error && {error:true, helperText:error})}
        />
    )
}