
import React, { useState } from 'react'
import styled from 'styled-components';

import Tooltip from '@mui/material/Tooltip';
import { FiX, FiPlus } from 'react-icons/fi';


export const CloseIcon = styled(FiX)`
    position: absolute;
    color: #ff0000;
    top: 1.4rem;
    right: 1.5rem;
    cursor: pointer;
    &:hover {
        transition: all ease-in-out;
        background: #ff9393;
        border-radius: 5px;
        padding: 3px;
    }
`
export const Icon = styled.div`
    position: absolute;
    top: 1.2rem;
    right: 1.5rem;
    font-size: 1.5rem;
    
    cursor: pointer;
    outline: none;

    &:hover {
        transition: all ease-in-out;
        background: #ff7c7c;
    }
`;

export const Button = styled.button`
    border-radius: 50px;
    background: ${({primary}) => (primary ? '#01BF71' : '#010606')};
    white-space: nowrap;
    padding: ${({big}) => (big ? '14px 18px' : '12px 30px')};
    color: ${({dark}) => (dark ? '#010606' : '#fff')};
    font-style: ${({fontBig}) => (fontBig ? '20px' : '#16px')};
    outline: none;
    border: none;
    cursor: pointer;
    display: flex;
    justify-content: center;
    align-items: center;
    transition: all 0.2s ease-in-out;

    &:hover {
        transition: all ease-in-out;
        background: ${({primary}) => (primary ? '#010606' : '#01BF71')};
    }
`;

export const ActionButtonWrapper = styled.div`
    display: flex;
    align-items: flex-start;
`;


export const ButtonAction = styled.div`
    
    color: ${({edit}) => (edit ? '#12acac' : '#ff0000')};
    cursor: pointer;
    margin-left: 5px;
    &:hover {
        transition: all ease-in-out;
        color: ${({edit}) => (edit ? '#53ffff' : '#ff7c7c')};
    }
`;



export const CancelButton = ({onClick}) => {
    return (
        
        <CloseIcon onClick={onClick} />
        
    )
}


export const AddButton = ({onClick}) => {
    const [ hover, setHover ] = useState(false);
    const onHover = () => {
        setHover(!hover)
    }

    return (
        <Button
            onClick={onClick}
            onMouseEnter={onHover}
            onMouseLeave={onHover}
            primary>
            Add { hover ? <FiPlus /> : ""}
        </Button>
    )
}


export const ActionButton = ({onClick, children, edit, title}) => {
    return (
        <Tooltip title={title} placement="top-start">
            <ButtonAction
                edit={edit}
                onClick={onClick}>
                {children}
            </ButtonAction>
        </Tooltip>
        
        
    )
}