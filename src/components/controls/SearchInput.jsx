import React from 'react';
import { makeStyles } from '@mui/styles';

import TextField from '@mui/material/TextField';
import InputAdornment from '@mui/material/InputAdornment';

import { FiSearch } from 'react-icons/fi'

// const useStyles = makeStyles(theme => ({
//     searchInput: {
//         width : "100%",
//         // [theme.breakpoints.down('sm')]: {
//         //     width: "85%"
//         // },
//         // [theme.breakpoints.down('md')]: {
//         //     width: "95%"
//         // }
//     }
// }));

const SearchInputField = (props) => {
    // const classes = useStyles();
    const { className, label, name, value, onChange, type, size, variant, ...other }  = props;

    return (
        
            <TextField 
                // className={className || classes.searchInput}
                label={label}
                size={size || "small"}
                variant={variant || "filled"}
                InputProps = {{
                    startAdornment:
                        <InputAdornment position="start">
                            <FiSearch/>
                        </InputAdornment>
                }}  
                onChange = { onChange } 
                {...other}
            />                              

     );
};

export default SearchInputField;
