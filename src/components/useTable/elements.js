import styled from 'styled-components';

export const TableTop = styled.div`
    justify-content: space-between;
    display: flex;
    width: 100% !important;
    padding-bottom: 5px;
    /* padding-bottom: 3px; */
`;


export const Chip = styled.div`
    border-radius: 50px;
    background: ${({active}) => (active ? '#01BF71' : 'red')};
    white-space: nowrap;
    /* padding: 12px 30px; */
    color: ${({dark}) => (dark ? '#010606' : '#fff')};
    font-style: ${({fontBig}) => (fontBig ? '20px' : '#16px')};
    outline: none;
    border: none;
    cursor: pointer;
    display: flex;
    justify-content: center;
    align-items: center;
    transition: all 0.2s ease-in-out;
`;