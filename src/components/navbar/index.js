import React, {useState} from 'react'
import { FaAngleLeft } from 'react-icons/fa'
import { NgSideBarToggleButton, ProfileAvatar } from './navbarElements'
import { NgNavContainer } from './navbarElements'

const Navbar = ({toggle, isOpen}) => {
	
	return (
		<NgNavContainer>
			<NgSideBarToggleButton isOpen={isOpen} onClick={toggle}>
				<FaAngleLeft />
			</NgSideBarToggleButton>
			<ProfileAvatar>Signed in as</ProfileAvatar>
		</NgNavContainer>
	)
}

export default Navbar