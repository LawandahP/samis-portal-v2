import styled from "styled-components";

export const NgNavContainer = styled.nav`
    background: ${({theme}) => theme.bg3};
    z-index: 1;
    height: 50px;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 1rem;
    position: sticky;
    top: 0;
           
    @media screen and (max-width: 960px) {
        transition: 0.8s all ease;
    }
`;

export const NgSideBarToggleButton = styled.div`
    color: #fff;
    display: block;
    position: absolute;
    left: 0;
    transform: translate(-100%, 60%);
    font-size: 1.8rem;
    color: inherit;
    cursor: pointer;
    transform: ${({ isOpen }) => (!isOpen ? `rotate(180deg)` : `initial`)};
    transition: all 0.3s inline;
`;

export const ProfileAvatar = styled.div`
    /* color: #fff; */
    display: block;
    position: absolute;
    right: 20px;
    border: 1px solid;
    padding: 5px;
    border-radius: 50px;
    font-size: 12px;
    cursor: pointer;

    &:hover {
        transform: scale(1.01);
        transition: #01bf71 0.2s ease-in-out;
    }
`;