import { css } from "styled-components";

export const spacing = {
    sidebarWidth: `250px`,
    sidebarWidthSm: `20px`,
    ngMainSmSpacing: `4px`,
    smSpacing: `8px`,
    mdSpacing: `12px`,
    lgSpacing: `24px`,
    xlSpacing: `32px`,
    xxlSpacing: `48px`,
    borderRadius: `6px`,
};

export const navSpacing = {
    xsSpacing: `2px`,
    smSpacing: `4px`,
};

export const btnReset = css`
    font-family: inherit;
    outline: none;
    border: none;
    background: none;
    letter-spacing: inherit;
    color: inherit;
    font-size: inherit;
    text-align: inherit;
    padding: 0;
`;

export const fontSize = {
    smSideBar: `.85rem`,
    mdSideBar: `1rem`,

    xsFont: `0.2rem`,
    smFont: `0.5rem`,
    mdFont: `0.75rem`,
    lgFont: `1rem`,


}