import React, { useState, createContext } from 'react'
import { ThemeProvider } from 'styled-components';
import './App.css';
import Layout from './components/layout/Layout';
import Routes from './routes';
import { GlobalStyle } from './styles/globalStyles';
import { darkTheme, greenTheme, lightTheme } from './styles/theme';

export const ThemeContext = createContext(null);

const App = () => {
    const [theme, setTheme] = useState("light")
    const themeStyle = theme === 'light' ? lightTheme : 'dark' ? darkTheme : 'green' ? greenTheme : ''

    return (
        <ThemeContext.Provider value={{setTheme, theme}}>
            <ThemeProvider theme={themeStyle}>
                <GlobalStyle />
                <Layout>
                    <Routes />
                </Layout>
            </ThemeProvider>
        </ThemeContext.Provider>
               
    )
}

export default App

