import { createContext } from "react";

export const CurrentUserContext = createContext(null);

export const UnitPropertiesContext = createContext({});

export const BuildingPropertiesContext = createContext({});

export const GetCountContext = createContext({});

export const LandlordDetailContext = createContext({});
