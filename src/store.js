import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { userSignInReducer, userSignUpReducer } from './auth/users/reducers';
import { 
    tenantCreateReducer, tenantDeleteReducer,
    tenantReadReducer, tenantUpdateReducer,
    tenantDetailsReducer
} from './screens/tenants/reducers';


import { 
    unitCreateReducer, unitDeleteReducer,
    unitReadReducer, unitUpdateReducer,
    unitDetailsReducer
} from './screens/units/reducers';

import { 
    kcseSubjectCreateReducer,
    kcseSubjectReadReducer, 
    kcseSubjectUpdateReducer, 
    kcseSubjectDeleteReducer, 
} from './screens/kcse_subjects/reducers';

import { 
    streamReadReducer,
    streamCreateReducer,
    streamDetailsReducer,
    streamUpdateReducer,
    streamDeleteReducer

} from './screens/streams/reducers'

import { 
    testimonialCreateReducer, 
    testimonialReadReducer, 
    testimonialUpdateReducer, 
    testimonialDeleteReducer, 
} 
from './screens/testimonials/reducers';

import { 
    gameCreateReducer, 
    gameReadReducer, 
    gameUpdateReducer,
    gameDeleteReducer
} from './screens/games/reducers';

import { 
    staffCreateReducer, 
    staffDeleteReducer, 
    staffReadReducer, 
    staffUpdateReducer 
} from './screens/staff/reducers';

import { 
    contactInfoCreateReducer, 
    contactInfoDeleteReducer, 
    contactInfoReadReducer, 
    contactInfoUpdateReducer 
} from './screens/contact_info/reducers';

import { 
    schoolInfoCreateReducer,
    schoolInfoReadReducer,
    schoolInfoUpdateReducer,
    schoolInfoDeleteReducer
} from './screens/school_info/reducers';

import { 
    schoolHistoryCreateReducer, 
    schoolHistoryDeleteReducer, 
    schoolHistoryReadReducer, 
    schoolHistoryUpdateReducer 
} from './screens/school_history/reducers';

import { 
    academicSubjectCreateReducer, 
    academicSubjectReadReducer, 
    academicSubjectUpdateReducer,
    academicSubjectDeleteReducer, 
} from './screens/academic_subjects/reducers';

import { 
    readAcademicReportsReducer, readAnalysisByTermAndYearReducer, 
    academicReportDetailsReducer, studentReportReducer 
} from './screens/analysis/reducers';

import { 
    statusCreateReducer, 
    statusReadReducer 
} from './screens/stepper/status/reducers';


const reducers = combineReducers({
    
    signUpUser: userSignUpReducer,
    signInUser: userSignInReducer,

    statusCreate: statusCreateReducer,
    statusRead: statusReadReducer,

    analysisRead:   readAnalysisByTermAndYearReducer,
    reportsList:    readAcademicReportsReducer,
    reportDetails:  academicReportDetailsReducer,
    studentReportRead: studentReportReducer,

    academicSubjectRead: academicSubjectReadReducer,
    academicSubjectCreate: academicSubjectCreateReducer,
    academicSubjectUpdate: academicSubjectUpdateReducer,
    academicSubjectDelete: academicSubjectDeleteReducer,

    schoolHistoryCreate: schoolHistoryCreateReducer,
    schoolHistoryRead: schoolHistoryReadReducer,
    schoolHistoryUpdate: schoolHistoryUpdateReducer,
    schoolHistoryDelete: schoolHistoryDeleteReducer,

    schoolInfoCreate: schoolInfoCreateReducer,
    schoolInfoRead:   schoolInfoReadReducer,
    schoolInfoUpdate: schoolInfoUpdateReducer,
    schoolInfoDelete: schoolInfoDeleteReducer,

    contactInfoRead: contactInfoReadReducer,
    contactInfoCreate: contactInfoCreateReducer,
    contactInfoUpdate: contactInfoUpdateReducer,
    contactInfoDelete: contactInfoDeleteReducer,

    staffCreate: staffCreateReducer,
    staffRead: staffReadReducer,
    staffUpdate: staffUpdateReducer,
    staffDelete: staffDeleteReducer,

    gameCreate: gameCreateReducer,
    gameRead: gameReadReducer,
    gameUpdate: gameUpdateReducer,
    gameDelete: gameDeleteReducer,

    testimonialCreate: testimonialCreateReducer,
    testimonialRead: testimonialReadReducer,
    testimonialUpdate: testimonialUpdateReducer,
    testimonialDelete: testimonialDeleteReducer,

    kcseSubjectCreate: kcseSubjectCreateReducer,
    kcseSubjectRead: kcseSubjectReadReducer,
    kcseSubjectUpdate: kcseSubjectUpdateReducer,
    kcseSubjectDelete: kcseSubjectDeleteReducer,

    streamCreate: streamCreateReducer,
    streamRead: streamReadReducer,
    streamDetails: streamDetailsReducer,
    streamUpdate: streamUpdateReducer,
    streamDelete: streamDeleteReducer,

    readTenants: tenantReadReducer,
    createTenant: tenantCreateReducer,
    updateTenant: tenantUpdateReducer,
    deleteTenant: tenantDeleteReducer,
    tenantDetails: tenantDetailsReducer,

    readUnits : unitReadReducer,
    createUnit: unitCreateReducer,
    updateUnit: unitUpdateReducer,
    deleteUnit: unitDeleteReducer,
    unitDetails: unitDetailsReducer,
})

const schoolInfoFromStorage = sessionStorage.getItem('schoolInfo') ?
    JSON.parse(sessionStorage.getItem('schoolInfo')) : null

// const schoolHistoryFromStorage = sessionStorage.getItem('schoolHistory') ?
//     JSON.parse(sessionStorage.getItem('schoolHistory')) : null

const userInfoFromStorage = sessionStorage.getItem('userInfo') ?
    JSON.parse(sessionStorage.getItem('userInfo')) : null


const initialState = {
    signInUser: { userInfo: userInfoFromStorage },
    schoolInfoCreate: { schoolInfo: schoolInfoFromStorage },
    // schoolHistoryCreate: { schoolHistory: schoolHistoryFromStorage}

}


const middleware = [thunk]

const store = createStore(reducers, initialState,
    composeWithDevTools(applyMiddleware(...middleware)));

export default store;