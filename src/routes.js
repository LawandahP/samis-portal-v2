import React from 'react'
import { Route, Routes as Switch } from 'react-router-dom'
import Dashboard from './screens/dashboard';
import Index from './screens/Home';

import KcseSubjectsReadScreen from './screens/kcse_subjects/index';
import StreamReadScreen from './screens/streams/index';

import StudentReadScreen from './screens/students';


import SignInPage from './auth/users/signin';
import SignUpPage from './auth/users/signup';
import PageNotFound from './auth/pageNotFound';
import Unauthorized from './auth/unauthorized';
import RequireAuth from './auth/use.auth';
import TestimonialReadScreen from './screens/testimonials';
import GamesReadScreen from './screens/games';
import StaffReadScreen from './screens/staff';
import ContactInfoReadScreen from './screens/contact_info';
import SchoolInfoReadScreen from './screens/school_info';
import SchoolHistoryReadScreen from './screens/school_history';
import AcademicSubjectReadScreen from './screens/academic_subjects';
import AnalysisScreen from './screens/analysis/analysis_screen';
import ReportScreen from './screens/analysis/report_screen';
import ReportDetailScreen from './screens/analysis/report_detail_screen';
import StudentsReportFormScreen from './screens/analysis/student_report_form';

import StepperScreen from './screens/stepper/process';


const roles = {
    'ADMIN': "admin",
    'STUDENT': "student",
    'TEACHER': "teacher",
    
}


const Routes = () => {
    return (
        <Switch>
            {/* <Route path='/' element={<Index />} exact /> */}
            <Route path='/' element={<Dashboard /> } exact />
            <Route path='/signup'  element={<SignUpPage />} />
            <Route path='/signin'  element={<SignInPage />} />
            <Route path='/process' element={<StepperScreen />} />

            <Route element={<RequireAuth allowedRoles={[roles.ADMIN]} />}>
                <Route path='/dashboard' element={<Dashboard />} />
            </Route>

            <Route element={<RequireAuth allowedRoles={[roles.ADMIN]} />}>
                <Route path='/kcse_subjects' element={<KcseSubjectsReadScreen />} />
                <Route path='/academic_subjects' element={<AcademicSubjectReadScreen />} />
                <Route path='/analysis' element={<AnalysisScreen />} />
                <Route path='/reports' element={<ReportScreen />} />
                <Route path='/report/:id' element={<ReportDetailScreen />} />
                <Route path='/student_report' element={<StudentsReportFormScreen /> } />

                <Route path='/testimonials'  element={<TestimonialReadScreen />} />

                <Route path='/students'       element={<StudentReadScreen />} />
                <Route path='/streams'        element={<StreamReadScreen />} />
                <Route path='/games'          element={<GamesReadScreen />} />
                <Route path='/staff'          element={<StaffReadScreen />} />
                <Route path='/contacts'       element={<ContactInfoReadScreen />} />
                <Route path='/school_info'    element={<SchoolInfoReadScreen />} />
                <Route path='/school_history' element={<SchoolHistoryReadScreen />} />

            </Route>

            <Route path='*' element={<PageNotFound />} />
            <Route path='/unauthorized' element={<Unauthorized />} />

        </Switch>
    )
}

export default Routes