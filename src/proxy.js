export const backend = window.__RUNTIME_CONFIG__.SAMIS_BACKEND_URL
export const visitor = window.__RUNTIME_CONFIG__.SAMIS_VISITOR_URL
export const portal = window.__RUNTIME_CONFIG__.SAMIS_PORTAL_URL
export const bff = window.__RUNTIME_CONFIG__.SAMIS_BACKEND_BFF_URL
